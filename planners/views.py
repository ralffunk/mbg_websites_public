# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import datetime
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.utils import timezone as tz
from mbg_websites import utils
from planners.models import Calendar, UserCalendarColor


# default color for displaying events in calendar
DEFAULT_BLUE = 'rgb(51,122,183)'


def planners_start_page(request):
    """
    Start page.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', ),
    ]

    calendar_list = []
    for c in Calendar.objects.filter(row_deleted=False):
        if c.current_details.is_public or request.user.has_perms(('planners.read_calendar', ), c):
            user_color = None
            if request.user.is_authenticated():
                user_color = request.user.usercalendarcolor_set.filter(calendar=c).first()

            if user_color:
                c.color = str(user_color)
            else:
                c.color = DEFAULT_BLUE

            calendar_list.append(c)

    ctx['calendar_list'] = sorted(calendar_list, key=lambda x: x.current_details.summary)

    return render(request, 'planners/responsive_calendar.html', ctx)


def events_json(request):
    """
    Return JSON list of events.
    :param request: HttpRequest
    :return: JsonResponse with list of events
    """
    calendar_list = [e for e in request.GET.get('calendar_list', '').split('-') if e]
    try:
        filter_start = datetime.datetime.strptime(request.GET.get('start', ''), '%Y-%m-%d').date()
        filter_end = datetime.datetime.strptime(request.GET.get('end', ''), '%Y-%m-%d').date()
    except ValueError:
        now_ = tz.now()
        filter_start = datetime.datetime(now_.year, now_.month, 1).date()
        filter_end = filter_start + datetime.timedelta(days=30)

    event_list = []
    for c in Calendar.objects.filter(row_deleted=False, pk__in=calendar_list):
        has_calendar_perm = request.user.has_perms(('planners.read_calendar', ), c)
        if c.current_details.is_public or has_calendar_perm:
            user_color = None
            if request.user.is_authenticated():
                user_color = request.user.usercalendarcolor_set.filter(calendar=c).first()

            if user_color:
                user_color = str(user_color)
            else:
                user_color = DEFAULT_BLUE

            for e in c.events.filter(row_deleted=False):
                det = e.current_details
                if filter_start <= tz.localtime(det.start_datetime).date() <= filter_end:
                    d = {
                        'title': det.summary,
                        'color': user_color,
                    }

                    if not det.is_confirmed:
                        d['title'] = '[A CONFIRMAR] ' + d['title']

                    if e.type == '01':
                        d['start'] = tz.localtime(det.start_datetime).strftime('%Y-%m-%dT%H:%M:00')
                    elif e.type == '02':
                        d['allDay'] = True
                        d['start'] = tz.localtime(det.start_datetime).strftime('%Y-%m-%d')
                        # end date is not included when showing in the calendar, so we add one day
                        aux = tz.localtime(det.end_datetime) + datetime.timedelta(days=1)
                        d['end'] = aux.strftime('%Y-%m-%d')

                    if has_calendar_perm:
                        d['url'] = reverse('planners:events-read', args=[c.pk, e.pk])

                    # only users with perms may see non-confirmed events
                    if det.is_confirmed or has_calendar_perm:
                        event_list.append(d)

    return JsonResponse(event_list, safe=False)


@login_required()
def user_colors(request):
    """
    Set user calendar colors.
    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == 'POST':
        for k, v in request.POST.items():
            if '_color' in k:
                try:
                    c = Calendar.objects.get(pk=k.split('_')[0], row_deleted=False)
                except Calendar.DoesNotExist:
                    c = None

                if c:
                    if c.current_details.is_public or request.user.has_perms(
                            ('planners.read_calendar', ), c):
                        ucc = request.user.usercalendarcolor_set.filter(calendar=c).first()
                        if not ucc:
                            if v != DEFAULT_BLUE:
                                ucc = UserCalendarColor()
                                ucc.user = request.user
                                ucc.calendar = c
                                ucc.color_r, ucc.color_g, ucc.color_b = v[4:-1].split(',')
                                ucc.save()
                        else:
                            ucc.color_r, ucc.color_g, ucc.color_b = v[4:-1].split(',')
                            ucc.save()

    return redirect('planners:start-page')


@login_required()
def user_responsibilities(request):
    """
    List responsibilities assigned to the current user.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Mis responsabilidades', ),
    ]

    ctx['sf_text'] = request.GET.get('sf_text', '')
    ctx.update(utils.get_sf_dates(request.GET))

    if request.GET.get('sf_status', '') == 'C':
        ctx['sf_status'] = 'C'
    elif request.GET.get('sf_status', '') == 'T':
        ctx['sf_status'] = 'T'
    else:
        ctx['sf_status'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
    else:
        ctx['sf_order'] = '0'

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_status={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_status'],
        ctx['sf_order'])

    responsibilities_list = []
    if request.user.profile.person:
        for e in request.user.profile.person.eventresponsibility_set.filter(
                event__calendar__row_deleted=False, event__row_deleted=False,
                row_time_end__isnull=True):
            r = {
                'date': tz.localtime(e.event.current_details.start_datetime),
                'task': e.task_display,
                'calendar': str(e.event.calendar),
                'event': str(e.event),
                'is_confirmed': e.event.current_details.is_confirmed,
            }

            if e.event.type == '01':
                r['time'] = r['date'].strftime('%H:%M')

            if (not ctx['sf_text']
                    or ctx['sf_text'].upper() in r['task'].upper()
                    or ctx['sf_text'].upper() in r['calendar'].upper()
                    or ctx['sf_text'].upper() in r['event'].upper()):
                if ctx['sf_date_start'] <= r['date'] <= ctx['sf_date_end']:
                    if (ctx['sf_status'] == 'A'
                            or (ctx['sf_status'] == 'C' and r['is_confirmed'])
                            or (ctx['sf_status'] == 'T' and not r['is_confirmed'])):
                        if request.user.has_perms(('planners.read_calendar', ), e.event.calendar):
                            r['url'] = reverse('planners:events-read', args=[
                                e.event.calendar.pk, e.event.pk])

                        responsibilities_list.append(r)

    if ctx['sf_order'] == '1':
        responsibilities_list = sorted(
            responsibilities_list,
            key=lambda x: (x['task'], x['date']))
    else:
        responsibilities_list = sorted(
            responsibilities_list,
            key=lambda x: x['date'])

    paginator = Paginator(responsibilities_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'planners/user_responsibilities_list.html', ctx)
