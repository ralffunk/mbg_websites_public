# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import datetime
import math
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError, PermissionDenied
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone as tz
from guardian.shortcuts import get_perms, get_objects_for_user
from mbg_websites import utils
from people.models import Person
from planners import pdf_reports
from planners.models import Calendar, Event, EventDetail, EventResponsibility, EventScheduleEntry


@login_required()
def events_list_all(request):
    """
    List events of all calendars.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de eventos', ),
    ]

    ctx['sf_text'] = request.GET.get('sf_text', '')
    ctx.update(utils.get_sf_dates(request.GET))

    if request.GET.get('sf_status', '') == 'C':
        ctx['sf_status'] = 'C'
    elif request.GET.get('sf_status', '') == 'T':
        ctx['sf_status'] = 'T'
    else:
        ctx['sf_status'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
    else:
        ctx['sf_order'] = '0'

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_status={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_status'],
        ctx['sf_order'])

    event_list = []
    for c in Calendar.objects.filter(row_deleted=False):
        has_calendar_perm = request.user.has_perms(('planners.read_calendar', ), c)
        has_sf_text = ctx['sf_text'].upper() in str(c).upper()

        if c.current_details.is_public or has_calendar_perm:
            qs = EventDetail.objects.filter(event__calendar=c, event__row_deleted=False,
                                            row_time_end__isnull=True)

            if not has_sf_text:
                q_t1 = Q(summary__icontains=ctx['sf_text'])
                q_t2 = Q(description__icontains=ctx['sf_text'])
                qs = qs.filter(q_t1 | q_t2)

            qs = qs.filter(start_datetime__range=(ctx['sf_date_start'], ctx['sf_date_end']))

            if ctx['sf_status'] == 'C':
                qs = qs.filter(is_confirmed=True)
            elif ctx['sf_status'] == 'T':
                qs = qs.filter(is_confirmed=False)

            for e in qs:
                if has_calendar_perm:
                    e.url = reverse('planners:events-read', args=[c.pk, e.event.pk])

                event_list.append(e)

    if ctx['sf_order'] == '1':
        event_list = sorted(
            event_list,
            key=lambda x: (x.event.calendar.current_details.summary, x.start_datetime))
    elif ctx['sf_order'] == '2':
        event_list = sorted(
            event_list,
            key=lambda x: (x.summary, x.start_datetime))
    else:
        event_list = sorted(event_list, key=lambda x: x.start_datetime)

    paginator = Paginator(event_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'planners/event_list_all.html', ctx)


@login_required()
def events_list_editable(request):
    """
    Event overview for planning.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Planificación de eventos', ),
    ]

    # get date filter params
    ctx.update(utils.get_sf_dates(request.GET))

    # get calendar filter params
    selected_calendar_set = set(request.GET.getlist('sf_c'))
    ctx['user_calendar_list'] = []
    for c in get_objects_for_user(
            request.user, 'planners.read_calendar', use_groups=False, accept_global_perms=False):
        if not c.row_deleted:
            if not selected_calendar_set or str(c.pk) in selected_calendar_set:
                c.is_selected = True
            else:
                c.is_selected = False
            ctx['user_calendar_list'].append(c)
    selected_calendar_list = [c for c in ctx['user_calendar_list'] if c.is_selected]
    ctx['user_calendar_list'] = sorted(
        ctx['user_calendar_list'],
        key=lambda x: x.current_details.summary)
    ctx['calendar_list_slice'] = ':{0} {0}:'.format(
        math.ceil(len(ctx['user_calendar_list']) / 2)).split()      # to show in two columns

    # get data columns filter params
    ctx['selected_data_set'] = set(request.GET.getlist('sf_d'))
    ctx['data_list'] = [
        {'code': e[0], 'dsc': e[1], 'is_selected': False}
        for e in (
            ('is_confirmed', 'Estado'),
            ('subtype', 'Tipo'),
            ('occasion', 'Ocasión'),
            ('topic', 'Tema'),
            ('bible_ref', 'Texto'),
        )
    ]
    ctx['data_list'].extend(
        {'code': e[0], 'dsc': e[1], 'is_selected': False}
        for e in EventResponsibility.RESPONSIBILITY_TASKS)
    if request.user.username == settings.ADMIN_USERNAME:
        ctx['data_list'].append({'code': 'google_last_sync', 'dsc': 'Última sincronización',
                                 'is_selected': False})
        ctx['data_list'].append({'code': 'google_sync_pending', 'dsc': 'Estado de sincronización',
                                 'is_selected': False})

    for d in ctx['data_list']:
        if d['code'] in ctx['selected_data_set']:
            d['is_selected'] = True
    ctx['data_list_slice'] = ':{0} {0}:'.format(
        math.ceil(len(ctx['data_list']) / 2)).split()               # to show in two columns

    ctx['sf'] = 'sf_date_start={}&sf_date_end={}&{}&{}'.format(
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        '&'.join('sf_c={}'.format(c.pk) for c in selected_calendar_list),
        '&'.join('sf_d={}'.format(d['code']) for d in ctx['data_list'] if d['is_selected']))

    event_list = []
    for c in selected_calendar_list:
        qs = EventDetail.objects.filter(event__calendar=c, event__row_deleted=False,
                                        row_time_end__isnull=True)
        qs = qs.filter(start_datetime__range=(ctx['sf_date_start'], ctx['sf_date_end']))

        for e in qs:
            e.url = reverse('planners:events-read', args=[c.pk, e.event.pk])
            event_list.append(e)

    event_list = sorted(event_list, key=lambda x: x.start_datetime)

    paginator = Paginator(event_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'planners/event_list_editable.html', ctx)


@login_required()
def events_list_editable_pdf(request):
    """
    Event overview for planning as PDF.
    :param request: HttpRequest
    :return: HttpResponse
    """
    # get date filter params
    dates = utils.get_sf_dates(request.GET)
    sf_date_start = dates['sf_date_start']
    sf_date_end = dates['sf_date_end']

    # get calendar filter params
    selected_calendar_set = set(request.GET.getlist('sf_c'))
    selected_calendar_list = []
    for c in get_objects_for_user(
            request.user, 'planners.read_calendar', use_groups=False, accept_global_perms=False):
        if not c.row_deleted and str(c.pk) in selected_calendar_set:
            selected_calendar_list.append(c)

    # get data columns filter params
    selected_data_set = set(request.GET.getlist('sf_d'))
    selected_data_dict = {
        e[0]: e[1]
        for e in (
            ('is_confirmed', 'Estado'),
            ('subtype', 'Tipo'),
            ('occasion', 'Ocasión'),
            ('topic', 'Tema'),
            ('bible_ref', 'Texto'),
        )
        if e[0] in selected_data_set
    }
    selected_data_dict.update(
        {e[0]: e[1] for e in EventResponsibility.RESPONSIBILITY_TASKS if e[0] in selected_data_set})

    event_list = []
    for c in selected_calendar_list:
        qs = EventDetail.objects.filter(event__calendar=c, event__row_deleted=False,
                                        row_time_end__isnull=True)
        qs = qs.filter(start_datetime__range=(dates['sf_date_start'], dates['sf_date_end']))

        for e in qs:
            event_list.append(e)

    event_list = sorted(event_list, key=lambda x: x.start_datetime)

    return pdf_reports.make_pdf(
        request.user,
        *pdf_reports.get_events_story(
            sf_date_start, sf_date_end, selected_calendar_list, selected_data_dict, event_list),
        as_portrait=False)


@login_required()
def events_list(request, calendar_id):
    """
    List events of calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eventos del calendario', ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    if not request.user.has_perms(('planners.read_calendar', ), ctx['calendar']):
        raise PermissionDenied

    qs = EventDetail.objects.filter(event__calendar=ctx['calendar'], event__row_deleted=False,
                                    row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(summary__icontains=ctx['sf_text'])
    q_t2 = Q(description__icontains=ctx['sf_text'])
    q_t3 = Q(location__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3)

    ctx.update(utils.get_sf_dates(request.GET))
    qs = qs.filter(start_datetime__range=(ctx['sf_date_start'], ctx['sf_date_end']))

    if request.GET.get('sf_status', '') == 'C':
        ctx['sf_status'] = 'C'
        qs = qs.filter(is_confirmed=True)
    elif request.GET.get('sf_status', '') == 'T':
        ctx['sf_status'] = 'T'
        qs = qs.filter(is_confirmed=False)
    else:
        ctx['sf_status'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('summary', 'start_datetime')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('start_datetime')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_status={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_status'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('planners:events-read', args=[ctx['calendar'].pk, e.event.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'planners/event_list.html', ctx)


@login_required()
def events_list_deleted(request, calendar_id):
    """
    List deleted events.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eventos', reverse('planners:events-list', args=[calendar_id])),
        ('Eventos eliminados', ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_events'),
                                  ctx['calendar']):
        raise PermissionDenied

    qs = EventDetail.objects.filter(event__calendar=ctx['calendar'], event__row_deleted=True,
                                    row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(summary__icontains=ctx['sf_text'])
    q_t2 = Q(description__icontains=ctx['sf_text'])
    q_t3 = Q(location__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3)

    ctx.update(utils.get_sf_dates(request.GET))
    qs = qs.filter(start_datetime__range=(ctx['sf_date_start'], ctx['sf_date_end']))

    if request.GET.get('sf_status', '') == 'C':
        ctx['sf_status'] = 'C'
        qs = qs.filter(is_confirmed=True)
    elif request.GET.get('sf_status', '') == 'T':
        ctx['sf_status'] = 'T'
        qs = qs.filter(is_confirmed=False)
    else:
        ctx['sf_status'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('start_datetime')
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
        qs = qs.order_by('summary', 'start_datetime')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_status={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_status'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('planners:events-read', args=[ctx['calendar'].pk, e.event.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'planners/event_deleted_list.html', ctx)


@login_required()
def events_create(request, calendar_id):
    """
    Create an event.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear nuevo evento')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eventos', reverse('planners:events-list', args=[calendar_id])),
        ('Crear nuevo evento', ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_events'),
                                  ctx['calendar']):
        raise PermissionDenied

    ctx['CRUD'] = 'C'
    ctx['event'] = Event()
    ctx['event'].calendar = ctx['calendar']

    # determines the visible fields in the form
    ctx['event'].type = '01'
    if request.GET.get('event_type', '') in [e[0] for e in Event.EVENT_TYPES]:
        ctx['event'].type = request.GET.get('event_type')

    return _events_create_or_update(request, ctx)


@login_required()
def events_read(request, calendar_id, event_id):
    """
    Read an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles del evento')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eventos', reverse('planners:events-list', args=[calendar_id])),
        ('Evento {}'.format(event_id), ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    user_calendar_perms = get_perms(request.user, ctx['calendar'])
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied

    ctx['CRUD'] = 'R'
    ctx['event'] = get_object_or_404(Event, pk=event_id)
    ctx['current_details'] = ctx['event'].current_details
    ctx['history_list'] = ctx['event'].details.order_by('-row_time_start')[:5]
    ctx['person_list'] = sorted(
        [{'pk': e.pk, 'name': str(e)} for e in Person.objects.filter(row_deleted=False)],
        key=lambda x: x['name'])

    if 'manage_events' in user_calendar_perms:
        ctx['responsibility_tasks'] = EventResponsibility.RESPONSIBILITY_TASKS
    else:
        # add only responsibilities for which the user has permissions
        ctx['responsibility_tasks'] = []
        for e in EventResponsibility.RESPONSIBILITY_TASKS:
            if 'change_responsibilities_{}'.format(e[0]) in user_calendar_perms:
                ctx['responsibility_tasks'].append(e)

    ctx['event'].can_be_updated = False
    if not ctx['event'].row_deleted and ctx['current_details'].start_datetime >= tz.now():
        ctx['event'].can_be_updated = True

    if ctx['event'].row_deleted:
        messages.error(request, 'Este evento se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            5, ('Eventos eliminados', reverse('planners:events-list-deleted', args=[calendar_id])))

    return render(request, 'planners/event_form.html', ctx)


@login_required()
def events_update(request, calendar_id, event_id):
    """
    Update an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar evento')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eventos', reverse('planners:events-list', args=[calendar_id])),
        ('Evento {}'.format(event_id), reverse(
            'planners:events-read', args=[calendar_id, event_id])),
        ('Modificar evento', ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_events'),
                                  ctx['calendar']):
        raise PermissionDenied

    ctx['CRUD'] = 'U'
    ctx['event'] = get_object_or_404(Event, pk=event_id, row_deleted=False)
    ctx['current_details'] = ctx['event'].current_details

    if request.user.username == settings.ADMIN_USERNAME:
        ctx['calendar_list'] = Calendar.objects.filter(row_deleted=False)

    return _events_create_or_update(request, ctx)


@login_required()
def events_delete(request, calendar_id, event_id):
    """
    Delete an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar evento')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eventos', reverse('planners:events-list', args=[calendar_id])),
        ('Evento {}'.format(event_id), reverse(
            'planners:events-read', args=[calendar_id, event_id])),
        ('Eliminar evento', ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_events'),
                                  ctx['calendar']):
        raise PermissionDenied

    ctx['CRUD'] = 'D'
    ctx['event'] = get_object_or_404(Event, pk=event_id, row_deleted=False)
    ctx['current_details'] = ctx['event'].current_details

    ctx['event'].can_be_deleted = True
    if False:
        ctx['event'].can_be_deleted = False
        messages.error(request, 'Este evento no puede ser eliminado porque la eliminación no está '
                                'implementada todavía.')

    if request.method == 'POST' and ctx['event'].can_be_deleted:
        ctx['event'].do_delete(request.user)
        return redirect('planners:events-list', ctx['calendar'].pk)
    else:
        return render(request, 'planners/event_form.html', ctx)


@login_required()
def events_delete_undo(request, calendar_id, event_id):
    """
    Undo the delete of an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)

    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_events'), calendar):
        raise PermissionDenied

    event = get_object_or_404(Event, pk=event_id)

    if request.method == 'POST':
        if event.row_deleted:
            event.undo_delete(request.user)

    return redirect('planners:events-read', calendar.pk, event.pk)


@login_required()
def events_history(request, calendar_id, event_id):
    """
    View changes history of an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eventos', reverse('planners:events-list', args=[calendar_id])),
        ('Evento {}'.format(event_id), reverse(
            'planners:events-read', args=[calendar_id, event_id])),
        ('Historial del evento', ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_events'),
                                  ctx['calendar']):
        raise PermissionDenied

    ctx['event'] = get_object_or_404(Event, pk=event_id)
    qs = ctx['event'].details

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(row_action__icontains=ctx['sf_text'])
    q_t2 = Q(summary__icontains=ctx['sf_text'])
    q_t3 = Q(description__icontains=ctx['sf_text'])
    q_t4 = Q(location__icontains=ctx['sf_text'])
    q_t5 = Q(occasion__icontains=ctx['sf_text'])
    q_t6 = Q(topic__icontains=ctx['sf_text'])
    q_t7 = Q(bible_ref__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3 | q_t4 | q_t5 | q_t6 | q_t7)

    ctx.update(utils.get_sf_dates(request.GET))
    q_d1 = Q(row_time_start__range=(ctx['sf_date_start'], ctx['sf_date_end']))
    q_d2 = Q(start_datetime__range=(ctx['sf_date_start'], ctx['sf_date_end']))
    qs = qs.filter(q_d1 | q_d2)

    if request.GET.get('sf_status', '') == 'C':
        ctx['sf_status'] = 'C'
        qs = qs.filter(is_confirmed=True)
    elif request.GET.get('sf_status', '') == 'T':
        ctx['sf_status'] = 'T'
        qs = qs.filter(is_confirmed=False)
    else:
        ctx['sf_status'] = 'A'

    ctx['sf_order'] = '0'
    qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_status={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_status'],
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['event'].row_deleted:
        messages.error(request, 'Este evento se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            5, ('Eventos eliminados', reverse('planners:events-list-deleted', args=[calendar_id])))

    return render(request, 'planners/event_history_list.html', ctx)


@login_required()
def events_sync(request, calendar_id, event_id):
    """
    Mark an event for synchronization with Google Calendar.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id)
    if request.user.username != settings.ADMIN_USERNAME:
        raise PermissionDenied

    event = get_object_or_404(Event, pk=event_id, row_deleted=False)

    if request.method == 'POST':
        if not event.google_sync_pending:
            event.google_sync_pending = True
            event.save()

    return redirect('planners:events-read', calendar.pk, event.pk)


@login_required()
def events_responsibilities_history(request, calendar_id, event_id):
    """
    View changes history of event responsibilities.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eventos', reverse('planners:events-list', args=[calendar_id])),
        ('Evento {}'.format(event_id), reverse(
            'planners:events-read', args=[calendar_id, event_id])),
        ('Historial de responsabilidades', ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    user_calendar_perms = get_perms(request.user, ctx['calendar'])
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied

    any_part_perm = False
    for s in user_calendar_perms:
        if 'change_responsibilities_' in s:
            any_part_perm = True
    if not ('manage_events' in user_calendar_perms or any_part_perm):
        raise PermissionDenied

    ctx['event'] = get_object_or_404(Event, pk=event_id)
    qs = ctx['event'].responsibilities

    ctx['sf_text'] = request.GET.get('sf_text', '')

    ctx.update(utils.get_sf_dates(request.GET))
    q_d1 = Q(row_time_start__range=(ctx['sf_date_start'], ctx['sf_date_end']))
    q_d2 = Q(row_time_end__range=(ctx['sf_date_start'], ctx['sf_date_end']))
    qs = qs.filter(q_d1 | q_d2)

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('-row_time_end')
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    event_list = []
    for e in qs:
        if (not ctx['sf_text']
                or ctx['sf_text'].upper() in e.task_display.upper()
                or ctx['sf_text'].upper() in e.person_display.upper()
                or ctx['sf_text'].upper() in e.obs.upper()):
            event_list.append(e)

    if ctx['sf_order'] == '1':
        pass
    elif ctx['sf_order'] == '2':
        event_list = sorted(
            event_list,
            key=lambda x: (x.get_task_display(), str(x.person), x.row_time_start))
    else:
        pass

    paginator = Paginator(event_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['event'].row_deleted:
        messages.error(request, 'Este evento se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            5, ('Eventos eliminados', reverse('planners:events-list-deleted', args=[calendar_id])))

    return render(request, 'planners/event_responsibilities_history_list.html', ctx)


@login_required()
def events_responsibilities_add(request, calendar_id, event_id):
    """
    Add responsibilities to an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)

    user_calendar_perms = get_perms(request.user, calendar)
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied

    event = get_object_or_404(Event, pk=event_id, row_deleted=False)
    if event.current_details.start_datetime < tz.now():
        messages.error(request, 'No puede modificar eventos pasados.')
        return redirect('planners:events-read', calendar.pk, event.pk)

    if request.method == 'POST':
        r = EventResponsibility()
        r.event = event
        r.task = request.POST.get('task', '99')

        if not ('manage_events' in user_calendar_perms or r.needed_perm in user_calendar_perms):
            raise PermissionDenied

        r.task_open = request.POST.get('task_open', '')
        if request.POST.get('person', '--') != '--':
            r.person = get_object_or_404(Person, pk=request.POST.get('person'), row_deleted=False)
        else:
            r.person_open = request.POST.get('person_open', '')
        r.obs = request.POST.get('obs', '')
        r.row_user_start = request.user

        try:
            r.full_clean()
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return redirect('planners:events-read', calendar.pk, event.pk)

        if r.task == '99' and not r.task_open:
            messages.error(request, 'Debe especificar que otra responsabilidad será.')
            return redirect('planners:events-read', calendar.pk, event.pk)

        if not r.person and not r.person_open:
            messages.error(request, 'Debe especificar que otra persona será.')
            return redirect('planners:events-read', calendar.pk, event.pk)

        r.save()
        messages.success(request, 'Se ha agregado una responsabilidad al evento.')

    return redirect('planners:events-read', calendar.pk, event.pk)


@login_required()
def events_responsibilities_edit(request, calendar_id, event_id):
    """
    Edit responsibilities from an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)

    user_calendar_perms = get_perms(request.user, calendar)
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied

    event = get_object_or_404(Event, pk=event_id, row_deleted=False)
    if event.current_details.start_datetime < tz.now():
        messages.error(request, 'No puede modificar eventos pasados.')
        return redirect('planners:events-read', calendar.pk, event.pk)

    if request.method == 'POST':
        r_old = get_object_or_404(EventResponsibility, pk=request.POST.get('r_pk'))

        if not ('manage_events' in user_calendar_perms or r_old.needed_perm in user_calendar_perms):
            raise PermissionDenied

        r_new = EventResponsibility()
        r_new.event = r_old.event
        r_new.task = r_old.task
        r_new.task_open = r_old.task_open
        r_new.person = r_old.person
        r_new.person_open = r_old.person_open
        r_new.obs = request.POST.get('obs', '')
        r_new.row_user_start = request.user

        try:
            r_new.full_clean()
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return redirect('planners:events-read', calendar.pk, event.pk)

        r_new.save()

        r_old.row_time_end = tz.now()
        r_old.row_user_end = request.user
        r_old.save()
        messages.success(request, 'Se ha modificado una responsabilidad del evento.')

    return redirect('planners:events-read', calendar.pk, event.pk)


@login_required()
def events_responsibilities_remove(request, calendar_id, event_id):
    """
    Remove responsibilities from an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)

    user_calendar_perms = get_perms(request.user, calendar)
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied

    event = get_object_or_404(Event, pk=event_id, row_deleted=False)
    if event.current_details.start_datetime < tz.now():
        messages.error(request, 'No puede modificar eventos pasados.')
        return redirect('planners:events-read', calendar.pk, event.pk)

    if request.method == 'POST':
        r = get_object_or_404(EventResponsibility, pk=request.POST.get('r_pk'))

        if not ('manage_events' in user_calendar_perms or r.needed_perm in user_calendar_perms):
            raise PermissionDenied

        r.row_time_end = tz.now()
        r.row_user_end = request.user
        r.save()
        messages.success(request, 'Se ha quitado una responsabilidad al evento.')

    return redirect('planners:events-read', calendar.pk, event.pk)


@login_required()
def events_schedule_history(request, calendar_id, event_id):
    """
    View changes history of event schedule.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eventos', reverse('planners:events-list', args=[calendar_id])),
        ('Evento {}'.format(event_id), reverse(
            'planners:events-read', args=[calendar_id, event_id])),
        ('Historial del programa', ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    user_calendar_perms = get_perms(request.user, ctx['calendar'])
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied
    if not ('manage_events' in user_calendar_perms or 'change_schedule' in user_calendar_perms):
        raise PermissionDenied

    ctx['event'] = get_object_or_404(Event, pk=event_id)
    qs = ctx['event'].schedule

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(label__icontains=ctx['sf_text'])
    q_t2 = Q(person__icontains=ctx['sf_text'])
    q_t3 = Q(obs__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3)

    ctx.update(utils.get_sf_dates(request.GET))
    q_d1 = Q(row_time_start__range=(ctx['sf_date_start'], ctx['sf_date_end']))
    q_d2 = Q(row_time_end__range=(ctx['sf_date_start'], ctx['sf_date_end']))
    qs = qs.filter(q_d1 | q_d2)

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('-row_time_end')
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
        qs = qs.order_by('label', 'person', 'row_time_start')
    elif request.GET.get('sf_order', '') == '3':
        ctx['sf_order'] = '3'
        qs = qs.order_by('datetime', 'row_time_start')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['event'].row_deleted:
        messages.error(request, 'Este evento se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            5, ('Eventos eliminados', reverse('planners:events-list-deleted', args=[calendar_id])))

    return render(request, 'planners/event_schedule_history_list.html', ctx)


@login_required()
def events_schedule_add(request, calendar_id, event_id):
    """
    Add a schedule entry to an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)

    user_calendar_perms = get_perms(request.user, calendar)
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied
    if not ('manage_events' in user_calendar_perms or 'change_schedule' in user_calendar_perms):
        raise PermissionDenied

    event = get_object_or_404(Event, pk=event_id, row_deleted=False)
    if event.current_details.start_datetime < tz.now():
        messages.error(request, 'No puede modificar eventos pasados.')
        return redirect('planners:events-read', calendar.pk, event.pk)

    if request.method == 'POST':
        s = EventScheduleEntry()
        s.event = event
        s.datetime = None
        try:
            s.datetime = utils.parse_date('01-01-2000', raise_error=True).replace(
                hour=int(request.POST.get('hour', 0)),
                minute=int(request.POST.get('minute', 0)))
        except ValueError:
            pass

        s.label = request.POST.get('label', '')
        s.person = request.POST.get('person', '')
        s.obs = request.POST.get('obs', '')
        s.row_user_start = request.user

        try:
            s.full_clean()
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return redirect('planners:events-read', calendar.pk, event.pk)

        s.save()
        messages.success(request, 'Se ha agregado un punto al programa del evento.')

    return redirect('planners:events-read', calendar.pk, event.pk)


@login_required()
def events_schedule_edit(request, calendar_id, event_id):
    """
    Edit a schedule entry from an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)

    user_calendar_perms = get_perms(request.user, calendar)
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied
    if not ('manage_events' in user_calendar_perms or 'change_schedule' in user_calendar_perms):
        raise PermissionDenied

    event = get_object_or_404(Event, pk=event_id, row_deleted=False)
    if event.current_details.start_datetime < tz.now():
        messages.error(request, 'No puede modificar eventos pasados.')
        return redirect('planners:events-read', calendar.pk, event.pk)

    if request.method == 'POST':
        s_old = get_object_or_404(EventScheduleEntry, pk=request.POST.get('s_pk'))

        s_new = EventScheduleEntry()
        s_new.event = s_old.event

        s_new.datetime = None
        try:
            s_new.datetime = utils.parse_date('01-01-2000', raise_error=True).replace(
                hour=int(request.POST.get('hour', 0)),
                minute=int(request.POST.get('minute', 0)))
        except ValueError:
            pass

        s_new.label = s_old.label
        s_new.person = s_old.person
        s_new.obs = request.POST.get('obs', '')
        s_new.row_user_start = request.user

        try:
            s_new.full_clean()
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return redirect('planners:events-read', calendar.pk, event.pk)

        s_new.save()

        s_old.row_time_end = tz.now()
        s_old.row_user_end = request.user
        s_old.save()
        messages.success(request, 'Se ha modificado un punto del programa del evento.')

    return redirect('planners:events-read', calendar.pk, event.pk)


@login_required()
def events_schedule_remove(request, calendar_id, event_id):
    """
    Remove a schedule entry from an event.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)

    user_calendar_perms = get_perms(request.user, calendar)
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied
    if not ('manage_events' in user_calendar_perms or 'change_schedule' in user_calendar_perms):
        raise PermissionDenied

    event = get_object_or_404(Event, pk=event_id, row_deleted=False)
    if event.current_details.start_datetime < tz.now():
        messages.error(request, 'No puede modificar eventos pasados.')
        return redirect('planners:events-read', calendar.pk, event.pk)

    if request.method == 'POST':
        s = get_object_or_404(EventScheduleEntry, pk=request.POST.get('s_pk'))
        s.row_time_end = tz.now()
        s.row_user_end = request.user
        s.save()
        messages.success(request, 'Se ha quitado un punto al programa del evento.')

    return redirect('planners:events-read', calendar.pk, event.pk)


@login_required()
def events_schedule_pdf(request, calendar_id, event_id):
    """
    Event schedule as PDF.
    :param request: HttpRequest
    :param calendar_id:
    :param event_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)

    user_calendar_perms = get_perms(request.user, calendar)
    if 'read_calendar' not in user_calendar_perms:
        raise PermissionDenied

    event = get_object_or_404(Event, pk=event_id, row_deleted=False)

    return pdf_reports.make_pdf(request.user, *pdf_reports.get_schedule_story(event))


def _events_create_or_update(request, ctx):
    """
    Create or update an event.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    t = tz.localtime(tz.now())
    ctx['tz_today'] = t.replace(hour=9, minute=0, second=0, microsecond=0)
    ctx['tz_start'] = t.replace(hour=0, minute=0, second=0, microsecond=0)
    ctx['tz_end'] = ctx['tz_start'] + datetime.timedelta(days=365)

    ctx['event_detail_subtypes'] = EventDetail.EVENT_DETAIL_SUBTYPES

    if request.method == 'POST':
        new_data = dict()
        new_data['summary'] = request.POST.get('summary', '')
        new_data['description'] = request.POST.get('description', '')
        new_data['location'] = request.POST.get('location', '')

        if request.POST.get('status', '') == 'C':
            new_data['is_confirmed'] = True
        else:
            new_data['is_confirmed'] = False

        try:
            new_data['start_datetime'] = utils.parse_date(
                request.POST.get('start_date', ''), raise_error=True).replace(
                    hour=int(request.POST.get('start_time_h', 0)),
                    minute=int(request.POST.get('start_time_m', 0)))
        except ValueError:
            messages.error(request, 'El formato de la fecha de inicio no es válido.')
            return render(request, 'planners/event_form.html', ctx)

        if not (ctx['tz_start'] <= new_data['start_datetime'] <= ctx['tz_end']):
            messages.error(request, 'La fecha de inicio debe estar dentro del rango permitido '
                                    '({} al {}).'.format(ctx['tz_start'].strftime('%d-%m-%Y'),
                                                         ctx['tz_end'].strftime('%d-%m-%Y')))
            return render(request, 'planners/event_form.html', ctx)

        if ctx['event'].type == '02':
            try:
                new_data['end_datetime'] = utils.parse_date(
                    request.POST.get('end_date', ''), raise_error=True)
            except ValueError:
                messages.error(request, 'El formato de la fecha final no es válido.')
                return render(request, 'planners/event_form.html', ctx)

            if not (ctx['tz_start'] <= new_data['end_datetime'] <= ctx['tz_end']):
                messages.error(request, 'La fecha final debe estar dentro del rango permitido '
                                        '({} al {}).'.format(ctx['tz_start'].strftime('%d-%m-%Y'),
                                                             ctx['tz_end'].strftime('%d-%m-%Y')))
                return render(request, 'planners/event_form.html', ctx)

            if new_data['end_datetime'] <= new_data['start_datetime']:
                messages.error(request, 'La fecha final debe ser posterior a la fecha inicial.')
                return render(request, 'planners/event_form.html', ctx)

        new_data['subtype'] = request.POST.get('subtype', '99')
        new_data['occasion'] = request.POST.get('occasion', '')
        new_data['topic'] = request.POST.get('topic', '')
        new_data['bible_ref'] = request.POST.get('bible_ref', '')

        try:
            if ctx['CRUD'] == 'C':
                ctx['event'].do_create(user=request.user, **new_data)
            else:
                ctx['event'].do_update(user=request.user, **new_data)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'planners/event_form.html', ctx)

        if ctx['CRUD'] == 'C' and request.POST.get('clone_flag', '') == 'True':
            try:
                clone_interval = int(request.POST.get('clone_interval', 0))
            except ValueError:
                clone_interval = 0

            if 1 <= clone_interval <= 30:
                try:
                    clone_date_end = utils.parse_date(
                        request.POST.get('clone_date_end', ''), raise_error=True)
                except ValueError:
                    clone_date_end = None

                if clone_date_end and clone_date_end <= ctx['tz_end']:
                    i = 0
                    delta = datetime.timedelta(days=clone_interval)
                    new_data['is_confirmed'] = False
                    new_data['start_datetime'] += delta
                    if ctx['event'].type == '02':
                        new_data['end_datetime'] += delta

                    while new_data['start_datetime'] <= clone_date_end:
                        new_event = Event()
                        new_event.calendar = ctx['calendar']
                        new_event.type = ctx['event'].type
                        try:
                            new_event.do_create(user=request.user, **new_data)
                            i += 1
                        except ValidationError:
                            pass

                        new_data['start_datetime'] += delta
                        if ctx['event'].type == '02':
                            new_data['end_datetime'] += delta

                    messages.success(request, 'Se han creado {} evento(s).'.format(i+1))

        if ctx['CRUD'] == 'U' and request.user.username == settings.ADMIN_USERNAME:
            # move event to other calendar
            try:
                ctx['calendar'] = Calendar.objects.get(pk=request.POST.get('new_calendar', 0),
                                                       row_deleted=False)
                ctx['event'].calendar = ctx['calendar']
                ctx['event'].save()
            except Calendar.DoesNotExist:
                pass

        return redirect('planners:events-read', ctx['calendar'].pk, ctx['event'].pk)
    else:
        return render(request, 'planners/event_form.html', ctx)
