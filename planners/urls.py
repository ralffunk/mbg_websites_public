# -*- coding: utf-8 -*-
"""
URL configuration.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.conf.urls import url, include
from planners import views, views_calendars, views_events


events_urlpatterns = [
    url(r'^$',
        views_events.events_list,
        name='events-list'),
    url(r'^deleted/$',
        views_events.events_list_deleted,
        name='events-list-deleted'),
    url(r'^create/$',
        views_events.events_create,
        name='events-create'),
    url(r'^(?P<event_id>\d+)/', include([
        url(r'^$',
            views_events.events_read,
            name='events-read'),
        url(r'^update/$',
            views_events.events_update,
            name='events-update'),
        url(r'^delete/$',
            views_events.events_delete,
            name='events-delete'),
        url(r'^delete/undo/$',
            views_events.events_delete_undo,
            name='events-delete-undo'),
        url(r'^history/$',
            views_events.events_history,
            name='events-history'),
        url(r'^sync/$',
            views_events.events_sync,
            name='events-sync'),
        url(r'^responsibilities/', include([
            url(r'^history/$',
                views_events.events_responsibilities_history,
                name='events-responsibilities-history'),
            url(r'^add/$',
                views_events.events_responsibilities_add,
                name='events-responsibilities-add'),
            url(r'^edit/$',
                views_events.events_responsibilities_edit,
                name='events-responsibilities-edit'),
            url(r'^remove/$',
                views_events.events_responsibilities_remove,
                name='events-responsibilities-remove'),
        ])),
        url(r'^schedule/', include([
            url(r'^history/$',
                views_events.events_schedule_history,
                name='events-schedule-history'),
            url(r'^add/$',
                views_events.events_schedule_add,
                name='events-schedule-add'),
            url(r'^edit/$',
                views_events.events_schedule_edit,
                name='events-schedule-edit'),
            url(r'^remove/$',
                views_events.events_schedule_remove,
                name='events-schedule-remove'),
            url(r'^pdf/$',
                views_events.events_schedule_pdf,
                name='events-schedule-pdf'),
        ])),
    ])),
]


calendar_urlpatterns = [
    url(r'^$',
        views_calendars.calendars_read,
        name='calendars-read'),
    url(r'^update/$',
        views_calendars.calendars_update,
        name='calendars-update'),
    url(r'^delete/$',
        views_calendars.calendars_delete,
        name='calendars-delete'),
    url(r'^delete/undo/$',
        views_calendars.calendars_delete_undo,
        name='calendars-delete-undo'),
    url(r'^history/$',
        views_calendars.calendars_history,
        name='calendars-history'),
    url(r'^sync/$',
        views_calendars.calendars_sync,
        name='calendars-sync'),
    url(r'^assign-perms/$',
        views_calendars.calendars_assign_perms,
        name='calendars-assign-perms'),
    url(r'^add-user/$',
        views_calendars.calendars_add_user,
        name='calendars-add-user'),
    url(r'^remove-user/$',
        views_calendars.calendars_remove_user,
        name='calendars-remove-user'),
    url(r'^events/', include(events_urlpatterns)),
]


calendars_urlpatterns = [
    url(r'^$',
        views_calendars.calendars_list,
        name='calendars-list'),
    url(r'^deleted/$',
        views_calendars.calendars_list_deleted,
        name='calendars-list-deleted'),
    url(r'^create/$',
        views_calendars.calendars_create,
        name='calendars-create'),
    url(r'^(?P<calendar_id>\d+)/', include(calendar_urlpatterns)),
]


urlpatterns = [
    url(r'^$',
        views.planners_start_page,
        name='start-page'),
    url(r'^calendars/', include(calendars_urlpatterns)),
    url(r'^events/', include([
        url(r'^$',
            views_events.events_list_all,
            name='events-list-all'),
        url(r'^editable/$',
            views_events.events_list_editable,
            name='events-editable'),
        url(r'^editable/pdf/$',
            views_events.events_list_editable_pdf,
            name='events-editable-pdf'),
    ])),
    url(r'^events/json/$',
        views.events_json,
        name='events-json'),
    url(r'^user-colors/$',
        views.user_colors,
        name='user-colors'),
    url(r'^user-responsibilities/$',
        views.user_responsibilities,
        name='user-responsibilities'),
]
