(function () {  // start of closure
    $(document).ready(function () {
        // function that fills the inputs of the form
        function submit_form(assignment_type_content, button_elem) {
            $('#assignment_type').val(assignment_type_content);
            $('#user_pk').val(button_elem.data('user'));
            $('#perm_pk').val(button_elem.data('perm'));
            document.perm_assign_form.submit();
        }

        // assign event handler for permission assignment
        $('.my-click-assign').click(function () {
            submit_form('assign', $(this));
        });
        $('.my-click-unassign').click(function () {
            submit_form('unassign', $(this));
        });

        // set focus when displaying modal
        $('#m_addUser').on('shown.bs.modal', function () {
            $('#m_addUser_select').focus();
        });

        // reset inputs before displaying modal
        $('#m_removeUser').on('show.bs.modal', function (event) {
            $('#m_removeUser_pk').val($(event.relatedTarget).data('user-pk'));
            $('#m_removeUser_username').val($(event.relatedTarget).data('user-dsc'));
        });

        // set focus when displaying modal
        $('#m_removeUser').on('shown.bs.modal', function () {
            $('#m_removeUser_confirmButton').focus();
        });
    });
})();   // end of closure
