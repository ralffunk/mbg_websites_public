(function () {  // start of closure
    $(document).ready(function () {
        // function that updates 'readonly' properties of the inputs in the modal
        function update_modal_inputs() {
            var task_select = $('#m_addResponsibility_task');
            var task_open_input = $('#m_addResponsibility_taskOpen');
            var person_select = $('#m_addResponsibility_person');
            var person_open_input = $('#m_addResponsibility_personOpen');

            if(task_select.val() == '99') {
                task_open_input.prop('readonly', null);
            } else {
                task_open_input.prop('readonly', 'true');
            }

            if(person_select.val() == '--') {
                person_open_input.prop('readonly', null);
            } else {
                person_open_input.prop('readonly', 'true');
            }
        }

        // event: change select
        $('#m_addResponsibility_task').change(function () {
            update_modal_inputs();
        });
        $('#m_addResponsibility_person').change(function () {
            update_modal_inputs();
        });

        // reset inputs before displaying modals
        $('#m_addResponsibility').on('show.bs.modal', function (event) {
            update_modal_inputs();
        });
        $('#m_editResponsibility').on('show.bs.modal', function (event) {
            $('#m_editResponsibility_pk').val($(event.relatedTarget).data('pk'));
            $('#m_editResponsibility_obs').val($(event.relatedTarget).data('obs'));
        });
        $('#m_removeResponsibility').on('show.bs.modal', function (event) {
            $('#m_removeResponsibility_pk').val($(event.relatedTarget).data('pk'));
        });
        $('#m_editSchedule').on('show.bs.modal', function (event) {
            $('#m_editSchedule_pk').val($(event.relatedTarget).data('pk'));
            $('#m_editSchedule_hour').val($(event.relatedTarget).data('hour'));
            $('#m_editSchedule_minute').val($(event.relatedTarget).data('minute'));
            $('#m_editSchedule_obs').val($(event.relatedTarget).data('obs'));
        });
        $('#m_removeSchedule').on('show.bs.modal', function (event) {
            $('#m_removeSchedule_pk').val($(event.relatedTarget).data('pk'));
        });

        // set focus when displaying modals
        $('#m_addResponsibility').on('shown.bs.modal', function () {
            $('#m_addResponsibility_task').focus();
        });
        $('#m_editResponsibility').on('shown.bs.modal', function () {
            $('#m_editResponsibility_obs').focus();
        });
        $('#m_removeResponsibility').on('shown.bs.modal', function () {
            $('#m_removeResponsibility_confirmButton').focus();
        });
        $('#m_addSchedule').on('shown.bs.modal', function () {
            $('#m_addSchedule_hour').focus();
        });
        $('#m_editSchedule').on('shown.bs.modal', function () {
            $('#m_editSchedule_hour').focus();
        });
        $('#m_removeSchedule').on('shown.bs.modal', function () {
            $('#m_removeSchedule_confirmButton').focus();
        });
    });
})();   // end of closure
