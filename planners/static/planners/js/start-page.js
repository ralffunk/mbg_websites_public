(function () {  // start of closure
    $(document).ready(function () {
        // create full calendar
        $('#calendar').fullCalendar({
            customButtons: {
                calendarListModal: {
                    text: 'Calendarios visibles',
                    click: function () {
                        $('#m_calendarList').modal('show');
                    }
                }
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'calendarListModal'
            },
            aspectRatio: 2,
            timeFormat: 'H:mm',
            columnFormat: 'dddd',
            allDayDefault: false,
            events: {
                url: $('#calendar').data('event-source'),
                data: function () {
                    var arr = [];
                    $('input:checked').each(function () {
                        arr.push(this.value);
                    });
                    return {
                        calendar_list: arr.join('-')
                    };
                }
            },
            eventRender: function (event, element) {
                // add event title as tooltip text, in case the title is truncated
                element.prop('title', event.title);
            }
        });

        // create color pickers in modal
        $('.my-colorpicker').each(function () {
            $(this).colorpicker({
                'format': 'rgb'
            });
        });

        // re-fetch events on click
        $('input[type=checkbox]').click(function () {
            $('#calendar').fullCalendar('refetchEvents');
        });
    });
})();   // end of closure
