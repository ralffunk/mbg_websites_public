# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User, Permission
from django.core.exceptions import ValidationError, PermissionDenied
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from guardian.shortcuts import assign_perm, remove_perm, get_perms
from mbg_websites import utils
from planners.models import Calendar, CalendarDetail


@login_required()
def calendars_list(request):
    """
    List calendars.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', ),
    ]

    qs = CalendarDetail.objects.filter(calendar__row_deleted=False, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(summary__icontains=ctx['sf_text'])
    q_t2 = Q(description__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2)

    if request.GET.get('sf_visibility', '') == 'P':
        ctx['sf_visibility'] = 'P'
        qs = qs.filter(is_public=True)
    elif request.GET.get('sf_visibility', '') == 'NP':
        ctx['sf_visibility'] = 'NP'
        qs = qs.filter(is_public=False)
    else:
        ctx['sf_visibility'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('is_public', 'summary')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('summary')

    ctx['sf'] = 'sf_text={}&sf_visibility={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_visibility'],
        ctx['sf_order'])

    include_all = request.user.has_perm('accounts.list_calendars')
    calendar_list = []
    for e in qs:
        if include_all or request.user.has_perm('planners.read_calendar', e.calendar):
            e.url = reverse('planners:calendars-read', args=[e.calendar.pk])
            calendar_list.append(e)

    paginator = Paginator(calendar_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'planners/calendar_list.html', ctx)


@login_required()
@permission_required('accounts.delete_calendar', raise_exception=True)
def calendars_list_deleted(request):
    """
    List deleted calendars.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendarios eliminados', ),
    ]

    qs = CalendarDetail.objects.filter(calendar__row_deleted=True, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(summary__icontains=ctx['sf_text'])
    q_t2 = Q(description__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2)

    if request.GET.get('sf_visibility', '') == 'P':
        ctx['sf_visibility'] = 'P'
        qs = qs.filter(is_public=True)
    elif request.GET.get('sf_visibility', '') == 'NP':
        ctx['sf_visibility'] = 'NP'
        qs = qs.filter(is_public=False)
    else:
        ctx['sf_visibility'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('summary')
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
        qs = qs.order_by('is_public', 'summary')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_visibility={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_visibility'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('planners:calendars-read', args=[e.calendar.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'planners/calendar_deleted_list.html', ctx)


@login_required()
@permission_required('accounts.create_calendar', raise_exception=True)
def calendars_create(request):
    """
    Create a calendar.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear nuevo calendario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Crear nuevo calendario', ),
    ]
    ctx['calendar'] = Calendar()

    if request.method == 'POST':
        if request.POST.get('visibility', '') == 'PU':
            is_public = True
        else:
            is_public = False

        try:
            ctx['calendar'].do_create(
                request.POST.get('summary', ''),
                request.POST.get('description', ''),
                is_public,
                request.user)

            if request.user.username != settings.ADMIN_USERNAME:
                # the creating user gets all perms for this calendar
                assign_perm('read_calendar', request.user, ctx['calendar'])
                for p in _get_ordered_perms():
                    assign_perm(p['codename'], request.user, ctx['calendar'])
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'planners/calendar_creation_form.html', ctx)

        return redirect('planners:calendars-read', ctx['calendar'].pk)
    else:
        return render(request, 'planners/calendar_creation_form.html', ctx)


@login_required()
def calendars_read(request, calendar_id):
    """
    Read a calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles del calendario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Detalles del calendario', ),
    ]
    ctx['CRUD'] = 'R'
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id)

    if not request.user.has_perm('accounts.list_calendars'):
        if not request.user.has_perms(('planners.read_calendar', ), ctx['calendar']):
            raise PermissionDenied

    ctx['current_details'] = ctx['calendar'].current_details
    ctx['history_list'] = ctx['calendar'].details.order_by('-row_time_start')[:5]
    ctx['permission_list'] = _get_ordered_perms()

    ctx['assigned_user_list'] = []
    ctx['unassigned_user_list'] = []
    for u in User.objects.exclude(username=settings.ADMIN_USERNAME):
        e = {
            'pk': u.pk,
            'dsc': str(u.profile),
            'calendar_permissions': get_perms(u, ctx['calendar']),
        }
        if e['calendar_permissions']:
            ctx['assigned_user_list'].append(e)
        else:
            ctx['unassigned_user_list'].append(e)
    ctx['assigned_user_list'] = sorted(ctx['assigned_user_list'], key=lambda x: x['dsc'])
    ctx['unassigned_user_list'] = sorted(ctx['unassigned_user_list'], key=lambda x: x['dsc'])

    if ctx['calendar'].row_deleted:
        messages.error(request, 'Este calendario se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            3, ('Calendarios eliminados', reverse('planners:calendars-list-deleted')))

    return render(request, 'planners/calendar_form.html', ctx)


@login_required()
def calendars_update(request, calendar_id):
    """
    Update a calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar calendario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Modificar calendario', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['current_details'] = ctx['calendar'].current_details
    ctx['breadcrumb'][3] = (str(ctx['current_details']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_calendar'),
                                  ctx['calendar']):
        raise PermissionDenied

    if request.method == 'POST':
        if request.POST.get('visibility', '') == 'PU':
            is_public = True
        else:
            is_public = False

        try:
            ctx['calendar'].do_update(
                request.POST.get('summary', ''),
                request.POST.get('description', ''),
                is_public,
                request.user)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'planners/calendar_form.html', ctx)

        return redirect('planners:calendars-read', ctx['calendar'].pk)
    else:
        return render(request, 'planners/calendar_form.html', ctx)


@login_required()
def calendars_delete(request, calendar_id):
    """
    Delete a calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar calendario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Eliminar calendario', ),
    ]
    ctx['CRUD'] = 'D'
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    ctx['current_details'] = ctx['calendar'].current_details
    ctx['breadcrumb'][3] = (str(ctx['current_details']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_calendar'),
                                  ctx['calendar']):
        raise PermissionDenied

    ctx['calendar'].can_be_deleted = True

    if request.method == 'POST' and ctx['calendar'].can_be_deleted:
        ctx['calendar'].do_delete(request.user)
        return redirect('planners:calendars-list')
    else:
        return render(request, 'planners/calendar_form.html', ctx)


@login_required()
def calendars_delete_undo(request, calendar_id):
    """
    Undo the delete of a calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id)

    if not request.user.has_perm('accounts.delete_calendar'):
        raise PermissionDenied

    if request.method == 'POST':
        if calendar.row_deleted:
            calendar.undo_delete(request.user)

    return redirect('planners:calendars-read', calendar.pk)


@login_required()
def calendars_history(request, calendar_id):
    """
    View changes history of a calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Calendario', reverse('planners:start-page')),
        ('Lista de calendarios', reverse('planners:calendars-list')),
        ('Calendario', '#'),
        ('Historial del calendario', ),
    ]
    ctx['calendar'] = get_object_or_404(Calendar, pk=calendar_id)
    ctx['breadcrumb'][3] = (str(ctx['calendar']), reverse(
        'planners:calendars-read', args=[calendar_id]))

    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_calendar'),
                                  ctx['calendar']):
        raise PermissionDenied

    qs = ctx['calendar'].details

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(row_action__icontains=ctx['sf_text'])
    q_t2 = Q(summary__icontains=ctx['sf_text'])
    q_t3 = Q(description__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3)

    ctx.update(utils.get_sf_dates(request.GET))
    qs = qs.filter(row_time_start__range=(ctx['sf_date_start'], ctx['sf_date_end']))

    if request.GET.get('sf_visibility', '') == 'P':
        ctx['sf_visibility'] = 'P'
        qs = qs.filter(is_public=True)
    elif request.GET.get('sf_visibility', '') == 'NP':
        ctx['sf_visibility'] = 'NP'
        qs = qs.filter(is_public=False)
    else:
        ctx['sf_visibility'] = 'A'

    ctx['sf_order'] = '0'
    qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_visibility={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_visibility'],
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['calendar'].row_deleted:
        messages.error(request, 'Este calendario se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            3, ('Calendarios eliminados', reverse('people:people-list-deleted')))

    return render(request, 'planners/calendar_history_list.html', ctx)


@login_required()
def calendars_sync(request, calendar_id):
    """
    Mark a calendar for synchronization with Google Calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    if request.user.username != settings.ADMIN_USERNAME:
        raise PermissionDenied

    if request.method == 'POST':
        if not calendar.google_sync_pending or not calendar.google_last_sync:
            calendar.google_sync_pending = True
            calendar.save()

    return redirect('planners:calendars-read', calendar.pk)


@login_required()
def calendars_assign_perms(request, calendar_id):
    """
    Modify user permission assignment for a calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_calendar'), calendar):
        raise PermissionDenied

    if request.method == 'POST':
        u = get_object_or_404(User, pk=request.POST.get('user'))
        p = get_object_or_404(Permission, content_type__model='calendar',
                              codename=request.POST.get('perm'))

        if p.codename == 'manage_calendar' and u == request.user:
            messages.error(request, 'Usted no se puede quitar a si mismo el permiso "{}".'.format(
                p.name))
        elif p.codename != 'read_calendar' and u.username != settings.ADMIN_USERNAME:
            if request.POST.get('assignment_type') == 'assign':
                assign_perm(p.codename, u, calendar)
                messages.success(request, 'Se ha asignado el permiso "{}" al usuario "{}".'.format(
                    p.name, u.profile))
            elif request.POST.get('assignment_type') == 'unassign':
                remove_perm(p.codename, u, calendar)
                messages.success(request, 'Se ha quitado el permiso "{}" al usuario "{}".'.format(
                    p.name, u.profile))

    return redirect('planners:calendars-read', calendar.pk)


@login_required()
def calendars_add_user(request, calendar_id):
    """
    Add an user to a calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_calendar'), calendar):
        raise PermissionDenied

    if request.method == 'POST':
        u = get_object_or_404(User, pk=request.POST.get('user'))

        if u.username != settings.ADMIN_USERNAME:
            assign_perm('read_calendar', u, calendar)
            messages.success(request, 'Se ha agregado el usuario "{}".'.format(u.profile))

    return redirect('planners:calendars-read', calendar.pk)


@login_required()
def calendars_remove_user(request, calendar_id):
    """
    Remove an user to a calendar.
    :param request: HttpRequest
    :param calendar_id:
    :return: HttpResponse
    """
    calendar = get_object_or_404(Calendar, pk=calendar_id, row_deleted=False)
    if not request.user.has_perms(('planners.read_calendar', 'planners.manage_calendar'), calendar):
        raise PermissionDenied

    if request.method == 'POST':
        u = get_object_or_404(User, pk=request.POST.get('user'))

        if u == request.user:
            messages.error(request, 'Usted no se puede quitar a si mismo del calendario.')
        elif u.username != settings.ADMIN_USERNAME:
            for codename in get_perms(u, calendar):
                remove_perm(codename, u, calendar)
            messages.success(request, 'Se ha quitado el usuario "{}".'.format(u.profile))

    return redirect('planners:calendars-read', calendar.pk)


def _get_ordered_perms():
    """
    Get permissions that are assigned on a per-object basis.
    :return:
    """
    ordered_perms = (
        'manage_calendar',
        'manage_events',
        'change_schedule',
        'change_responsibilities_01',
        'change_responsibilities_02',
        'change_responsibilities_03',
        'change_responsibilities_04',
        'change_responsibilities_05',
        'change_responsibilities_06',
        'change_responsibilities_07',
        'change_responsibilities_08',
        'change_responsibilities_99',
    )

    perms = []
    for name in ordered_perms:
        p = Permission.objects.get(content_type__model='calendar', codename=name)
        perms.append({'pk': p.pk, 'codename': p.codename, 'name': p.name})
    return perms
