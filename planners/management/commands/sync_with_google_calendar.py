# -*- coding: utf-8 -*-
"""
Management command.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import datetime
import httplib2
from apiclient import discovery
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone as tz
from googleapiclient.errors import HttpError
from mbg_websites.utils import get_google_credentials
from planners.models import Calendar, Event


LOG_TEMPLATE = '{:4}. {:7} {:1} {:4} "{}" | {}'


class Command(BaseCommand):
    help = 'Sync calendars and their events with Google Calendar.'

    def add_arguments(self, parser):
        # add named (optional) arguments
        parser.add_argument(
            '--mock', action='store_true', default=False, dest='mock',
            help='Only list the actions to be done, without making any changes.')

    def handle(self, *args, **options):
        self.stdout.write('Connecting to Google Calendar ...')

        credentials = get_google_credentials()
        if not credentials:
            raise CommandError('Invalid credentials')

        http = credentials.authorize(httplib2.Http())
        if not http:
            raise CommandError('Invalid http')

        service = discovery.build('calendar', 'v3', http=http)
        if not service:
            raise CommandError('Invalid service')

        self.stdout.write('Connection established successfully.')
        self.stdout.write('')
        self.stdout.write('Checking for calendars with pending synchronization ...')

        for i, c in enumerate(Calendar.objects.filter(google_sync_pending=True)):
            try:
                if options['mock']:
                    raise ValueError('mock')

                r = _sync_calendar_details(service, c)
                self.stdout.write(LOG_TEMPLATE.format(i+1, 'SUCCESS', 'c', c.pk, c, r))
            except (ValueError, HttpError) as err:
                self.stdout.write(LOG_TEMPLATE.format(i+1, 'ERROR', 'c', c.pk, c, err))

        self.stdout.write('Finished calendar synchronization.')
        self.stdout.write('')
        self.stdout.write('Checking for events with pending synchronization ...')

        for i, e in enumerate(Event.objects.filter(google_sync_pending=True)):
            try:
                if options['mock']:
                    raise ValueError('mock')

                r = _sync_event_details(service, e)
                self.stdout.write(LOG_TEMPLATE.format(i+1, 'SUCCESS', 'e', e.pk, e, r))
            except (ValueError, HttpError) as err:
                self.stdout.write(LOG_TEMPLATE.format(i+1, 'ERROR', 'e', e.pk, e, err))

        self.stdout.write('Finished event synchronization.')


def _sync_calendar_details(service, calendar):
    """
    Sync calendars.
    :param service:
    :param calendar:
    :return:
    """
    crud = ''
    det = calendar.current_details

    g_cal = None
    if calendar.google_id:
        # check if calendar actually exists
        try:
            g_cal = service.calendars().get(calendarId=calendar.google_id).execute()
        except HttpError:
            pass

    if g_cal:
        # there is an existing google calendar
        if calendar.row_deleted:
            # delete calendar
            crud = 'D'
            service.calendars().delete(calendarId=calendar.google_id).execute()
            calendar.google_id = ''
            calendar.google_last_sync = tz.now()
        else:
            # update calendar details
            crud = 'U'
            g_cal['summary'] = det.summary
            g_cal['description'] = det.description
            service.calendars().update(calendarId=calendar.google_id, body=g_cal).execute()

            _sync_calendar_visibility(service, calendar, det)

            calendar.google_last_sync = tz.now()
    else:
        # there is no existing google calendar
        if calendar.row_deleted:
            # assure that field 'google_id' is empty as well
            calendar.google_id = ''
        else:
            # create calendar
            crud = 'C'
            g_cal = {
                'summary': det.summary,
                'description': det.description,
                'location': 'Paraguay',
                'timeZone': settings.TIME_ZONE,
            }
            g_cal = service.calendars().insert(body=g_cal).execute()
            calendar.google_id = g_cal['id']

            _sync_calendar_visibility(service, calendar, det)

            calendar.google_last_sync = tz.now()

    calendar.google_sync_pending = False
    calendar.save()
    return crud


def _sync_calendar_visibility(service, calendar, details):
    """
    Sync calendar visibility.
    :param service:
    :param calendar:
    :param details:
    :return:
    """
    acl = service.acl().list(calendarId=calendar.google_id).execute()
    mbg_is_present = False
    public_is_present = False

    for g_rule in acl['items']:
        if g_rule['role'] == 'owner':
            pass
        elif g_rule['role'] == 'reader':
            if g_rule['scope']['type'] == 'user':
                if g_rule['scope']['value'] == settings.SERVER_EMAIL:
                    mbg_is_present = True
            elif g_rule['scope']['type'] == 'default':
                if details.is_public:
                    public_is_present = True
                else:
                    # this calendar is no longer public
                    service.acl().delete(
                        calendarId=calendar.google_id, ruleId=g_rule['id']).execute()
        else:
            service.acl().delete(calendarId=calendar.google_id, ruleId=g_rule['id']).execute()

    if not mbg_is_present:
        g_rule = {
            'role': 'reader',
            'scope': {
                'type': 'user',
                'value': settings.SERVER_EMAIL,
            },
        }
        service.acl().insert(calendarId=calendar.google_id, body=g_rule).execute()

    if details.is_public and not public_is_present:
        g_rule = {
            'role': 'reader',
            'scope': {
                'type': 'default',
            },
        }
        service.acl().insert(calendarId=calendar.google_id, body=g_rule).execute()


def _sync_event_details(service, event):
    """
    Sync events.
    :param service:
    :param event:
    :return:
    """
    if not event.calendar.google_id:
        # calendar was deleted or never synced
        crud = '*'
    else:
        id_ = 'event{}'.format(event.pk)
        det = event.current_details

        if event.row_deleted or not det.is_confirmed:
            # delete event, even if it never was synced
            try:
                crud = 'D'
                service.events().delete(calendarId=event.calendar.google_id, eventId=id_).execute()
            except HttpError:
                crud = '-'
        else:
            # create or update event
            try:
                crud = 'U'
                g_event = service.events().get(
                    calendarId=event.calendar.google_id, eventId=id_).execute()
            except HttpError:
                crud = 'C'
                g_event = {
                    'id': id_,
                }

            g_event['summary'] = det.summary
            g_event['description'] = det.description
            g_event['location'] = det.location
            g_event['status'] = 'confirmed'     # so that event shows up in calendar

            t = tz.localtime(det.start_datetime)
            if event.type == '01':
                g_event['start'] = {
                    'dateTime': t.strftime('%Y-%m-%dT%H:%M:%S%z'),
                }
                g_event['end'] = {
                    'dateTime': (t + datetime.timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S%z'),
                }
            elif event.type == '02':
                g_event['start'] = {
                    'date': t.strftime('%Y-%m-%d'),
                }
                g_event['end'] = {
                    'date': tz.localtime(det.end_datetime).strftime('%Y-%m-%d'),
                }

            if crud == 'C':
                service.events().insert(
                    calendarId=event.calendar.google_id, body=g_event).execute()
            else:
                service.events().update(
                    calendarId=event.calendar.google_id, eventId=id_, body=g_event).execute()

    event.google_last_sync = tz.now()
    event.google_sync_pending = False
    event.save()
    return crud
