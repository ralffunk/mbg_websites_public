# -*- coding: utf-8 -*-
"""
Models.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone as tz
from people.models import Person
from songs.models import Song


class Calendar(models.Model):
    """
    A calendar with version control.
    """
    google_id = models.CharField(max_length=200, blank=True, default='')
    google_last_sync = models.DateTimeField(null=True, blank=True)
    google_sync_pending = models.BooleanField(default=True)
    row_deleted = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()
        permissions = (
            ('read_calendar', 'ver calendario'),
            ('manage_calendar', 'administrar calendario'),
            ('manage_events', 'administrar eventos'),
            ('change_schedule', 'modificar programa'),
            ('change_responsibilities_01', 'asignar orador'),
            ('change_responsibilities_02', 'asignar conductor'),
            ('change_responsibilities_03', 'asignar líder de alabanza'),
            ('change_responsibilities_04', 'asignar equipo de alabanza'),
            ('change_responsibilities_05', 'asignar encargado de consola'),
            ('change_responsibilities_06', 'asignar encargado de proyección'),
            ('change_responsibilities_07', 'asignar ujier'),
            ('change_responsibilities_08', 'asignar traductor'),
            ('change_responsibilities_99', 'asignar otras responsabilidades'),
        )

    def __str__(self):
        det = self.current_details
        if det:
            return str(det)
        else:
            return 'Calendar(pk: {})'.format(self.pk)

    @property
    def current_details(self):
        return self.details.filter(row_time_end__isnull=True).order_by('-row_time_start').first()

    def do_create(self, summary, description, is_public, user):
        new_det = CalendarDetail()
        new_det.summary = summary
        new_det.description = description
        new_det.is_public = is_public
        new_det.row_action = 'CREATE'
        new_det.row_user_insert = user
        new_det.full_clean(exclude=['calendar'])

        if CalendarDetail.objects.filter(summary=summary, row_time_end__isnull=True,
                                         calendar__row_deleted=False).exists():
            raise ValidationError(
                {'summary': 'El título del calendario no puede ser duplicado.'})

        self.save()
        new_det.calendar = self
        new_det.save()

    def do_update(self, summary, description, is_public, user):
        new_det = CalendarDetail()
        new_det.calendar = self
        new_det.summary = summary
        new_det.description = description
        new_det.is_public = is_public
        new_det.row_action = 'UPDATE'
        new_det.row_user_insert = user
        new_det.full_clean()

        old_det = self.current_details
        if old_det:
            if not new_det.is_equal(old_det):
                if CalendarDetail.objects.exclude(pk=old_det.pk).filter(
                        summary=summary, row_time_end__isnull=True,
                        calendar__row_deleted=False).exists():
                    raise ValidationError(
                        {'summary': 'El título del calendario no puede ser duplicado.'})

                old_det.row_time_end = tz.now()
                old_det.save()
                new_det.save()
                self.google_sync_pending = True
                self.save()
        else:
            new_det.save()
            self.google_sync_pending = True
            self.save()

    def do_delete(self, user):
        new_det = CalendarDetail()
        new_det.calendar = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.summary = old_det.summary
            new_det.description = old_det.description
            new_det.is_public = old_det.is_public

        new_det.row_action = 'DELETE'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = True
        self.google_sync_pending = True
        self.save()

        for e in self.events.all():
            e.google_sync_pending = True
            e.save()

    def undo_delete(self, user):
        new_det = CalendarDetail()
        new_det.calendar = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.summary = old_det.summary
            new_det.description = old_det.description
            new_det.is_public = old_det.is_public

        new_det.row_action = 'UNDO_D'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = False
        self.google_sync_pending = True
        self.save()

        for e in self.events.all():
            e.google_sync_pending = True
            e.save()

    def get_event_qty(self):
        return '{} | {}'.format(
            self.events.filter(row_deleted=False).count(),
            self.events.filter(row_deleted=True).count())


class CalendarDetail(models.Model):
    """
    A calendar detail with version control.
    """
    calendar = models.ForeignKey(Calendar, related_name='details')
    summary = models.CharField(max_length=100, default='', error_messages={
        'blank': 'El nombre del calendario no puede quedar vacío.'})
    description = models.CharField(max_length=100, blank=True, default='')
    is_public = models.BooleanField(default=False)
    row_action = models.CharField(max_length=6, default='CREATE')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return '{}'.format(self.summary)

    def is_equal(self, other):
        if self.summary == other.summary:
            if self.description == other.description:
                if self.is_public == other.is_public:
                    return True
        return False


class Event(models.Model):
    """
    An event with version control.
    """
    EVENT_TYPES = (
        ('01', 'Programa normal'),
        ('02', 'Evento de varios días'),
    )
    calendar = models.ForeignKey(Calendar, related_name='events')
    type = models.CharField(max_length=2, default='01', choices=EVENT_TYPES)
    google_last_sync = models.DateTimeField(null=True, blank=True)
    google_sync_pending = models.BooleanField(default=True)
    row_deleted = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()

    def __str__(self):
        det = self.current_details
        if det:
            return str(det)
        else:
            return 'Event(pk: {})'.format(self.pk)

    @property
    def current_details(self):
        return self.details.filter(row_time_end__isnull=True).order_by('-row_time_start').first()

    @property
    def current_responsibilities(self):
        return self.responsibilities.filter(row_time_end__isnull=True).order_by(
            'task', 'task_open', 'person', 'person_open')

    @property
    def current_schedule(self):
        return self.schedule.filter(row_time_end__isnull=True).order_by('datetime')

    def do_create(self, summary, description, location, is_confirmed, start_datetime=None,
                  end_datetime=None, subtype='99', occasion='', topic='', bible_ref='', user=None):
        new_det = EventDetail()
        new_det.summary = summary
        new_det.description = description
        new_det.location = location
        new_det.is_confirmed = is_confirmed
        new_det.start_datetime = start_datetime
        new_det.end_datetime = end_datetime
        new_det.subtype = subtype
        new_det.occasion = occasion
        new_det.topic = topic
        new_det.bible_ref = bible_ref
        new_det.row_action = 'CREATE'
        new_det.row_user_insert = user
        new_det.full_clean(exclude=['event'])

        self.save()
        new_det.event = self
        new_det.save()

    def do_update(self, summary, description, location, is_confirmed, start_datetime=None,
                  end_datetime=None, subtype='99', occasion='', topic='', bible_ref='', user=None):
        new_det = EventDetail()
        new_det.event = self
        new_det.summary = summary
        new_det.description = description
        new_det.location = location
        new_det.is_confirmed = is_confirmed
        new_det.start_datetime = start_datetime
        new_det.end_datetime = end_datetime
        new_det.subtype = subtype
        new_det.occasion = occasion
        new_det.topic = topic
        new_det.bible_ref = bible_ref
        new_det.row_action = 'UPDATE'
        new_det.row_user_insert = user
        new_det.full_clean()

        old_det = self.current_details
        if old_det:
            if not new_det.is_equal(old_det):
                old_det.row_time_end = tz.now()
                old_det.save()
                new_det.save()
        else:
            new_det.save()

        self.google_sync_pending = True
        self.save()

    def do_delete(self, user):
        new_det = EventDetail()
        new_det.event = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.summary = old_det.summary
            new_det.description = old_det.description
            new_det.location = old_det.location
            new_det.is_confirmed = old_det.is_confirmed
            new_det.start_datetime = old_det.start_datetime
            new_det.end_datetime = old_det.end_datetime
            new_det.subtype = old_det.subtype
            new_det.occasion = old_det.occasion
            new_det.topic = old_det.topic
            new_det.bible_ref = old_det.bible_ref

        new_det.row_action = 'DELETE'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = True
        self.google_sync_pending = True
        self.save()

    def undo_delete(self, user):
        new_det = EventDetail()
        new_det.event = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.summary = old_det.summary
            new_det.description = old_det.description
            new_det.location = old_det.location
            new_det.is_confirmed = old_det.is_confirmed
            new_det.start_datetime = old_det.start_datetime
            new_det.end_datetime = old_det.end_datetime
            new_det.subtype = old_det.subtype
            new_det.occasion = old_det.occasion
            new_det.topic = old_det.topic
            new_det.bible_ref = old_det.bible_ref

        new_det.row_action = 'UNDO_D'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = False
        self.google_sync_pending = True
        self.save()

    def r01(self):
        return _get_responsibility(self, '01')

    def r02(self):
        return _get_responsibility(self, '02')

    def r03(self):
        return _get_responsibility(self, '03')

    def r04(self):
        return _get_responsibility(self, '04')

    def r05(self):
        return _get_responsibility(self, '05')

    def r06(self):
        return _get_responsibility(self, '06')

    def r07(self):
        return _get_responsibility(self, '07')

    def r08(self):
        return _get_responsibility(self, '08')

    def r99(self):
        return _get_responsibility(self, '99')


class EventDetail(models.Model):
    """
    An event detail with version control.
    """
    EVENT_DETAIL_SUBTYPES = (
        ('01', 'Sólo MBG'),
        ('02', 'Juntos - MBG organiza'),
        ('03', 'Juntos - MG organiza'),
        ('99', 'Otro'),
    )
    event = models.ForeignKey(Event, related_name='details')
    summary = models.CharField(max_length=100, default='', error_messages={
        'blank': 'El nombre del evento no puede quedar vacío.'})
    description = models.CharField(max_length=1000, blank=True, default='')
    location = models.CharField(max_length=100, blank=True, default='')
    is_confirmed = models.BooleanField(default=False)
    start_datetime = models.DateTimeField(null=True, blank=True)
    end_datetime = models.DateTimeField(null=True, blank=True)
    subtype = models.CharField(max_length=2, default='99', choices=EVENT_DETAIL_SUBTYPES)
    occasion = models.CharField(max_length=100, blank=True, default='')
    topic = models.CharField(max_length=100, blank=True, default='')
    bible_ref = models.CharField(max_length=100, blank=True, default='')
    row_action = models.CharField(max_length=6, default='CREATE')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return '{}'.format(self.summary)

    def is_equal(self, other):
        if self.summary == other.summary:
            if self.description == other.description:
                if self.location == other.location:
                    if self.is_confirmed == other.is_confirmed:
                        if self.start_datetime == other.start_datetime:
                            if self.end_datetime == other.end_datetime:
                                if self.subtype == other.subtype:
                                    if self.occasion == other.occasion:
                                        if self.topic == other.topic:
                                            if self.bible_ref == other.bible_ref:
                                                return True
        return False


class EventResponsibility(models.Model):
    """
    An event responsibility with version control.
    """
    RESPONSIBILITY_TASKS = (
        ('01', 'Orador'),
        ('02', 'Conductor'),
        ('03', 'Líder de alabanza'),
        ('04', 'Equipo de alabanza'),
        ('05', 'Encargado de consola'),
        ('06', 'Encargado de proyección'),
        ('07', 'Ujier'),
        ('08', 'Traductor'),
        ('99', 'Otro'),
    )
    event = models.ForeignKey(Event, related_name='responsibilities')
    task = models.CharField(max_length=2, default='99', choices=RESPONSIBILITY_TASKS)
    task_open = models.CharField(max_length=20, blank=True, default='')
    person = models.ForeignKey(Person, null=True, blank=True, on_delete=models.PROTECT)
    person_open = models.CharField(max_length=50, blank=True, default='')
    obs = models.CharField(max_length=1000, blank=True, default='')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_start = models.ForeignKey(
        User, null=True, blank=True, related_name='eventresponsibility_start_set',
        on_delete=models.PROTECT)
    row_user_end = models.ForeignKey(
        User, null=True, blank=True, related_name='eventresponsibility_end_set',
        on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return '{}: {}'.format(self.task_display, self.person_display)

    @property
    def task_display(self):
        if self.task != '99':
            return self.get_task_display()
        else:
            return self.task_open

    @property
    def person_display(self):
        if self.person:
            return str(self.person)
        else:
            return self.person_open

    @property
    def needed_perm(self):
        return 'change_responsibilities_{}'.format(self.task)


class EventScheduleEntry(models.Model):
    """
    An event schedule entry with version control.
    """
    event = models.ForeignKey(Event, related_name='schedule')
    datetime = models.DateTimeField(default=tz.now)
    label = models.CharField(max_length=20, blank=True, default='')
    person = models.CharField(max_length=50, blank=True, default='')
    obs = models.CharField(max_length=1000, blank=True, default='')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_start = models.ForeignKey(
        User, null=True, blank=True, related_name='eventscheduleentry_start_set',
        on_delete=models.PROTECT)
    row_user_end = models.ForeignKey(
        User, null=True, blank=True, related_name='eventscheduleentry_end_set',
        on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return '{} | {}: {}'.format(
            tz.localtime(self.datetime).strftime('%H:%M'), self.label, self.person)

    @property
    def current_song_list(self):
        return self.song_list_entries.filter(row_time_end__isnull=True).order_by('sequence')


class SongListEntry(models.Model):
    """
    An song list entry with version control, used to assign songs to an event schedule entry.
    """
    schedule_entry = models.ForeignKey(EventScheduleEntry, related_name='song_list_entries')
    sequence = models.IntegerField(default=0)
    song = models.ForeignKey(Song, null=True, blank=True, on_delete=models.PROTECT)
    obs = models.CharField(max_length=1000, blank=True, default='')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_start = models.ForeignKey(
        User, null=True, blank=True, related_name='songlistentry_start_set',
        on_delete=models.PROTECT)
    row_user_end = models.ForeignKey(
        User, null=True, blank=True, related_name='songlistentry_end_set',
        on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return '{}. {}'.format(self.sequence, self.song_display)

    @property
    def song_display(self):
        if self.song:
            return self.song.first_title
        else:
            return self.obs


class UserCalendarColor(models.Model):
    """
    Display color of calendar events for each user.
    """
    user = models.ForeignKey(User)
    calendar = models.ForeignKey(Calendar)
    color_r = models.IntegerField(default=0)
    color_g = models.IntegerField(default=0)
    color_b = models.IntegerField(default=0)
    row_time_update = models.DateTimeField(auto_now=True)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return 'rgb({},{},{})'.format(self.color_r, self.color_g, self.color_b)


def _get_responsibility(event, task):
    """
    Get responsibility assignments for the specified task.
    :param event:
    :param task:
    :return: s
    """
    qs = event.responsibilities.filter(row_time_end__isnull=True, task=task)
    for e in qs:
        e.dsc = e.person_display
    return sorted(qs, key=lambda x: x.dsc)
