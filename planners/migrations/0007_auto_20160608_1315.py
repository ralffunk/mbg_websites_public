# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('planners', '0006_auto_20160607_0715'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventscheduleentry',
            name='datetime',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
