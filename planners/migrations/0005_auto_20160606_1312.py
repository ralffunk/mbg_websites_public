# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planners', '0004_auto_20160606_1148'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendar',
            name='google_last_sync',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='google_last_sync',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
