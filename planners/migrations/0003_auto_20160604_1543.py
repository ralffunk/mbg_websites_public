# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planners', '0002_auto_20160604_1322'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='calendardetail',
            name='row_user',
        ),
        migrations.RemoveField(
            model_name='eventdetail',
            name='row_user',
        ),
        migrations.RemoveField(
            model_name='eventresponsibility',
            name='row_user',
        ),
        migrations.RemoveField(
            model_name='eventscheduleentry',
            name='row_user',
        ),
        migrations.RemoveField(
            model_name='songlistentry',
            name='row_user',
        ),
    ]
