# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('planners', '0007_auto_20160608_1315'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserCalendarColor',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('color_r', models.IntegerField(default=0)),
                ('color_g', models.IntegerField(default=0)),
                ('color_b', models.IntegerField(default=0)),
                ('row_time_update', models.DateTimeField(auto_now=True)),
                ('calendar', models.ForeignKey(to='planners.Calendar')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'default_permissions': (),
            },
        ),
    ]
