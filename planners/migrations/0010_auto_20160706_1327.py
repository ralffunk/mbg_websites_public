# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planners', '0009_auto_20160621_1228'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventdetail',
            name='subtype',
            field=models.CharField(max_length=2, choices=[('01', 'Sólo MBG'), ('02', 'Juntos - MBG organiza'), ('03', 'Juntos - MG organiza'), ('99', 'Otro')], default='99'),
        ),
    ]
