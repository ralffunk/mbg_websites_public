# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planners', '0003_auto_20160604_1543'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eventdetail',
            name='end_date',
        ),
        migrations.RemoveField(
            model_name='eventdetail',
            name='start_date',
        ),
    ]
