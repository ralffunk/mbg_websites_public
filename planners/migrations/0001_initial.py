# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('songs', '0004_singable'),
    ]

    operations = [
        migrations.CreateModel(
            name='Calendar',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('google_id', models.CharField(unique=True, default='', max_length=100)),
                ('google_last_sync', models.DateTimeField(default=django.utils.timezone.now)),
                ('google_sync_pending', models.BooleanField(default=True)),
                ('row_deleted', models.BooleanField(default=False)),
            ],
            options={
                'permissions': (('read_calendar', 'ver calendario'), ('manage_calendar', 'administrar calendario'), ('manage_events', 'administrar eventos'), ('change_schedule', 'modificar programa'), ('change_responsibilities_01', 'asignar orador'), ('change_responsibilities_02', 'asignar conductor'), ('change_responsibilities_03', 'asignar líder de alabanza'), ('change_responsibilities_04', 'asignar equipo de alabanza'), ('change_responsibilities_05', 'asignar encargado de consola'), ('change_responsibilities_06', 'asignar encargado de proyección'), ('change_responsibilities_07', 'asignar ujier'), ('change_responsibilities_08', 'asignar traductor'), ('change_responsibilities_99', 'asignar otras responsabilidades')),
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='CalendarDetail',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('summary', models.CharField(error_messages={'blank': 'El nombre del calendario no puede quedar vacío.'}, default='', max_length=100)),
                ('description', models.CharField(blank=True, default='', max_length=100)),
                ('is_public', models.BooleanField(default=False)),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('calendar', models.ForeignKey(related_name='details', to='planners.Calendar')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('type', models.CharField(choices=[('01', 'Programa normal'), ('02', 'Evento de varios días')], default='01', max_length=2)),
                ('google_last_sync', models.DateTimeField(default=django.utils.timezone.now)),
                ('google_sync_pending', models.BooleanField(default=True)),
                ('row_deleted', models.BooleanField(default=False)),
                ('calendar', models.ForeignKey(related_name='events', to='planners.Calendar')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='EventDetail',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('summary', models.CharField(error_messages={'blank': 'El nombre del evento no puede quedar vacío.'}, default='', max_length=100)),
                ('description', models.CharField(blank=True, default='', max_length=1000)),
                ('location', models.CharField(blank=True, default='', max_length=100)),
                ('is_confirmed', models.BooleanField(default=False)),
                ('start_date', models.DateField(blank=True, null=True)),
                ('start_datetime', models.DateTimeField(blank=True, null=True)),
                ('end_date', models.DateField(blank=True, null=True)),
                ('end_datetime', models.DateTimeField(blank=True, null=True)),
                ('subtype', models.CharField(choices=[('01', 'Sólo MBG'), ('02', 'MG y MBG - MBG organiza'), ('03', 'MG y MBG - MG organiza'), ('99', 'Otro')], default='99', max_length=2)),
                ('occasion', models.CharField(blank=True, default='', max_length=100)),
                ('topic', models.CharField(blank=True, default='', max_length=100)),
                ('bible_ref', models.CharField(blank=True, default='', max_length=100)),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('event', models.ForeignKey(related_name='details', to='planners.Event')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='EventResponsibility',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('task', models.CharField(choices=[('01', 'Orador'), ('02', 'Conductor'), ('03', 'Líder de alabanza'), ('04', 'Equipo de alabanza'), ('05', 'Encargado de consola'), ('06', 'Encargado de proyección'), ('07', 'Ujier'), ('08', 'Traductor'), ('99', 'Otro')], default='99', max_length=2)),
                ('task_open', models.CharField(blank=True, default='', max_length=20)),
                ('person_open', models.CharField(blank=True, default='', max_length=50)),
                ('obs', models.CharField(blank=True, default='', max_length=1000)),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('event', models.ForeignKey(related_name='responsibilities', to='planners.Event')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, null=True, to='people.Person', blank=True)),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='EventScheduleEntry',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('datetime', models.DateField(default=django.utils.timezone.now)),
                ('label', models.CharField(blank=True, default='', max_length=20)),
                ('person', models.CharField(blank=True, default='', max_length=50)),
                ('obs', models.CharField(blank=True, default='', max_length=1000)),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('event', models.ForeignKey(related_name='schedule', to='planners.Event')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='SongListEntry',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('sequence', models.IntegerField(default=0)),
                ('obs', models.CharField(blank=True, default='', max_length=1000)),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('schedule_entry', models.ForeignKey(related_name='song_list_entries', to='planners.EventScheduleEntry')),
                ('song', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='songs.Song')),
            ],
            options={
                'default_permissions': (),
            },
        ),
    ]
