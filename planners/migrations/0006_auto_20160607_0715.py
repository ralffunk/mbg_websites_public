# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('planners', '0005_auto_20160606_1312'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eventresponsibility',
            name='row_action',
        ),
        migrations.RemoveField(
            model_name='eventresponsibility',
            name='row_user_insert',
        ),
        migrations.RemoveField(
            model_name='eventscheduleentry',
            name='row_action',
        ),
        migrations.RemoveField(
            model_name='eventscheduleentry',
            name='row_user_insert',
        ),
        migrations.RemoveField(
            model_name='songlistentry',
            name='row_action',
        ),
        migrations.RemoveField(
            model_name='songlistentry',
            name='row_user_insert',
        ),
        migrations.AddField(
            model_name='eventresponsibility',
            name='row_user_end',
            field=models.ForeignKey(related_name='eventresponsibility_end_set', null=True, blank=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='eventresponsibility',
            name='row_user_start',
            field=models.ForeignKey(related_name='eventresponsibility_start_set', null=True, blank=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='eventscheduleentry',
            name='row_user_end',
            field=models.ForeignKey(related_name='eventscheduleentry_end_set', null=True, blank=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='eventscheduleentry',
            name='row_user_start',
            field=models.ForeignKey(related_name='eventscheduleentry_start_set', null=True, blank=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='songlistentry',
            name='row_user_end',
            field=models.ForeignKey(related_name='songlistentry_end_set', null=True, blank=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='songlistentry',
            name='row_user_start',
            field=models.ForeignKey(related_name='songlistentry_start_set', null=True, blank=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='songlistentry',
            name='song',
            field=models.ForeignKey(null=True, blank=True, on_delete=django.db.models.deletion.PROTECT, to='songs.Song'),
        ),
    ]
