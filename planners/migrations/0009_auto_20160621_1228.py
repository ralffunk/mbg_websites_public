# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planners', '0008_usercalendarcolor'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendar',
            name='google_id',
            field=models.CharField(max_length=200, default='', blank=True),
        ),
    ]
