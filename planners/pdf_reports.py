# -*- coding: utf-8 -*-
"""
PDF reports.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.utils import timezone as tz
from reportlab.lib import colors
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import mm
from reportlab.platypus import Paragraph, Spacer
from reportlab.platypus import TableStyle, Table
from mbg_websites.utils import make_pdf         # make this function accessible through this module


def get_schedule_story(event):
    """
    Get list of flowables.
    :param event:
    :return:
    """
    p_style_n = ParagraphStyle('Normal')
    story = list()                              # list of flowables with the content
    story.append(Spacer(1, 10*mm))              # spacer to not override the title

    pdf_title = 'Programa del evento'

    det = event.current_details
    story.append(
        Table(
            [
                ['Calendario:', Paragraph(str(event.calendar), p_style_n)],
                ['Evento:', Paragraph(str(event), p_style_n)],
                ['Fecha:', Paragraph(_event_date(det), p_style_n)],
            ],
            colWidths=[20*mm, 160*mm], repeatRows=1,
            style=TableStyle([
                ('LEFTPADDING', (0, 0), (-1, -1), 0),
                ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                ('TOPPADDING', (0, 0), (-1, -1), 0),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 2*mm),
                ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ]))
    )
    story.append(Spacer(1, 5*mm))

    col_widths = [20*mm, 40*mm, 40*mm, 80*mm]
    table1_data = list()

    # add the table header (a table with one row)
    table1_data.append([
        Table([['Hora', 'Punto del programa', 'Persona', 'Observación'], ],
              colWidths=col_widths,
              style=TableStyle([
                  ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                  ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
              ]))
    ])

    # add the schedule entries
    qs = event.current_schedule

    if not qs.exists():
        table1_data.append([
            Table([['No hay puntos para mostrar', ], ],
                  colWidths=[sum(col_widths), ],
                  style=TableStyle([
                      ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                      ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                  ]))
        ])
    else:
        for s_entry in qs:
            aux = [
                tz.localtime(s_entry.datetime).strftime('%H:%M'),
                s_entry.label,
                s_entry.person,
                s_entry.obs,
            ]
            table1_data.append([
                Table([[Paragraph(s, p_style_n) for s in aux], ],
                      colWidths=col_widths,
                      style=TableStyle([
                          ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                          ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                      ]))
            ])

    # add table with schedule entries to the report
    story.append(
        Table(table1_data, colWidths=sum(col_widths), repeatRows=1,
              style=TableStyle([
                  ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                  ('LEFTPADDING', (0, 0), (-1, -1), 0),
                  ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                  ('TOPPADDING', (0, 0), (-1, -1), 0),
                  ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
                  ('BACKGROUND', (0, 0), (-1, 0), colors.gainsboro),
              ]))
    )

    return pdf_title, story


def get_events_story(sf_date_start, sf_date_end, selected_calendar_list, selected_data_dict,
                     event_list):
    """
    Get list of flowables.
    :param sf_date_start:
    :param sf_date_end:
    :param selected_calendar_list:
    :param selected_data_dict:
    :param event_list:
    :return:
    """
    p_style_n = ParagraphStyle('Normal')
    story = list()                              # list of flowables with the content
    story.append(Spacer(1, 10*mm))              # spacer to not override the title

    pdf_title = 'Planificación de eventos'

    story.append(
        Table(
            [
                ['Fechas:', Paragraph(
                    '{} al {}'.format(
                        tz.localtime(sf_date_start).strftime('%d-%m-%Y'),
                        tz.localtime(sf_date_end).strftime('%d-%m-%Y')),
                    p_style_n)],
                ['Calendarios:', [Paragraph('{}. {}'.format(i+1, c), p_style_n)
                                  for i, c in enumerate(selected_calendar_list)]],
            ],
            colWidths=[30*mm, 237*mm], repeatRows=1,
            style=TableStyle([
                ('LEFTPADDING', (0, 0), (-1, -1), 0),
                ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                ('TOPPADDING', (0, 0), (-1, -1), 0),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 2*mm),
                ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ]))
    )
    story.append(Spacer(1, 5*mm))

    columns = [
        {
            'width': 35*mm,
            'title': 'Fecha',
            'data_f': lambda x: _event_date(x),
        },
        {
            'width': 35*mm,
            'title': 'Calendario',
            'data_f': lambda x: str(x.event.calendar)[:15],
        },
        {
            'width': 35*mm,
            'title': 'Evento',
            'data_f': lambda x: str(x.event)[:15],
        },
    ]
    if 'is_confirmed' in selected_data_dict:
        columns.append({
            'width': 15*mm,
            'title': selected_data_dict['is_confirmed'],
            'data_f': lambda x: 'C' if x.is_confirmed else 'AC',
        })
    if 'subtype' in selected_data_dict:
        columns.append({
            'width': 25*mm,
            'title': selected_data_dict['subtype'],
            'data_f': lambda x: x.get_subtype_display()[:12],
        })
    if 'occasion' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['occasion'],
            'data_f': lambda x: x.occasion[:20],
        })
    if 'topic' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['topic'],
            'data_f': lambda x: x.topic[:20],
        })
    if 'bible_ref' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['bible_ref'],
            'data_f': lambda x: x.bible_ref[:20],
        })
    if '01' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['01'],
            'data_f': lambda x: _person_list(x.event.r01()),
        })
    if '02' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['02'],
            'data_f': lambda x: _person_list(x.event.r02()),
        })
    if '03' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['03'],
            'data_f': lambda x: _person_list(x.event.r03()),
        })
    if '04' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['04'],
            'data_f': lambda x: _person_list(x.event.r04()),
        })
    if '05' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['05'],
            'data_f': lambda x: _person_list(x.event.r05()),
        })
    if '06' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['06'],
            'data_f': lambda x: _person_list(x.event.r06()),
        })
    if '07' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['07'],
            'data_f': lambda x: _person_list(x.event.r07()),
        })
    if '08' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['08'],
            'data_f': lambda x: _person_list(x.event.r08()),
        })
    if '99' in selected_data_dict:
        columns.append({
            'width': 30*mm,
            'title': selected_data_dict['99'],
            'data_f': lambda x: _person_list(x.event.r99()),
        })

    col_widths = [e['width'] for e in columns]
    table1_data = list()

    # add the table header (a table with one row)
    table1_data.append([
        Table([[Paragraph(e['title'], p_style_n) for e in columns], ],
              colWidths=col_widths,
              style=TableStyle([
                  ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                  ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
              ]))
    ])

    # add the events
    if not event_list:
        table1_data.append([
            Table([['No hay eventos para mostrar', ], ],
                  colWidths=[sum(col_widths), ],
                  style=TableStyle([
                      ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                      ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                  ]))
        ])
    else:
        for det in event_list:
            table1_data.append([
                Table([[Paragraph(e['data_f'](det), p_style_n) for e in columns], ],
                      colWidths=col_widths,
                      style=TableStyle([
                          ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                          ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                      ]))
            ])

    # add table with events to the report
    story.append(
        Table(table1_data, colWidths=sum(col_widths), repeatRows=1, hAlign='LEFT',
              style=TableStyle([
                  ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                  ('LEFTPADDING', (0, 0), (-1, -1), 0),
                  ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                  ('TOPPADDING', (0, 0), (-1, -1), 0),
                  ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
                  ('BACKGROUND', (0, 0), (-1, 0), colors.gainsboro),
              ]))
    )

    return pdf_title, story


def _event_date(det):
    """
    Return event date as string according to event type.
    :param det:
    :return:
    """
    s = '???'

    if det.event.type == '01':
        s = '{} - {}'.format(
            tz.localtime(det.start_datetime).strftime('%d-%m-%Y'),
            tz.localtime(det.start_datetime).strftime('%H:%M'))
    elif det.event.type == '02':
        s = '{} al {}'.format(
            tz.localtime(det.start_datetime).strftime('%d-%m-%Y'),
            tz.localtime(det.end_datetime).strftime('%d-%m-%Y'))

    return s


def _person_list(the_list):
    """
    Return person list as string.
    :param the_list:
    :return:
    """
    return ', '.join(e.dsc for e in the_list)
