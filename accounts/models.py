# -*- coding: utf-8 -*-
"""
Models.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.contrib.auth.models import User
from django.db import models
from people.models import Person


class UserProfile(models.Model):
    """
    This model represents the profile of a user, extending the default User model.
    """
    user = models.OneToOneField(User, related_name='profile', primary_key=True)
    person = models.ForeignKey(Person, null=True, blank=True, on_delete=models.PROTECT)
    row_time_insert = models.DateTimeField(auto_now_add=True)
    row_time_update = models.DateTimeField(auto_now=True)
    row_user_insert = models.ForeignKey(User, related_name='created_user_profiles',
                                        on_delete=models.PROTECT)
    row_user_update = models.ForeignKey(User, related_name='modified_user_profiles',
                                        on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (
            ('list_users', 'ver lista de usuarios'),
            ('update_user', 'modificar usuario'),
            ('list_calendars', 'ver lista de calendarios'),
            ('create_calendar', 'crear calendario'),
            ('delete_calendar', 'eliminar calendario'),
        )

    def __str__(self):
        if self.person:
            return str(self.person)
        else:
            return str(self.user)


class Log(models.Model):
    """
    Log of events.
    """
    EVENT_TYPES = (
        ('LOGIN', 'Inicio de sesión'),
        ('PW_CH', 'Cambio de contraseña'),
    )
    user = models.ForeignKey(User, related_name='log_entries', on_delete=models.PROTECT)
    type = models.CharField(max_length=5, choices=EVENT_TYPES, default='LOGIN',
                            error_messages={'blank': 'El tipo no puede quedar vacío.'})
    ip = models.CharField(max_length=20, default='',
                          error_messages={'blank': 'La IP no puede quedar vacía.'})
    row_time_insert = models.DateTimeField(auto_now_add=True)

    class Meta:
        default_permissions = ()
        ordering = ['-row_time_insert']

    def __str__(self):
        return 'Log(username: {}, type: {})'.format(self.user.username, self.type)
