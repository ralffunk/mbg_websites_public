# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from collections import OrderedDict
from django.conf import settings
from django.contrib import auth, messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User, Group
from django.contrib.sessions.models import Session
from django.core.exceptions import ValidationError, PermissionDenied
from django.core import mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db import connection
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone as tz
from mailer.models import Message, MessageLog
from ratelimit.decorators import ratelimit
from social.apps.django_app.default.models import UserSocialAuth
from accounts.models import UserProfile
from mbg_websites import utils
from people.models import Person, PersonDetail, MarriageDetail, GroupDetail, GroupMember
from planners.models import CalendarDetail, EventDetail, EventResponsibility, EventScheduleEntry
from planners.models import SongListEntry, UserCalendarColor
from songs.models import AuthorDetail, SongbookDetail, SongLyrics, SongProperties, File
from songs.models import PresentationStyle


MODEL_LIST = OrderedDict((
    (UserProfile.__name__, 'Usuario'),
    (PersonDetail.__name__, 'Persona'),
    (MarriageDetail.__name__, 'Matrimonio'),
    (GroupDetail.__name__, 'Grupo'),
    (GroupMember.__name__, 'Membresía de grupo'),
    (CalendarDetail.__name__, 'Calendario'),
    (EventDetail.__name__, 'Evento'),
    (EventResponsibility.__name__, 'Responsabilidad en evento'),
    (EventScheduleEntry.__name__, 'Programa de evento'),
    (SongListEntry.__name__, 'Canción en evento'),
    (UserCalendarColor.__name__, 'Color de calendario'),
    (AuthorDetail.__name__, 'Autor'),
    (SongbookDetail.__name__, 'Himnario'),
    (SongLyrics.__name__, 'Letra de canción'),
    (SongProperties.__name__, 'Propiedades de canción'),
    (File.__name__, 'Archivo de canción'),
    (PresentationStyle.__name__, 'Estilo de presentación'),
))


@ratelimit(key='post:username', rate='3/m', method='POST')
def login(request):
    """
    Log the user in and redirect to the page in the 'next' parameter.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Iniciar sesión', ),
    ]

    if request.user.is_authenticated():
        return redirect(request.GET.get('next', 'accounts:profile'))

    ctx['login_next'] = request.GET.get('next')
    if request.GET.get('alternative', '') == 'true':
        ctx['alternative_login'] = True

    if request.method == 'POST' and 'alternative_login' in ctx:
        if request.limited:
            messages.error(request, 'Ha hecho demasiados intentos erróneos. Espere un minuto para '
                                    'volver a intentarlo.')
            return render(request, 'accounts/login.html', ctx)

        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)

        if user is None:
            messages.error(request, 'El nombre de usuario o la contraseña no son correctos.')
            return render(request, 'accounts/login.html', ctx)

        if not user.is_active:
            messages.error(request, 'La cuenta del usuario "{}" tiene estado inactivo.'.format(
                username))
            return render(request, 'accounts/login.html', ctx)

        auth.login(request, user)
        user.log_entries.create(ip=request.session.get('_auth_user_backend', '').split('.')[-1])

        return redirect(request.GET.get('next', 'accounts:profile'))
    else:
        return render(request, 'accounts/login.html', ctx)


def login_error(request, inactive_user=False):
    """
    Replace Facebook message with custom message on login failure.
    :param request: HttpRequest
    :param inactive_user:
    :return: HttpResponse
    """
    # iterate over the messages marks them as read, so they won't get displayed
    for msg in messages.get_messages(request):
        pass

    if inactive_user:
        new_msg = '{}'.format('La cuenta del usuario tiene estado inactivo y no puede ser '
                              'utilizada para iniciar sesión.')
    else:
        new_msg = '{}'.format('Ocurrió un error durante el proceso de autenticación y no se pudo '
                              'iniciar la sesión.')

    messages.error(request, new_msg)
    return redirect('start-page')


@login_required()
def profile(request, new_user=False):
    """
    User profile.
    :param request: HttpRequest
    :param new_user:
    :return: HttpResponse
    """
    ctx = dict(page_title='Mi perfil')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Mi perfil', ),
    ]
    ctx['perm_groups'] = Group.objects.order_by('name')

    ctx['auth_list'] = [sa.provider for sa in request.user.social_auth.order_by('provider')]
    if request.user.has_usable_password():
        ctx['auth_list'].append('contraseña')

    if new_user:
        messages.success(request, 'Bienvenido, nuevo usuario "{}".'.format(request.user.username))

    if request.method == 'POST':
        sub = 'Solicitud de autorización para "{}"'.format(request.user.username)
        msg = '{}\n{}'.format(
            'El usuario "{}" ha hecho una solicitud de autorización.'.format(request.user.username),
            'Observación: {}'.format(request.POST.get('obs', '(ninguna)')))
        mail.mail_admins(subject=sub, message=msg)
        messages.success(request, 'Se envió tu solicitud al administrador del sistema.')
        return redirect('accounts:profile')

    return render(request, 'accounts/profile.html', ctx)


@login_required()
def profile_activity_list(request):
    """
    View activity list of current user.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Mi perfil', reverse('accounts:profile')),
        ('Mi historial de acciones', ),
    ]
    ctx['sf_text'] = request.GET.get('sf_text', '')
    ctx.update(utils.get_sf_dates(request.GET))
    ctx['sf_order'] = '0'

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    action_list = _get_user_actions(request.user, 'A', ctx['sf_date_start'], ctx['sf_date_end'])
    if ctx['sf_text']:
        s = ctx['sf_text'].upper()
        action_list = filter(
            lambda x: s in x['model'].upper() or s in x['action'].upper() or s in x['dsc'].upper(),
            action_list)

    action_list = sorted(
        action_list,
        key=lambda x: x['date'],
        reverse=True)

    paginator = Paginator(action_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'accounts/profile_activity_list.html', ctx)


@login_required()
def profile_password(request):
    """
    Change the currently logged-in users password.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Cambiar contraseña')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Mi perfil', reverse('accounts:profile')),
        ('Cambiar contraseña', ),
    ]

    if not request.user.has_usable_password():
        messages.error(request, 'Este usuario no puede cambiar su contraseña en este sistema '
                                'porque utiliza autenticación a través de una red social.')
        return redirect('accounts:profile')

    if request.method == 'POST':
        u = request.user
        old_pw = request.POST.get('oldPw')
        new_pw1 = request.POST.get('newPw1', '')
        new_pw2 = request.POST.get('newPw2', '')
        if not u.check_password(old_pw):
            messages.error(request, 'La contraseña actual ingresada no es correcta.')
            return render(request, 'accounts/profile_password.html', ctx)

        if new_pw1 == '':
            messages.error(request, 'La contraseña nueva no puede ser vacía.')
            return render(request, 'accounts/profile_password.html', ctx)

        if new_pw1 != new_pw2:
            messages.error(request, 'Las contraseñas nuevas no son iguales.')
            return render(request, 'accounts/profile_password.html', ctx)

        u.set_password(new_pw1)
        u.save()
        u.profile.row_user_update = u
        u.profile.save()
        u.log_entries.create(type='PW_CH', ip=request.session['ip'])

        return redirect('accounts:profile')
    else:
        return render(request, 'accounts/profile_password.html', ctx)


@login_required()
@permission_required('accounts.list_users', raise_exception=True)
def email_list(request):
    """
    List emails.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de emails', ),
    ]

    ctx['sf_text'] = request.GET.get('sf_text', '')
    ctx.update(utils.get_sf_dates(request.GET))
    ctx['sf_order'] = '0'

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    email_list_1 = list(Message.objects.all())
    email_list_1.extend(MessageLog.objects.all())

    email_list_2 = []
    for m in email_list_1:
        d = {
            'status': 'QUEUED',
            'to': ', '.join(m.email.to),
            'subject': m.email.subject,
            'body': m.email.body[:1000],
            'when_added': m.when_added,
            'priority': m.get_priority_display(),
            'when_attempted': None,
            'result_code': '',
            'result_str': '',
            'log_message': '',
        }

        if isinstance(m, MessageLog):
            d['status'] = 'ATTEMPTED'
            d['when_attempted'] = m.when_attempted
            d['result_code'] = m.result
            d['result_str'] = m.get_result_display()
            d['log_message'] = m.log_message

        if (ctx['sf_date_start'] <= d['when_added'] <= ctx['sf_date_end']
                or (d['when_attempted']
                    and ctx['sf_date_start'] <= d['when_attempted'] <= ctx['sf_date_end'])):
            if (not ctx['sf_text']
                    or ctx['sf_text'].upper() in d['to'].upper()
                    or ctx['sf_text'].upper() in d['subject'].upper()
                    or ctx['sf_text'].upper() in d['priority'].upper()
                    or ctx['sf_text'].upper() in d['result_str'].upper()
                    or ctx['sf_text'].upper() in d['log_message'].upper()):
                email_list_2.append(d)

    email_list_2 = sorted(
        email_list_2,
        key=lambda x: (x['status'], x['when_added'], x['when_attempted']),
        reverse=True)

    paginator = Paginator(email_list_2, 20)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'accounts/email_list.html', ctx)


@login_required()
@permission_required('accounts.list_users', raise_exception=True)
def session_list(request):
    """
    List sessions.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de sesiones', ),
    ]

    ctx['sf_text'] = request.GET.get('sf_text', '')
    ctx.update(utils.get_sf_dates(request.GET))

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
    else:
        ctx['sf_order'] = '0'

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    now_ = tz.now()
    session_list_1 = []
    for s in Session.objects.filter(expire_date__range=(ctx['sf_date_start'], ctx['sf_date_end'])):
        data = s.get_decoded()
        d = {
            'expire_date': s.expire_date,
            'is_expired': s.expire_date < now_,
            'session_key': '{}...{}'.format(s.session_key[:5], s.session_key[-5:]),
            'ip': data.get('ip', ''),
            'user_str': '',
            'auth_backend': '',
        }
        if data.get('_auth_user_id'):
            try:
                d['user_str'] = str(User.objects.get(pk=data['_auth_user_id']).profile)
            except User.DoesNotExist:
                d['user_str'] = 'ERROR_{}'.format(data['_auth_user_id'])
            d['auth_backend'] = data.get('_auth_user_backend').split('.')[-1]

        if (not ctx['sf_text']
                or ctx['sf_text'].upper() in d['ip'].upper()
                or ctx['sf_text'].upper() in d['user_str'].upper()
                or ctx['sf_text'].upper() in d['auth_backend'].upper()):
            session_list_1.append(d)

    if ctx['sf_order'] == '1':
        session_list_1 = sorted(
            session_list_1,
            key=lambda x: (x['user_str'], x['expire_date']))
    elif ctx['sf_order'] == '2':
        session_list_1 = sorted(
            session_list_1,
            key=lambda x: (x['auth_backend'], x['user_str'], x['expire_date']))
    else:
        session_list_1 = sorted(
            session_list_1,
            key=lambda x: x['expire_date'],
            reverse=True)

    paginator = Paginator(session_list_1, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'accounts/session_list.html', ctx)


@login_required()
def system_status(request):
    """
    System status information.
    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.user.username != settings.ADMIN_USERNAME:
        raise PermissionDenied

    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Estado del sistema', ),
    ]

    table_list = connection.introspection.table_names()
    app_dict = {}
    ctx['total_row_count'] = 0
    ctx['table_count'] = len(table_list)
    for i, t in enumerate(table_list):
        d = {
            'table_name': t,
            'model_name': '',
            'row_count': 0,
        }

        m_list = list(connection.introspection.installed_models([t, ]))
        if not m_list:
            cursor = connection.cursor()
            cursor.execute('SELECT count(*) FROM {}'.format(t))
            row = cursor.fetchone()
            try:
                d['row_count'] = int(row[0])
            except (IndexError, ValueError):
                pass
        elif len(m_list) == 1:
            m = m_list[0]
            d['model_name'] = str(m)[8:-2]
            d['row_count'] = m.objects.count()
        else:
            messages.error(request, 'La tabla "{}" tiene {} modelos.'.format(t, len(m_list)))

        app = t.split('_')[0]
        if app in app_dict:
            app_dict[app]['table_list'].append(d)
        else:
            app_dict[app] = {
                'app_name': app,
                'table_list': [d, ],
            }
        ctx['total_row_count'] += d['row_count']

    ctx['app_list'] = []
    for app in sorted(app_dict.values(), key=lambda x: x['app_name']):
        app['row_count'] = sum(e['row_count'] for e in app['table_list'])
        app['row_percent'] = int(app['row_count'] * 100 / ctx['total_row_count'])
        ctx['app_list'].append(app)

    return render(request, 'accounts/system_status.html', ctx)


@login_required()
@permission_required('accounts.list_users', raise_exception=True)
def users_list(request):
    """
    List users.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de usuarios', ),
    ]
    ctx['perm_groups'] = Group.objects.order_by('name')

    qs = User.objects.all()

    ctx['sf_text'] = request.GET.get('sf_text', '')
    qs = qs.filter(username__icontains=ctx['sf_text'])

    if request.GET.get('sf_status', '') == 'AC':
        ctx['sf_status'] = 'AC'
        qs = qs.filter(is_active=True)
    elif request.GET.get('sf_status', '') == 'IN':
        ctx['sf_status'] = 'IN'
        qs = qs.filter(is_active=False)
    else:
        ctx['sf_status'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('username')
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
        qs = qs.order_by('-last_login')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('is_active', 'username')

    ctx['sf'] = 'sf_text={}&sf_status={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_status'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('accounts:users-read', args=[e.pk])
        e.auth_list = [sa.provider for sa in e.social_auth.order_by('provider')]
        if e.has_usable_password():
            e.auth_list.append('contraseña')

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'accounts/user_list.html', ctx)


@login_required()
@permission_required('accounts.list_users', raise_exception=True)
def users_read(request, user_id):
    """
    Read an user.
    :param request: HttpRequest
    :param user_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles del usuario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Usuarios', reverse('accounts:users-list')),
        ('Detalles del usuario', ),
    ]
    ctx['CRUD'] = 'R'
    ctx['usr'] = get_object_or_404(User, pk=user_id)
    ctx['perm_groups'] = Group.objects.order_by('name')

    ctx['auth_list'] = [sa.provider for sa in ctx['usr'].social_auth.order_by('provider')]
    if ctx['usr'].has_usable_password():
        ctx['auth_list'].append('contraseña')

    return render(request, 'accounts/user_form.html', ctx)


@login_required()
@permission_required('accounts.update_user', raise_exception=True)
def users_update(request, user_id):
    """
    Update an user.
    :param request: HttpRequest
    :param user_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar usuario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Usuarios', reverse('accounts:users-list')),
        ('Usuario {}'.format(user_id), reverse('accounts:users-read', args=[user_id])),
        ('Modificar usuario', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['usr'] = get_object_or_404(User, pk=user_id)

    if ctx['usr'].username == settings.ADMIN_USERNAME:
        return redirect('accounts:users-read', ctx['usr'].pk)

    return _users_create_or_update(request, ctx)


@login_required()
@permission_required('accounts.list_users', raise_exception=True)
def users_activity_list(request, user_id):
    """
    View activity list of an user.
    :param request: HttpRequest
    :param user_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Usuarios', reverse('accounts:users-list')),
        ('Usuario {}'.format(user_id), reverse('accounts:users-read', args=[user_id])),
        ('Historial de acciones', ),
    ]
    ctx['usr'] = get_object_or_404(User, pk=user_id)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    ctx.update(utils.get_sf_dates(request.GET))

    ctx['model_list'] = MODEL_LIST
    if request.GET.get('sf_model', '') in MODEL_LIST:
        ctx['sf_model'] = request.GET.get('sf_model')
    else:
        ctx['sf_model'] = 'A'

    ctx['sf_order'] = '0'

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_model={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_model'],
        ctx['sf_order'])

    action_list = _get_user_actions(ctx['usr'], ctx['sf_model'], ctx['sf_date_start'],
                                    ctx['sf_date_end'])
    if ctx['sf_text']:
        s = ctx['sf_text'].upper()
        action_list = filter(
            lambda x: s in x['model'].upper() or s in x['action'].upper() or s in x['dsc'].upper(),
            action_list)

    action_list = sorted(
        action_list,
        key=lambda x: x['date'],
        reverse=True)

    paginator = Paginator(action_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'accounts/user_activity_list.html', ctx)


@login_required()
def social_auth_list(request):
    """
    List social auth info of users.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Usuarios', reverse('accounts:users-list')),
        ('Lista de accesos', ),
    ]
    if request.user.username != settings.ADMIN_USERNAME:
        raise PermissionDenied

    qs = UserSocialAuth.objects.all()

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(user__username__icontains=ctx['sf_text'])
    q_t2 = Q(provider__icontains=ctx['sf_text'])
    q_t3 = Q(uid__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3)

    ctx['sf_order'] = '0'
    qs = qs.order_by('user')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'accounts/social_auth_list.html', ctx)


def _users_create_or_update(request, ctx):
    """
    Create or update an user.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    ctx['person_list'] = sorted(
        [{'pk': e.pk, 'name': str(e)} for e in Person.objects.filter(row_deleted=False)],
        key=lambda x: x['name'])
    ctx['perm_groups'] = Group.objects.order_by('name')

    if request.method == 'POST':
        if request.user.username == settings.ADMIN_USERNAME:
            ctx['usr'].username = request.POST.get('username')

        try:
            p = Person.objects.get(pk=request.POST.get('person'), row_deleted=False)

            if p.userprofile_set.exclude(user=ctx['usr']).exists():
                messages.error(request, 'La persona "{}" está asignada a otro usuario.'.format(p))
                p = None
        except Person.DoesNotExist:
            p = None
        ctx['usr'].profile.person = p

        if request.POST.get('status', '') == 'A':
            ctx['usr'].is_active = True
        else:
            ctx['usr'].is_active = False
        ctx['usr'].profile.row_user_update = request.user

        try:
            ctx['usr'].profile.full_clean()
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'accounts/user_form.html', ctx)

        ctx['usr'].save()
        ctx['usr'].profile.save()

        ctx['usr'].groups.clear()
        for pk in request.POST.getlist('perm_groups'):
            try:
                ctx['usr'].groups.add(Group.objects.get(pk=pk))
            except Group.DoesNotExist:
                pass

        return redirect('accounts:users-read', ctx['usr'].pk)
    else:
        return render(request, 'accounts/user_form.html', ctx)


def _get_user_actions(user, selected_model, start_date, end_date):
    q_1 = Q(row_time_start__range=(start_date, end_date))
    action_list = []

    if selected_model in ('A', UserProfile.__name__):
        for e in user.created_user_profiles.filter(row_time_insert__range=(start_date, end_date)):
            d = {
                'date': e.row_time_insert,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'CREATE',
                'pk_str': '(PK {})'.format(e.user.pk),
                'dsc': str(e.user),
            }
            action_list.append(d)
        for e in user.modified_user_profiles.filter(row_time_update__range=(start_date, end_date)):
            d = {
                'date': e.row_time_update,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'UPDATE',
                'pk_str': '(PK {})'.format(e.user.pk),
                'dsc': str(e.user),
            }
            action_list.append(d)
        for e in user.log_entries.filter(row_time_insert__range=(start_date, end_date)):
            d = {
                'date': e.row_time_insert,
                'model': MODEL_LIST[UserProfile.__name__],
                'action': e.type,
                'pk_str': '(PK {})'.format(e.user.pk),
                'dsc': str(e.user),
            }
            action_list.append(d)

    if selected_model in ('A', PersonDetail.__name__):
        for e in user.persondetail_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': e.row_action,
                'pk_str': '(PK {})'.format(e.person.pk),
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', MarriageDetail.__name__):
        for e in user.marriagedetail_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': e.row_action,
                'pk_str': '(PK {})'.format(e.marriage.pk),
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', GroupDetail.__name__):
        for e in user.groupdetail_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': e.row_action,
                'pk_str': '(PK {})'.format(e.group.pk),
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', GroupMember.__name__):
        for e in user.groupmember_start_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'CREATE',
                'pk_str': '',
                'dsc': str(e),
            }
            action_list.append(d)
        for e in user.groupmember_end_set.filter(q_1):
            d = {
                'date': e.row_time_end,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'DELETE',
                'pk_str': '',
                'dsc': str(e),
            }
            action_list.append(d)

    if selected_model in ('A', CalendarDetail.__name__):
        for e in user.calendardetail_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': e.row_action,
                'pk_str': '(PK {})'.format(e.calendar.pk),
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', EventDetail.__name__):
        for e in user.eventdetail_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': e.row_action,
                'pk_str': '(PK {})'.format(e.event.pk),
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', EventResponsibility.__name__):
        for e in user.eventresponsibility_start_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'CREATE',
                'pk_str': '',
                'dsc': str(e),
            }
            action_list.append(d)
        for e in user.eventresponsibility_end_set.filter(q_1):
            d = {
                'date': e.row_time_end,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'DELETE',
                'pk_str': '',
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', EventScheduleEntry.__name__):
        for e in user.eventscheduleentry_start_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'CREATE',
                'pk_str': '',
                'dsc': str(e),
            }
            action_list.append(d)
        for e in user.eventscheduleentry_end_set.filter(q_1):
            d = {
                'date': e.row_time_end,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'DELETE',
                'pk_str': '',
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', SongListEntry.__name__):
        for e in user.songlistentry_start_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'CREATE',
                'pk_str': '',
                'dsc': str(e),
            }
            action_list.append(d)
        for e in user.songlistentry_end_set.filter(q_1):
            d = {
                'date': e.row_time_end,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'DELETE',
                'pk_str': '',
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', UserCalendarColor.__name__):
        for e in user.usercalendarcolor_set.filter(row_time_update__range=(start_date, end_date)):
            d = {
                'date': e.row_time_update,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'UPDATE',
                'pk_str': '',
                'dsc': '{} | {}'.format(e.calendar, e),
            }
            action_list.append(d)

    if selected_model in ('A', AuthorDetail.__name__):
        for e in user.authordetail_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': e.row_action,
                'pk_str': '(PK {})'.format(e.author.pk),
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', SongbookDetail.__name__):
        for e in user.songbookdetail_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': e.row_action,
                'pk_str': '(PK {})'.format(e.songbook.pk),
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', SongLyrics.__name__):
        for e in user.songlyrics_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': e.row_action,
                'pk_str': '(PK {})'.format(e.song.pk),
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', SongProperties.__name__):
        for e in user.songproperties_set.filter(q_1):
            d = {
                'date': e.row_time_start,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': e.row_action,
                'pk_str': '(PK {})'.format(e.song.pk),
                'dsc': str(e),
            }
            action_list.append(d)
    if selected_model in ('A', File.__name__):
        for e in user.file_set.filter(row_time__range=(start_date, end_date)):
            d = {
                'date': e.row_time,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'CREATE',
                'pk_str': '(PK {})'.format(e.pk),
                'dsc': 'Canción {} | {} | {}'.format(e.song.pk, e.content_type, e.codename),
            }
            action_list.append(d)
    if selected_model in ('A', PresentationStyle.__name__):
        for e in user.created_styles.filter(row_time_insert__range=(start_date, end_date)):
            d = {
                'date': e.row_time_insert,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'CREATE',
                'pk_str': '(PK {})'.format(e.pk),
                'dsc': str(e),
            }
            action_list.append(d)
        for e in user.modified_styles.filter(row_time_update__range=(start_date, end_date)):
            d = {
                'date': e.row_time_update,
                'model': MODEL_LIST[e.__class__.__name__],
                'action': 'UPDATE',
                'pk_str': '(PK {})'.format(e.pk),
                'dsc': str(e),
            }
            action_list.append(d)

    return action_list
