# -*- coding: utf-8 -*-
"""
Pipelines for authentication.

See also: http://psa.matiasaguirre.net/docs/pipeline.html
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.core import mail
from django.utils import timezone
from accounts.models import UserProfile


def create_profile(user=None, is_new=False, *args, **kwargs):
    """
    Create an user profile after external authentication and new local user creation.
    :param user:
    :param is_new:
    :param args:
    :param kwargs:
    :return:
    """
    if user and is_new:
        up = UserProfile()
        up.user = user
        up.row_user_insert = user
        up.row_user_update = user
        up.save()

        sub = 'Nuevo usuario "{}"'.format(user.username)
        msg = 'El usuario "{}" fue creado el {}.'.format(
            user.username,
            timezone.localtime(up.row_time_insert).strftime('%d-%m-%Y a las %H:%M'))
        mail.mail_admins(subject=sub, message=msg)


def log_login(user=None, *args, **kwargs):
    """
    Log the login event.
    :param user:
    :param args:
    :param kwargs:
    :return:
    """
    if user:
        s = '---'
        if 'backend' in kwargs:
            b = kwargs.get('backend')
            if b:
                s = b.name[:20]

        # we save backend at least, because IP is not available here
        user.log_entries.create(ip=s)
