# -*- coding: utf-8 -*-
"""
URL configuration.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.conf.urls import url, include
from accounts import views, views_roles


users_urlpatterns = [
    url(r'^$',
        views.users_list,
        name='users-list'),
    url(r'^(?P<user_id>\d+)/$',
        views.users_read,
        name='users-read'),
    url(r'^(?P<user_id>\d+)/update/$',
        views.users_update,
        name='users-update'),
    url(r'^(?P<user_id>\d+)/activity/$',
        views.users_activity_list,
        name='users-activity-list'),
    url(r'^social-auth/$',
        views.social_auth_list,
        name='social-auth-list'),
]


roles_urlpatterns = [
    url(r'^$',
        views_roles.roles_list,
        name='roles-list'),
    url(r'^create/$',
        views_roles.roles_create,
        name='roles-create'),
    url(r'^(?P<role_id>\d+)/$',
        views_roles.roles_read,
        name='roles-read'),
    url(r'^(?P<role_id>\d+)/update/$',
        views_roles.roles_update,
        name='roles-update'),
    url(r'^(?P<role_id>\d+)/delete/$',
        views_roles.roles_delete,
        name='roles-delete'),
]


urlpatterns = [
    url(r'^login/$', views.login,
        name='login'),
    url(r'^login/error/$', views.login_error),
    url(r'^login/error/inactive/$', views.login_error, {'inactive_user': True}),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': 'start-page'},
        name='logout'),
    url(r'^profile/$', views.profile,
        name='profile'),
    url(r'^profile/activity-list/$', views.profile_activity_list,
        name='profile-activity-list'),
    url(r'^profile/new/$', views.profile, {'new_user': True}),
    url(r'^profile/password/$', views.profile_password,
        name='profile-password'),
    url(r'^emails/$',
        views.email_list,
        name='email-list'),
    url(r'^sessions/$',
        views.session_list,
        name='session-list'),
    url(r'^status/$',
        views.system_status,
        name='system-status'),
    url(r'^users/', include(users_urlpatterns)),
    url(r'^roles/', include(roles_urlpatterns)),
]
