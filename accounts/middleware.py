# -*- coding: utf-8 -*-
"""
Middleware.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


class PersistentAnonymousSessionsMiddleware:
    """
    Make anonymous sessions persistent.
    """

    def process_request(self, request):
        # WARNING: in deployments without proxies that replace the header HTTP_X_FORWARDED_FOR
        # with the real IP, an attacker can send this header with a fake IP and the system would not
        # detect it. But because we use the IP merely as an additional info, this is not a big issue
        if request.META.get('HTTP_X_FORWARDED_FOR'):
            ip = request.META.get('HTTP_X_FORWARDED_FOR')
        else:
            ip = request.META.get('REMOTE_ADDR')

        if request.session.get('ip') != ip:
            request.session['ip'] = ip
