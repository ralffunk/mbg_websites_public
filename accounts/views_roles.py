# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group, Permission
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404


@login_required()
@permission_required('auth.change_group', raise_exception=True)
def roles_list(request):
    """
    List roles.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de roles', ),
    ]
    ctx['perm_list'] = Permission.objects.filter(content_type__app_label__in=(
        'accounts', 'people', 'songs')).order_by('content_type', 'codename')

    qs = Group.objects.order_by('name')

    ctx['sf_text'] = request.GET.get('sf_text', '')
    qs = qs.filter(name__icontains=ctx['sf_text'])

    ctx['sf_order'] = '0'

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('accounts:roles-read', args=[e.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'accounts/role_list.html', ctx)


@login_required()
@permission_required('auth.add_group', raise_exception=True)
def roles_create(request):
    """
    Create a role.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear rol')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Roles', reverse('accounts:roles-list')),
        ('Crear rol', ),
    ]
    ctx['CRUD'] = 'C'
    ctx['role'] = Group()
    return _roles_create_or_update(request, ctx)


@login_required()
@permission_required('auth.change_group', raise_exception=True)
def roles_read(request, role_id):
    """
    Read a role.
    :param request: HttpRequest
    :param role_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles del rol')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Roles', reverse('accounts:roles-list')),
        ('Detalles del rol', ),
    ]
    ctx['CRUD'] = 'R'
    ctx['role'] = get_object_or_404(Group, pk=role_id)
    ctx['perm_list'] = Permission.objects.filter(content_type__app_label__in=(
        'accounts', 'people', 'songs')).order_by('content_type', 'codename')

    return render(request, 'accounts/role_form.html', ctx)


@login_required()
@permission_required('auth.change_group', raise_exception=True)
def roles_update(request, role_id):
    """
    Update a role.
    :param request: HttpRequest
    :param role_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar rol')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Roles', reverse('accounts:roles-list')),
        ('Rol {}'.format(role_id), reverse('accounts:roles-read', args=[role_id])),
        ('Modificar rol', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['role'] = get_object_or_404(Group, pk=role_id)
    return _roles_create_or_update(request, ctx)


@login_required()
@permission_required('auth.delete_group', raise_exception=True)
def roles_delete(request, role_id):
    """
    Delete a role.
    :param request: HttpRequest
    :param role_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar rol')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Roles', reverse('accounts:roles-list')),
        ('Rol {}'.format(role_id), reverse('accounts:roles-read', args=[role_id])),
        ('Eliminar rol', ),
    ]
    ctx['CRUD'] = 'D'
    ctx['role'] = get_object_or_404(Group, pk=role_id)
    ctx['perm_list'] = Permission.objects.filter(content_type__app_label__in=(
        'accounts', 'people', 'songs')).order_by('content_type', 'codename')

    if request.method == 'POST':
        ctx['role'].delete()
        return redirect('accounts:roles-list')
    else:
        return render(request, 'accounts/role_form.html', ctx)


def _roles_create_or_update(request, ctx):
    """
    Create or update a role.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    ctx['perm_list'] = Permission.objects.filter(content_type__app_label__in=(
        'accounts', 'people', 'songs')).order_by('content_type', 'codename')

    if request.method == 'POST':
        ctx['role'].name = request.POST.get('name', '')

        try:
            ctx['role'].full_clean()
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'accounts/role_form.html', ctx)

        ctx['role'].save()

        ctx['role'].permissions.clear()
        for pk in request.POST.getlist('perms'):
            try:
                ctx['role'].permissions.add(Permission.objects.get(pk=pk))
            except Permission.DoesNotExist:
                pass

        return redirect('accounts:roles-read', ctx['role'].pk)
    else:
        return render(request, 'accounts/role_form.html', ctx)
