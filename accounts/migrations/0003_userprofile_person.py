# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('accounts', '0002_first_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, null=True, to='people.Person', related_name='user_profiles', blank=True),
        ),
    ]
