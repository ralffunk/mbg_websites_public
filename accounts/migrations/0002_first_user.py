# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.db import migrations


def create_first_user(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    User = apps.get_model('auth', 'User')
    UserProfile = apps.get_model('accounts', 'UserProfile')

    usr = User.objects.create(username=settings.ADMIN_USERNAME, password=make_password('123'),
                              is_superuser=True)

    up = UserProfile()
    up.user = usr
    up.row_user_insert = usr
    up.row_user_update = usr
    up.save()


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
        ('auth', '__latest__'),         # dependency for User model
    ]

    operations = [
        migrations.RunPython(create_first_user),
    ]
