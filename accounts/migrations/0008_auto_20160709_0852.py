# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0007_googlecredentials'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='googlecredentials',
            name='user',
        ),
        migrations.DeleteModel(
            name='GoogleCredentials',
        ),
    ]
