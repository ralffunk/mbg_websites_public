# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('type', models.CharField(choices=[('LOGIN', 'Inicio de sesión'), ('PW_CH', 'Cambio de contraseña')], max_length=5, default='LOGIN', error_messages={'blank': 'El tipo no puede quedar vacío.'})),
                ('ip', models.CharField(max_length=20, default='', error_messages={'blank': 'La IP no puede quedar vacía.'})),
                ('row_time_insert', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'default_permissions': (),
                'ordering': ['-row_time_insert'],
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL, primary_key=True, serialize=False)),
                ('username', models.CharField(validators=[django.core.validators.RegexValidator('^[\\w]+$', 'El nombre del usuario no es válido.')], max_length=30, error_messages={'blank': 'El nombre del usuario no puede quedar vacío.', 'unique': 'El nombre del usuario no puede ser duplicado.'}, unique=True)),
                ('row_time_insert', models.DateTimeField(auto_now_add=True)),
                ('row_time_update', models.DateTimeField(auto_now=True)),
                ('row_user_insert', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='created_user_profiles', on_delete=django.db.models.deletion.PROTECT)),
                ('row_user_update', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='modified_user_profiles', on_delete=django.db.models.deletion.PROTECT)),
            ],
            options={
                'default_permissions': (),
                'permissions': (('list_users', 'ver lista de usuarios'), ('update_user', 'modificar usuario')),
            },
        ),
        migrations.AddField(
            model_name='log',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='log_entries', on_delete=django.db.models.deletion.PROTECT),
        ),
    ]
