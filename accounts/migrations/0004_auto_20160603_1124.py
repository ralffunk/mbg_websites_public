# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_userprofile_person'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userprofile',
            options={'default_permissions': (), 'permissions': (('list_users', 'ver lista de usuarios'), ('update_user', 'modificar usuario'), ('list_calendars', 'ver lista de calendarios'), ('create_calendar', 'crear calendario'))},
        ),
    ]
