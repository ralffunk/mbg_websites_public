# -*- coding: utf-8 -*-
"""
URL configuration.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.conf.urls import url, include
from people import views_people, views_marriages, views_groups


marriage_urlpatterns = [
    url(r'^$',
        views_marriages.marriages_list,
        name='marriages-list'),
    url(r'^excel/$',
        views_marriages.marriages_list_excel,
        name='marriages-list-excel'),
    url(r'^deleted/$',
        views_marriages.marriages_list_deleted,
        name='marriages-list-deleted'),
    url(r'^create/$',
        views_marriages.marriages_create,
        name='marriages-create'),
    url(r'^(?P<marriage_id>\d+)/', include([
        url(r'^$',
            views_marriages.marriages_read,
            name='marriages-read'),
        url(r'^update/$',
            views_marriages.marriages_update,
            name='marriages-update'),
        url(r'^delete/$',
            views_marriages.marriages_delete,
            name='marriages-delete'),
        url(r'^delete/undo/$',
            views_marriages.marriages_delete_undo,
            name='marriages-delete-undo'),
        url(r'^history/$',
            views_marriages.marriages_history,
            name='marriages-history'),
    ])),
]


group_urlpatterns = [
    url(r'^$',
        views_groups.groups_list,
        name='groups-list'),
    url(r'^deleted/$',
        views_groups.groups_list_deleted,
        name='groups-list-deleted'),
    url(r'^create/$',
        views_groups.groups_create,
        name='groups-create'),
    url(r'^(?P<group_id>\d+)/', include([
        url(r'^$',
            views_groups.groups_read,
            name='groups-read'),
        url(r'^update/$',
            views_groups.groups_update,
            name='groups-update'),
        url(r'^delete/$',
            views_groups.groups_delete,
            name='groups-delete'),
        url(r'^delete/undo/$',
            views_groups.groups_delete_undo,
            name='groups-delete-undo'),
        url(r'^history/$',
            views_groups.groups_history,
            name='groups-history'),
        url(r'^members/', include([
            url(r'^excel/$',
                views_groups.groups_members_excel,
                name='groups-members-excel'),
            url(r'^pdf/$',
                views_groups.groups_members_pdf,
                name='groups-members-pdf'),
            url(r'^history/$',
                views_groups.groups_members_history,
                name='groups-members-history'),
        ])),
    ])),
]


urlpatterns = [
    url(r'^$',
        views_people.people_list,
        name='people-list'),
    url(r'^excel/$',
        views_people.people_list_excel,
        name='people-list-excel'),
    url(r'^excel/members/$',
        views_people.people_list_members_excel,
        name='people-list-members-excel'),
    url(r'^pdf/$',
        views_people.people_list_pdf,
        name='people-list-pdf'),
    url(r'^deleted/$',
        views_people.people_list_deleted,
        name='people-list-deleted'),
    url(r'^create/$',
        views_people.people_create,
        name='people-create'),
    url(r'^(?P<person_id>\d+)/', include([
        url(r'^$',
            views_people.people_read,
            name='people-read'),
        url(r'^update/$',
            views_people.people_update,
            name='people-update'),
        url(r'^delete/$',
            views_people.people_delete,
            name='people-delete'),
        url(r'^delete/undo/$',
            views_people.people_delete_undo,
            name='people-delete-undo'),
        url(r'^history/$',
            views_people.people_history,
            name='people-history'),
        url(r'^groups/', include([
            url(r'^history/$',
                views_people.people_groups_history,
                name='people-groups-history'),
            url(r'^add/$',
                views_people.people_groups_add,
                name='people-groups-add'),
            url(r'^edit/$',
                views_people.people_groups_edit,
                name='people-groups-edit'),
            url(r'^remove/$',
                views_people.people_groups_remove,
                name='people-groups-remove'),
        ])),
    ])),
    url(r'^birthdays/$',
        views_people.people_birthday_list,
        name='birthday-list'),
    url(r'^marriages/', include(marriage_urlpatterns)),
    url(r'^groups/', include(group_urlpatterns)),
]
