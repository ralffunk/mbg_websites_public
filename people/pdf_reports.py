# -*- coding: utf-8 -*-
"""
PDF reports.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from reportlab.lib import colors
from reportlab.lib.enums import TA_RIGHT
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import mm
from reportlab.platypus import Paragraph, Spacer
from reportlab.platypus import TableStyle, Table
from mbg_websites.utils import month_dict
from mbg_websites.utils import make_pdf         # make this function accessible through this module
from people.models import PersonDetail, MarriageDetail


def get_people_list():
    """
    Get list of flowables.
    :return:
    """
    p_style_n = ParagraphStyle('Normal', leftIndent=3*mm, firstLineIndent=-3*mm)
    p_style_children = ParagraphStyle('Normal', leftIndent=5*mm, firstLineIndent=-2*mm)
    p_style_right = ParagraphStyle('Normal', alignment=TA_RIGHT)
    story = list()                              # list of flowables with the content
    story.append(Spacer(1, 10*mm))              # spacer to not override the title

    pdf_title = 'Guía de direcciones'

    col_widths = [10*mm, 55*mm, 25*mm, 30*mm, 60*mm]
    table1_data = list()

    # add the table header (a table with one row)
    table1_data.append([
        Table([['#', 'Personas', 'Cumpleaños', 'Teléfono', 'Dirección'], ],
              colWidths=col_widths,
              style=TableStyle([
                  ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                  ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
              ]))
    ])

    # #####
    # #####
    # #####

    present_people = {}
    people_list = []

    for m in MarriageDetail.objects.filter(marriage__row_deleted=False, row_time_end__isnull=True,
                                           end_date__isnull=True):
        p1 = m.person_1.current_details
        p2 = m.person_2.current_details

        if p1.is_member or p2.is_member:
            for pk in (m.person_1.pk, m.person_2.pk):
                present_people.setdefault(pk, 0)
                present_people[pk] += 1

            row = {
                'names': [
                    ('{}, {}'.format(p1.last_name.split()[0], p1.first_name), p_style_n),
                    ('({}) {}'.format(p2.last_name.split()[0], p2.first_name), p_style_n),
                ],
                'birthdays': [
                    _format_birthday(p1),
                    _format_birthday(p2),
                ],
                'phones': [
                    p1.phone if p1.phone else '-',
                    p2.phone if p2.phone else '-',
                ],
                'addresses': [
                    p1.address,     # for now we only add one address per row
                ],
            }

            for c in m.children:
                pk = c.person.pk
                if pk not in present_people and not c.is_member:
                    if not c.death_date:
                        row['names'].append((c.first_name, p_style_children))
                        row['birthdays'].append(_format_birthday(c))

                        present_people.setdefault(pk, 0)
                        present_people[pk] += 1

            people_list.append(row)

    for p in PersonDetail.objects.exclude(person__pk__in=present_people).filter(
            person__row_deleted=False, row_time_end__isnull=True, death_date__isnull=True):
        if p.is_member:
            present_people.setdefault(p.person.pk, 0)
            present_people[p.person.pk] += 1

            row = {
                'names': [('{}, {}'.format(p.last_name.split()[0], p.first_name), p_style_n), ],
                'birthdays': [_format_birthday(p), ],
                'phones': [p.phone if p.phone else '-', ],
                'addresses': [p.address, ],
            }

            for c in p.children:
                pk = c.person.pk
                if pk not in present_people and not c.is_member:
                    if not c.death_date:
                        row['names'].append((c.first_name, p_style_children))
                        row['birthdays'].append(_format_birthday(c))

                        present_people.setdefault(pk, 0)
                        present_people[pk] += 1

            people_list.append(row)

    people_list = sorted(
        people_list,
        key=lambda x: x['names'][0])

    for k, v in sorted(present_people.items(), key=lambda x: x[1], reverse=True):
        if v > 1:
            raise ValueError('Person {} appears {} times in the list'.format(k, v))

    # #####
    # #####
    # #####

    # add people
    if not people_list:
        table1_data.append([
            Table([['No hay personas para mostrar', ], ],
                  colWidths=[sum(col_widths) - 50*mm, 50*mm],
                  style=TableStyle([
                      ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                      ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                  ]))
        ])
    else:
        for i, row in enumerate(people_list):
            cell_0 = Paragraph(str(i+1), p_style_right)
            cell_1 = Table(
                [[[Paragraph(e[0], e[1]) for e in row['names']], ], ],
                colWidths=col_widths[1],
                style=TableStyle([
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                ]))
            cell_2 = Table(
                [[[Paragraph(s, p_style_n) for s in row['birthdays']], ], ],
                colWidths=col_widths[2],
                style=TableStyle([
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                ]))
            cell_3 = Table(
                [[[Paragraph(s, p_style_n) for s in row['phones']], ], ],
                colWidths=col_widths[3],
                style=TableStyle([
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                ]))
            cell_4 = Table(
                [[[Paragraph(s, p_style_n) for s in row['addresses']], ], ],
                colWidths=col_widths[4],
                style=TableStyle([
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                ]))

            row = Table(
                [[cell_0, cell_1, cell_2, cell_3, cell_4], ],
                colWidths=col_widths,
                style=TableStyle([
                    ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                    ('LEFTPADDING', (1, 0), (-1, -1), 0),
                    ('RIGHTPADDING', (1, 0), (-1, -1), 0),
                    ('TOPPADDING', (1, 0), (-1, -1), 0),
                    ('BOTTOMPADDING', (1, 0), (-1, -1), 0),
                ]))
            table1_data.append([row, ])

    # add table with entries to the report
    story.append(
        Table(table1_data, colWidths=sum(col_widths), repeatRows=1,
              style=TableStyle([
                  ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                  ('LEFTPADDING', (0, 0), (-1, -1), 0),
                  ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                  ('TOPPADDING', (0, 0), (-1, -1), 0),
                  ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
                  ('BACKGROUND', (0, 0), (-1, 0), colors.gainsboro),
              ]))
    )

    return pdf_title, story


def get_group_members_story(group):
    """
    Get list of flowables.
    :param group:
    :return:
    """
    p_style_n = ParagraphStyle('Normal')
    story = list()                              # list of flowables with the content
    story.append(Spacer(1, 10*mm))              # spacer to not override the title

    pdf_title = 'Miembros del grupo'

    det = group.current_details
    story.append(
        Table(
            [
                ['Tipo:', Paragraph(det.get_type_display(), p_style_n)],
                ['Nombre:', Paragraph(det.name, p_style_n)],
                ['Obs:', Paragraph(str(det.obs), p_style_n)],
            ],
            colWidths=[20*mm, 160*mm], repeatRows=1,
            style=TableStyle([
                ('LEFTPADDING', (0, 0), (-1, -1), 0),
                ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                ('TOPPADDING', (0, 0), (-1, -1), 0),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 2*mm),
                ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ]))
    )
    story.append(Spacer(1, 5*mm))

    col_widths = [10*mm, 75*mm, 75*mm, 20*mm]
    table1_data = list()

    # add the table header (a table with one row)
    table1_data.append([
        Table([['#', 'Nombre', 'Apellido', 'Edad'], ],
              colWidths=col_widths,
              style=TableStyle([
                  ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                  ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
              ]))
    ])

    # add members
    member_list = group.sorted_current_members

    if not member_list:
        table1_data.append([
            Table([['No hay miembros para mostrar', ], ],
                  colWidths=[sum(col_widths), ],
                  style=TableStyle([
                      ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                      ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                  ]))
        ])
    else:
        for i, m in enumerate(member_list):
            p = m.person.current_details
            aux = [
                str(i+1),
                p.first_name,
                p.last_name,
                str(p.age),
            ]
            table1_data.append([
                Table([[Paragraph(s, p_style_n) for s in aux], ],
                      colWidths=col_widths,
                      style=TableStyle([
                          ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                          ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                      ]))
            ])

    # add table with entries to the report
    story.append(
        Table(table1_data, colWidths=sum(col_widths), repeatRows=1,
              style=TableStyle([
                  ('GRID', (0, 0), (-1, -1), 0.5, colors.black),
                  ('LEFTPADDING', (0, 0), (-1, -1), 0),
                  ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                  ('TOPPADDING', (0, 0), (-1, -1), 0),
                  ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
                  ('BACKGROUND', (0, 0), (-1, 0), colors.gainsboro),
              ]))
    )

    return pdf_title, story


def _format_birthday(person):
    """
    Returns a string of the birth date.
    """
    s = '-'
    if person.birth_date:
        s = '{} {}'.format(person.birth_date.day, month_dict[person.birth_date.month][:3])

    if person.death_date:
        s = '\u271F ' + s

    return s
