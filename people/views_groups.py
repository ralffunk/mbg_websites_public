# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import xlsxwriter
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone as tz
from io import BytesIO
from mbg_websites import utils
from people import pdf_reports
from people.models import Group, GroupDetail


@login_required()
@permission_required('people.list_groups', raise_exception=True)
def groups_list(request):
    """
    List groups.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de grupos', ),
    ]

    qs = GroupDetail.objects.filter(group__row_deleted=False, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    qs = qs.filter(name__icontains=ctx['sf_text'])

    ctx['group_type_list'] = GroupDetail.GROUP_TYPES
    if request.GET.get('sf_type', '') in [e[0] for e in GroupDetail.GROUP_TYPES]:
        ctx['sf_type'] = request.GET.get('sf_type')
        qs = qs.filter(type=ctx['sf_type'])
    else:
        ctx['sf_type'] = 'A'

    ctx['sf_order'] = '0'
    qs = qs.order_by('type', 'name')

    ctx['sf'] = 'sf_text={}&sf_type={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_type'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('people:groups-read', args=[e.group.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'people/group_list.html', ctx)


@login_required()
@permission_required('people.delete_group', raise_exception=True)
def groups_list_deleted(request):
    """
    List deleted groups.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Grupos', reverse('people:groups-list')),
        ('Grupos eliminados', ),
    ]

    qs = GroupDetail.objects.filter(group__row_deleted=True, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    qs = qs.filter(name__icontains=ctx['sf_text'])

    ctx['group_type_list'] = GroupDetail.GROUP_TYPES
    if request.GET.get('sf_type', '') in [e[0] for e in GroupDetail.GROUP_TYPES]:
        ctx['sf_type'] = request.GET.get('sf_type')
        qs = qs.filter(type=ctx['sf_type'])
    else:
        ctx['sf_type'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('type', 'name')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_type={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_type'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('people:groups-read', args=[e.group.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'people/group_deleted_list.html', ctx)


@login_required()
@permission_required('people.create_group', raise_exception=True)
def groups_create(request):
    """
    Create a group.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear nuevo grupo')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Grupos', reverse('people:groups-list')),
        ('Crear nuevo grupo', ),
    ]
    ctx['CRUD'] = 'C'
    ctx['group'] = Group()
    return _groups_create_or_update(request, ctx)


@login_required()
@permission_required('people.list_groups', raise_exception=True)
def groups_read(request, group_id):
    """
    Read a group.
    :param request: HttpRequest
    :param group_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles del grupo')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Grupos', reverse('people:groups-list')),
        ('Grupo {}'.format(group_id), ),
    ]
    ctx['CRUD'] = 'R'
    ctx['group'] = get_object_or_404(Group, pk=group_id)
    ctx['current_details'] = ctx['group'].current_details
    ctx['history_list'] = ctx['group'].details.order_by('-row_time_start')[:5]

    if ctx['group'].row_deleted:
        messages.error(request, 'Este grupo se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Grupos eliminados', reverse('people:groups-list-deleted')))

    return render(request, 'people/group_form.html', ctx)


@login_required()
@permission_required('people.update_group', raise_exception=True)
def groups_update(request, group_id):
    """
    Update a group.
    :param request: HttpRequest
    :param group_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar grupo')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Grupos', reverse('people:groups-list')),
        ('Grupo {}'.format(group_id), reverse('people:groups-read', args=[group_id])),
        ('Modificar grupo', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['group'] = get_object_or_404(Group, pk=group_id, row_deleted=False)
    ctx['current_details'] = ctx['group'].current_details
    return _groups_create_or_update(request, ctx)


@login_required()
@permission_required('people.delete_group', raise_exception=True)
def groups_delete(request, group_id):
    """
    Delete a group.
    :param request: HttpRequest
    :param group_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar grupo')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Grupos', reverse('people:groups-list')),
        ('Grupo {}'.format(group_id), reverse('people:groups-read', args=[group_id])),
        ('Eliminar grupo', ),
    ]
    ctx['CRUD'] = 'D'
    ctx['group'] = get_object_or_404(Group, pk=group_id, row_deleted=False)
    ctx['current_details'] = ctx['group'].current_details

    # for now there are no additional checks
    ctx['group'].can_be_deleted = True
    if ctx['group'].current_members.exists():
        ctx['group'].can_be_deleted = False
        messages.error(request, 'Este grupo no puede ser eliminado porque tiene miembros.')

    if request.method == 'POST' and ctx['group'].can_be_deleted:
        ctx['group'].do_delete(request.user)
        return redirect('people:groups-list')
    else:
        return render(request, 'people/group_form.html', ctx)


@login_required()
@permission_required('people.delete_group', raise_exception=True)
def groups_delete_undo(request, group_id):
    """
    Undo the delete of a group.
    :param request: HttpRequest
    :param group_id:
    :return: HttpResponse
    """
    group = get_object_or_404(Group, pk=group_id)

    if request.method == 'POST':
        if group.row_deleted:
            group.undo_delete(request.user)

    return redirect('people:groups-read', group.pk)


@login_required()
@permission_required('people.list_groups', raise_exception=True)
def groups_history(request, group_id):
    """
    View changes history of a group.
    :param request: HttpRequest
    :param group_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Grupos', reverse('people:groups-list')),
        ('Grupo {}'.format(group_id), reverse('people:groups-read', args=[group_id])),
        ('Historial del grupo', ),
    ]
    ctx['group'] = get_object_or_404(Group, pk=group_id)
    qs = ctx['group'].details

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(row_action__icontains=ctx['sf_text'])
    q_t2 = Q(name__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2)

    ctx.update(utils.get_sf_dates(request.GET))
    qs = qs.filter(row_time_start__range=(ctx['sf_date_start'], ctx['sf_date_end']))

    ctx['sf_order'] = '0'
    qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['group'].row_deleted:
        messages.error(request, 'Este grupo se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Grupos eliminados', reverse('people:groups-list-deleted')))

    return render(request, 'people/group_history_list.html', ctx)


def _groups_create_or_update(request, ctx):
    """
    Create or update a group.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    ctx['group_type_list'] = GroupDetail.GROUP_TYPES

    if request.method == 'POST':
        new_data = dict()

        new_data['type_'] = '99'
        if request.POST.get('type', '') in [e[0] for e in GroupDetail.GROUP_TYPES]:
            new_data['type_'] = request.POST.get('type')

        new_data['name'] = request.POST.get('name', '')
        new_data['obs'] = request.POST.get('obs', '')

        try:
            if ctx['CRUD'] == 'C':
                ctx['group'].do_create(user=request.user, **new_data)
            else:
                ctx['group'].do_update(user=request.user, **new_data)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'people/group_form.html', ctx)

        return redirect('people:groups-read', ctx['group'].pk)
    else:
        return render(request, 'people/group_form.html', ctx)


@login_required()
@permission_required('people.list_groups', raise_exception=True)
def groups_members_excel(request, group_id):
    """
    Group member list as EXCEL.
    :param request: HttpRequest
    :param group_id:
    :return: HttpResponse
    """
    group = get_object_or_404(Group, pk=group_id, row_deleted=False)

    filename = 'lista-de-miembros-del-grupo'
    column_title_list = (
        'ID interno',
        'Nombre',
        'Apellido',
        'Género',
        'Fecha de nacimiento',
        'Email',
        'Teléfono',
        'Dirección',
    )

    temp_buffer = BytesIO()
    wb = xlsxwriter.Workbook(temp_buffer, options={'in_memory': True})
    ws = wb.add_worksheet('list')
    date_format = wb.add_format({'num_format': 'dd-mm-yyyy'})

    row = 0
    ws.write(row, 0, 'Miembros del grupo')
    row += 1
    ws.write(row, 0, 'Archivo descargado por "{}" el {} a las {}'.format(
        request.user.profile, *tz.localtime(tz.now()).strftime('%Y-%m-%d %H:%M').split()))
    row += 1
    row += 1

    det = group.current_details
    ws.write(row, 0, 'Tipo: {}'.format(det.get_type_display()))
    row += 1
    ws.write(row, 0, 'Nombre: {}'.format(det.name))
    row += 1
    ws.write(row, 0, 'Obs: {}'.format(det.obs))
    row += 1
    row += 1

    for j, col_title in enumerate(column_title_list):
        ws.write(row, j, col_title)
    row += 1

    for m in group.sorted_current_members:
        p = m.person.current_details
        j = 0

        ws.write(row, j, p.person.pk)
        j += 1
        ws.write(row, j, p.first_name)
        j += 1
        ws.write(row, j, p.complete_last_name)
        j += 1
        ws.write(row, j, p.get_gender_display())
        j += 1
        ws.write(row, j, p.birth_date, date_format)
        j += 1
        ws.write(row, j, p.email)
        j += 1
        ws.write(row, j, p.phone)
        j += 1
        ws.write(row, j, p.address)

        row += 1

    wb.close()
    return utils.make_excel(temp_buffer, filename)


@login_required()
@permission_required('people.list_groups', raise_exception=True)
def groups_members_pdf(request, group_id):
    """
    Group member list as PDF.
    :param request: HttpRequest
    :param group_id:
    :return: HttpResponse
    """
    group = get_object_or_404(Group, pk=group_id, row_deleted=False)

    return pdf_reports.make_pdf(
        request.user,
        *pdf_reports.get_group_members_story(group))


@login_required()
@permission_required('people.list_groups', raise_exception=True)
def groups_members_history(request, group_id):
    """
    View changes history of group members.
    :param request: HttpRequest
    :param group_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Grupos', reverse('people:groups-list')),
        ('Grupo {}'.format(group_id), reverse('people:groups-read', args=[group_id])),
        ('Historial de miembros', ),
    ]
    ctx['group'] = get_object_or_404(Group, pk=group_id, row_deleted=False)
    qs = ctx['group'].groupmember_set

    ctx['sf_order'] = '0'
    qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_order={}'.format(
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'people/group_members_history_list.html', ctx)
