# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import datetime
import xlsxwriter
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone as tz
from io import BytesIO
from mbg_websites import utils
from people import pdf_reports
from people.models import Person, PersonDetail, MarriageDetail, Group, GroupDetail, GroupMember


@login_required()
@permission_required('people.list_people', raise_exception=True)
def people_list(request):
    """
    List people.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de personas', ),
    ]

    qs = PersonDetail.objects.filter(person__row_deleted=False, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(first_name__icontains=ctx['sf_text'])
    q_t2 = Q(last_name__icontains=ctx['sf_text'])
    q_t3 = Q(married_last_name__icontains=ctx['sf_text'])
    q_t4 = Q(email__icontains=ctx['sf_text'])
    q_t5 = Q(phone__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3 | q_t4 | q_t5)

    if request.GET.get('sf_status', '') == 'D':
        ctx['sf_status'] = 'D'
        qs = qs.filter(death_date__isnull=False)
    elif request.GET.get('sf_status', '') == 'ND':
        ctx['sf_status'] = 'ND'
        qs = qs.filter(death_date__isnull=True)
    else:
        ctx['sf_status'] = 'A'

    q_is_member = Q(mbg_entry_date__isnull=False, mbg_exit_date__isnull=True)
    if request.GET.get('sf_membership', '') == 'M':
        ctx['sf_membership'] = 'M'
        qs = qs.filter(q_is_member)
    elif request.GET.get('sf_membership', '') == 'NM':
        ctx['sf_membership'] = 'NM'
        qs = qs.exclude(q_is_member)
    else:
        ctx['sf_membership'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = sorted(qs, key=lambda x: (x.first_name, x.complete_last_name))
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
        qs = qs.order_by('birth_date', 'first_name')
    else:
        ctx['sf_order'] = '0'
        qs = sorted(qs, key=lambda x: (x.complete_last_name, x.first_name))

    ctx['sf'] = 'sf_text={}&sf_status={}&sf_membership={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_status'],
        ctx['sf_membership'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('people:people-read', args=[e.person.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'people/person_list.html', ctx)


@login_required()
@permission_required('people.list_people', raise_exception=True)
def people_list_excel(request):
    """
    Download people list as EXCEL.
    :param request: HttpRequest
    :return: HttpResponse
    """
    filename = 'lista-de-personas'
    temp_buffer = _people_list_excel(
        request.user,
        'Lista de personas',
        sorted(
            PersonDetail.objects.filter(person__row_deleted=False, row_time_end__isnull=True),
            key=lambda x: (x.complete_last_name, x.first_name)))

    return utils.make_excel(temp_buffer, filename)


@login_required()
@permission_required('people.list_people', raise_exception=True)
def people_list_members_excel(request):
    """
    Download member list as EXCEL.
    :param request: HttpRequest
    :return: HttpResponse
    """
    filename = 'lista-de-miembros'
    temp_buffer = _people_list_excel(
        request.user,
        'Lista de miembros',
        sorted(
            PersonDetail.objects.filter(person__row_deleted=False, row_time_end__isnull=True,
                                        mbg_entry_date__isnull=False, mbg_exit_date__isnull=True),
            key=lambda x: (x.complete_last_name, x.first_name)))

    return utils.make_excel(temp_buffer, filename)


def _people_list_excel(user, title_str, people_list_):
    column_title_list = (
        'ID interno',
        'Nombre',
        'Apellido',
        'Apellido de casada',
        'Género',
        'Padre',
        'Madre',
        'Fecha de nacimiento',
        'Lugar de nacimiento',
        'Fecha de fallecimiento',
        'Lugar de fallecimiento',
        'Fecha de bautismo',
        'Lugar de bautismo',
        'Documento de identidad',
        'Email',
        'Teléfono',
        'Dirección',
        'Ubicación donde retira correo',
        'Observación',
        'Fecha de entrada',
        'Fecha de salida',
        'Suscripción email',
        'Suscripción impresa',
    )

    temp_buffer = BytesIO()
    wb = xlsxwriter.Workbook(temp_buffer, options={'in_memory': True})
    ws = wb.add_worksheet('list')
    date_format = wb.add_format({'num_format': 'dd-mm-yyyy'})

    row = 0
    ws.write(row, 0, title_str)
    row += 1
    ws.write(row, 0, 'Archivo descargado por "{}" el {} a las {}'.format(
        user.profile, *tz.localtime(tz.now()).strftime('%Y-%m-%d %H:%M').split()))
    row += 1
    row += 1

    for j, col_title in enumerate(column_title_list):
        ws.write(row, j, col_title)
    row += 1

    for p in people_list_:
        j = 0
        ws.write(row, j, p.person.pk)
        j += 1
        ws.write(row, j, p.first_name)
        j += 1
        ws.write(row, j, p.last_name)
        j += 1
        ws.write(row, j, p.married_last_name)
        j += 1
        ws.write(row, j, p.get_gender_display())
        j += 1

        s = ''
        if p.father:
            s = '{}: {}'.format(p.father.pk, p.father)
        ws.write(row, j, s)
        j += 1

        s = ''
        if p.mother:
            s = '{}: {}'.format(p.mother.pk, p.mother)
        ws.write(row, j, s)
        j += 1

        ws.write(row, j, p.birth_date, date_format)
        j += 1
        ws.write(row, j, p.birth_location)
        j += 1
        ws.write(row, j, p.death_date, date_format)
        j += 1
        ws.write(row, j, p.death_location)
        j += 1
        ws.write(row, j, p.baptism_date, date_format)
        j += 1
        ws.write(row, j, p.baptism_location)
        j += 1
        ws.write(row, j, p.doc_id)
        j += 1
        ws.write(row, j, p.email)
        j += 1
        ws.write(row, j, p.phone)
        j += 1
        ws.write(row, j, p.address)
        j += 1
        ws.write(row, j, p.mail_box)
        j += 1
        ws.write(row, j, p.obs)
        j += 1
        ws.write(row, j, p.mbg_entry_date, date_format)
        j += 1
        ws.write(row, j, p.mbg_exit_date, date_format)
        j += 1
        ws.write(row, j, p.mbg_subscription_email)
        j += 1
        ws.write(row, j, p.mbg_subscription_print)

        row += 1

    wb.close()
    return temp_buffer


@login_required()
@permission_required('people.list_people', raise_exception=True)
def people_list_pdf(request):
    """
    Download people list as PDF.
    :param request: HttpRequest
    :return: HttpResponse
    """
    return pdf_reports.make_pdf(
        request.user,
        *pdf_reports.get_people_list())


@login_required()
@permission_required('people.delete_person', raise_exception=True)
def people_list_deleted(request):
    """
    List deleted people.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Personas', reverse('people:people-list')),
        ('Personas eliminadas', ),
    ]

    qs = PersonDetail.objects.filter(person__row_deleted=True, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(first_name__icontains=ctx['sf_text'])
    q_t2 = Q(last_name__icontains=ctx['sf_text'])
    q_t3 = Q(married_last_name__icontains=ctx['sf_text'])
    q_t4 = Q(phone__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3 | q_t4)

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = sorted(qs, key=lambda x: (x.first_name, x.complete_last_name))
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '0'
        qs = sorted(qs, key=lambda x: (x.complete_last_name, x.first_name))
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('people:people-read', args=[e.person.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'people/person_deleted_list.html', ctx)


@login_required()
@permission_required('people.create_person', raise_exception=True)
def people_create(request):
    """
    Create a person.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear nueva persona')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Personas', reverse('people:people-list')),
        ('Crear nueva persona', ),
    ]
    ctx['CRUD'] = 'C'
    ctx['person'] = Person()
    return _people_create_or_update(request, ctx)


@login_required()
@permission_required('people.list_people', raise_exception=True)
def people_read(request, person_id):
    """
    Read a person.
    :param request: HttpRequest
    :param person_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles de la persona')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Personas', reverse('people:people-list')),
        ('Persona {}'.format(person_id), ),
    ]
    ctx['CRUD'] = 'R'
    ctx['person'] = get_object_or_404(Person, pk=person_id)
    ctx['current_details'] = ctx['person'].current_details
    ctx['history_list'] = ctx['person'].details.order_by('-row_time_start')[:5]

    siblings_list = []
    for p in PersonDetail.objects.exclude(person=ctx['person']).filter(
            person__row_deleted=False, row_time_end__isnull=True).order_by('birth_date'):
        if ctx['current_details'].father and ctx['current_details'].father == p.father:
            p.same_father = True
        else:
            p.same_father = False

        if ctx['current_details'].mother and ctx['current_details'].mother == p.mother:
            p.same_mother = True
        else:
            p.same_mother = False

        if p.same_father or p.same_mother:
            siblings_list.append(p)
    ctx['siblings_list'] = siblings_list

    marriage_list = []
    for m in MarriageDetail.objects.filter(
            marriage__row_deleted=False, row_time_end__isnull=True).order_by('start_date'):
        if ctx['person'] == m.person_1 or ctx['person'] == m.person_2:
            marriage_list.append(m)
    ctx['marriage_list'] = marriage_list

    children_list = []
    for p in PersonDetail.objects.exclude(person=ctx['person']).filter(
            person__row_deleted=False, row_time_end__isnull=True).order_by('birth_date'):
        if ctx['person'] == p.father or ctx['person'] == p.mother:
            children_list.append(p)
    ctx['children_list'] = children_list

    current_groups = [e.group.pk for e in ctx['person'].current_groups]
    ctx['possible_group_list'] = GroupDetail.objects.exclude(group__pk__in=current_groups).filter(
        group__row_deleted=False, row_time_end__isnull=True).order_by('type', 'name')

    if ctx['person'].row_deleted:
        messages.error(request, 'Esta persona se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Personas eliminadas', reverse('people:people-list-deleted')))

    if ctx['current_details'].death_date:
        if ctx['current_details'].is_member:
            messages.error(request, 'Esta persona falleció pero aún figura como miembro.')
        if len(list(filter(lambda x: x.start_date and not x.end_date, ctx['marriage_list']))):
            messages.error(request, 'Esta persona falleció pero aún tiene matrimonio activo.')

    return render(request, 'people/person_form.html', ctx)


@login_required()
@permission_required('people.update_person', raise_exception=True)
def people_update(request, person_id):
    """
    Update a person.
    :param request: HttpRequest
    :param person_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar persona')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Personas', reverse('people:people-list')),
        ('Persona {}'.format(person_id), reverse('people:people-read', args=[person_id])),
        ('Modificar persona', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['person'] = get_object_or_404(Person, pk=person_id, row_deleted=False)
    ctx['current_details'] = ctx['person'].current_details
    return _people_create_or_update(request, ctx)


@login_required()
@permission_required('people.delete_person', raise_exception=True)
def people_delete(request, person_id):
    """
    Delete a person.
    :param request: HttpRequest
    :param person_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar persona')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Personas', reverse('people:people-list')),
        ('Persona {}'.format(person_id), reverse('people:people-read', args=[person_id])),
        ('Eliminar persona', ),
    ]
    ctx['CRUD'] = 'D'
    ctx['person'] = get_object_or_404(Person, pk=person_id, row_deleted=False)
    ctx['current_details'] = ctx['person'].current_details

    ctx['person'].can_be_deleted = True
    if ctx['person'].userprofile_set.exists():
        ctx['person'].can_be_deleted = False
        messages.error(request, 'Esta persona no puede ser eliminada porque está asociada a un '
                                'usuario.')
    elif (ctx['person'].marriagedetail_p1_set.filter(marriage__row_deleted=False,
                                                     row_time_end__isnull=True).exists()
          or ctx['person'].marriagedetail_p2_set.filter(marriage__row_deleted=False,
                                                        row_time_end__isnull=True).exists()):
        ctx['person'].can_be_deleted = False
        messages.error(request, 'Esta persona no puede ser eliminada porque está asociada a un '
                                'matrimonio.')
    elif ctx['person'].current_groups.exists():
        ctx['person'].can_be_deleted = False
        messages.error(request, 'Esta persona no puede ser eliminada porque está asociada a por lo '
                                'menos un grupo.')

    if request.method == 'POST' and ctx['person'].can_be_deleted:
        ctx['person'].do_delete(request.user)
        return redirect('people:people-list')
    else:
        return render(request, 'people/person_form.html', ctx)


@login_required()
@permission_required('people.delete_person', raise_exception=True)
def people_delete_undo(request, person_id):
    """
    Undo the delete of a person.
    :param request: HttpRequest
    :param person_id:
    :return: HttpResponse
    """
    person = get_object_or_404(Person, pk=person_id)

    if request.method == 'POST':
        if person.row_deleted:
            person.undo_delete(request.user)

    return redirect('people:people-read', person.pk)


@login_required()
@permission_required('people.list_people', raise_exception=True)
def people_history(request, person_id):
    """
    View changes history of a person.
    :param request: HttpRequest
    :param person_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Personas', reverse('people:people-list')),
        ('Persona {}'.format(person_id), reverse('people:people-read', args=[person_id])),
        ('Historial de la persona', ),
    ]
    ctx['person'] = get_object_or_404(Person, pk=person_id)
    qs = ctx['person'].details

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(row_action__icontains=ctx['sf_text'])
    q_t2 = Q(first_name__icontains=ctx['sf_text'])
    q_t3 = Q(last_name__icontains=ctx['sf_text'])
    q_t4 = Q(married_last_name__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3 | q_t4)

    ctx.update(utils.get_sf_dates(request.GET))
    qs = qs.filter(row_time_start__range=(ctx['sf_date_start'], ctx['sf_date_end']))

    ctx['sf_order'] = '0'
    qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['person'].row_deleted:
        messages.error(request, 'Esta persona se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Personas eliminadas', reverse('people:people-list-deleted')))

    return render(request, 'people/person_history_list.html', ctx)


def _people_create_or_update(request, ctx):
    """
    Create or update a person.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    ctx['gender_list'] = PersonDetail.GENDERS
    ctx['male_person_list'] = sorted(
        [(e.person.pk, str(e)) for e in PersonDetail.objects.filter(
            person__row_deleted=False, row_time_end__isnull=True, gender='M').exclude(
            person__pk=ctx['person'].pk)],
        key=lambda x: x[1])
    ctx['female_person_list'] = sorted(
        [(e.person.pk, str(e)) for e in PersonDetail.objects.filter(
            person__row_deleted=False, row_time_end__isnull=True, gender='F').exclude(
            person__pk=ctx['person'].pk)],
        key=lambda x: x[1])

    if request.method == 'POST':
        new_data = dict()
        new_data['first_name'] = request.POST.get('first_name', '')
        new_data['last_name'] = request.POST.get('last_name', '')
        new_data['married_last_name'] = request.POST.get('married_last_name', '')
        new_data['gender'] = request.POST.get('gender', '')

        if new_data['gender'] == 'M':
            # for now only females can have married_last_name
            new_data['married_last_name'] = ''

        new_data['father'] = None
        if request.POST.get('father', ''):
            try:
                new_data['father'] = Person.objects.exclude(pk=ctx['person'].pk).get(
                    pk=int(request.POST.get('father')), row_deleted=False)
            except (ValueError, Person.DoesNotExist):
                pass

        new_data['mother'] = None
        if request.POST.get('mother', ''):
            try:
                new_data['mother'] = Person.objects.exclude(pk=ctx['person'].pk).get(
                    pk=int(request.POST.get('mother')), row_deleted=False)
            except (ValueError, Person.DoesNotExist):
                pass

        if new_data['father'] and new_data['father'] == new_data['mother']:
            messages.error(request, 'Padre y madre no pueden ser las mismas personas.')
            return render(request, 'people/person_form.html', ctx)

        new_data['birth_location'] = request.POST.get('birth_location', '')
        new_data['death_location'] = request.POST.get('death_location', '')
        new_data['baptism_location'] = request.POST.get('baptism_location', '')

        new_data['birth_date'] = None
        if request.POST.get('birth_date', ''):
            try:
                new_data['birth_date'] = utils.parse_date(
                    request.POST.get('birth_date'), raise_error=True).date()
            except ValueError:
                messages.error(request, 'El formato de la fecha de nacimiento no es válido.')
                return render(request, 'people/person_form.html', ctx)

        new_data['death_date'] = None
        if request.POST.get('death_date', ''):
            try:
                new_data['death_date'] = utils.parse_date(
                    request.POST.get('death_date'), raise_error=True).date()
            except ValueError:
                messages.error(request, 'El formato de la fecha de fallecimiento no es válido.')
                return render(request, 'people/person_form.html', ctx)

        if (new_data['birth_date'] and new_data['death_date']
                and new_data['birth_date'] > new_data['death_date']):
            messages.error(request, 'La fecha de nacimiento debe ser anterior al fallecimiento.')
            return render(request, 'people/person_form.html', ctx)

        new_data['baptism_date'] = None
        if request.POST.get('baptism_date', ''):
            try:
                new_data['baptism_date'] = utils.parse_date(
                    request.POST.get('baptism_date'), raise_error=True).date()
            except ValueError:
                messages.error(request, 'El formato de la fecha de bautismo no es válido.')
                return render(request, 'people/person_form.html', ctx)

        if (new_data['birth_date'] and new_data['baptism_date']
                and new_data['birth_date'] > new_data['baptism_date']):
            messages.error(request, 'La fecha de nacimiento debe ser anterior al bautismo.')
            return render(request, 'people/person_form.html', ctx)

        if (new_data['baptism_date'] and new_data['death_date']
                and new_data['baptism_date'] > new_data['death_date']):
            messages.error(request, 'La fecha de bautismo debe ser anterior al fallecimiento.')
            return render(request, 'people/person_form.html', ctx)

        new_data['mbg_entry_date'] = None
        if request.POST.get('mbg_entry_date', ''):
            try:
                new_data['mbg_entry_date'] = utils.parse_date(
                    request.POST.get('mbg_entry_date'), raise_error=True).date()
            except ValueError:
                messages.error(request, 'El formato de la fecha de entrada no es válido.')
                return render(request, 'people/person_form.html', ctx)

        new_data['mbg_exit_date'] = None
        if request.POST.get('mbg_exit_date', ''):
            try:
                new_data['mbg_exit_date'] = utils.parse_date(
                    request.POST.get('mbg_exit_date'), raise_error=True).date()
            except ValueError:
                messages.error(request, 'El formato de la fecha de salida no es válido.')
                return render(request, 'people/person_form.html', ctx)

        if (new_data['mbg_entry_date'] and new_data['mbg_exit_date']
                and new_data['mbg_entry_date'] > new_data['mbg_exit_date']):
            messages.error(request, 'La fecha de entrada debe ser anterior a la salida.')
            return render(request, 'people/person_form.html', ctx)

        new_data['mbg_subscription_email'] = False
        if request.POST.get('mbg_subscription_email', '') == 'True':
            new_data['mbg_subscription_email'] = True
        new_data['mbg_subscription_print'] = False
        if request.POST.get('mbg_subscription_print', '') == 'True':
            new_data['mbg_subscription_print'] = True

        new_data['doc_id'] = request.POST.get('doc_id', '')
        new_data['email'] = request.POST.get('email', '')
        new_data['phone'] = request.POST.get('phone', '')
        new_data['address'] = request.POST.get('address', '')
        new_data['mail_box'] = request.POST.get('mail_box', '')
        new_data['obs'] = request.POST.get('obs', '')

        try:
            if ctx['CRUD'] == 'C':
                ctx['person'].do_create(user=request.user, **new_data)
            else:
                ctx['person'].do_update(user=request.user, **new_data)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'people/person_form.html', ctx)

        return redirect('people:people-read', ctx['person'].pk)
    else:
        return render(request, 'people/person_form.html', ctx)


@login_required()
@permission_required('people.list_people', raise_exception=True)
def people_birthday_list(request):
    """
    List birthdays per month.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Personas', reverse('people:people-list')),
        ('Lista de cumpleaños', ),
    ]

    qs = PersonDetail.objects.filter(person__row_deleted=False, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(first_name__icontains=ctx['sf_text'])
    q_t2 = Q(last_name__icontains=ctx['sf_text'])
    q_t3 = Q(married_last_name__icontains=ctx['sf_text'])
    q_t4 = Q(email__icontains=ctx['sf_text'])
    q_t5 = Q(phone__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3 | q_t4 | q_t5)

    ctx['month_list'] = utils.month_dict.items()
    try:
        ctx['sf_month'] = int(request.GET.get('sf_month', 0))
    except ValueError:
        pass
    if 'sf_month' not in ctx or ctx['sf_month'] not in utils.month_dict:
        ctx['sf_month'] = tz.now().month
    ctx['month_name'] = utils.month_dict[ctx['sf_month']]
    qs = qs.filter(birth_date__month=ctx['sf_month'])

    if request.GET.get('sf_status', '') == 'ND':
        ctx['sf_status'] = 'ND'
        qs = qs.filter(death_date__isnull=True)
    else:
        ctx['sf_status'] = 'A'

    ctx['sf_order'] = '0'

    ctx['sf'] = 'sf_text={}&sf_month={}&sf_status={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_month'],
        ctx['sf_status'],
        ctx['sf_order'])

    ctx['year'] = tz.now().year
    for e in qs:
        e.url = reverse('people:people-read', args=[e.person.pk])
        e.birth_date_current_year = datetime.date(ctx['year'], ctx['sf_month'], e.birth_date.day)

    paginator = Paginator(
        sorted(qs, key=lambda x: (x.birth_date.day, x.first_name)),
        50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'people/birthday_list.html', ctx)


@login_required()
@permission_required('people.update_person', raise_exception=True)
def people_groups_history(request, person_id):
    """
    View changes history of group memberships.
    :param request: HttpRequest
    :param person_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Personas', reverse('people:people-list')),
        ('Persona {}'.format(person_id), ),
        ('Historial de grupos', ),
    ]
    ctx['person'] = get_object_or_404(Person, pk=person_id)
    qs = ctx['person'].groupmember_set.all()

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('-row_time_start')
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
        qs = qs.order_by('-row_time_end')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('group', '-row_time_start')

    ctx['sf'] = 'sf_order={}'.format(
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['person'].row_deleted:
        messages.error(request, 'Esta persona se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Personas eliminadas', reverse('people:people-list-deleted')))

    return render(request, 'people/person_groups_history_list.html', ctx)


@login_required()
@permission_required('people.update_person', raise_exception=True)
def people_groups_add(request, person_id):
    """
    Add this person to a group.
    :param request: HttpRequest
    :param person_id:
    :return: HttpResponse
    """
    person = get_object_or_404(Person, pk=person_id, row_deleted=False)

    if request.method == 'POST':
        m = GroupMember()
        m.group = get_object_or_404(Group, pk=request.POST.get('group', ''), row_deleted=False)
        m.person = person
        m.obs = request.POST.get('obs', '')
        m.row_user_start = request.user

        try:
            m.full_clean()
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return redirect('people:people-read', person.pk)

        m.save()
        messages.success(request, 'Se ha agregado la persona al grupo especificado.')

    return redirect('people:people-read', person.pk)


@login_required()
@permission_required('people.update_person', raise_exception=True)
def people_groups_edit(request, person_id):
    """
    Edit a group membership of this person.
    :param request: HttpRequest
    :param person_id:
    :return: HttpResponse
    """
    person = get_object_or_404(Person, pk=person_id, row_deleted=False)

    if request.method == 'POST':
        m_old = get_object_or_404(GroupMember, pk=request.POST.get('m_pk'))

        m_new = GroupMember()
        m_new.group = m_old.group
        m_new.person = m_old.person
        m_new.obs = request.POST.get('obs', '')
        m_new.row_user_start = request.user

        try:
            m_new.full_clean()
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return redirect('people:people-read', person.pk)

        m_new.save()

        m_old.row_time_end = tz.now()
        m_old.row_user_end = request.user
        m_old.save()
        messages.success(request, 'Se ha modificado la membresía al grupo especificado.')

    return redirect('people:people-read', person.pk)


@login_required()
@permission_required('people.update_person', raise_exception=True)
def people_groups_remove(request, person_id):
    """
    Remove a group membership of this person.
    :param request: HttpRequest
    :param person_id:
    :return: HttpResponse
    """
    person = get_object_or_404(Person, pk=person_id, row_deleted=False)

    if request.method == 'POST':
        m = get_object_or_404(GroupMember, pk=request.POST.get('m_pk'))
        m.row_time_end = tz.now()
        m.row_user_end = request.user
        m.save()
        messages.success(request, 'Se ha quitado la membresía al grupo especificado.')

    return redirect('people:people-read', person.pk)
