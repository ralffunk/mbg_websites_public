(function () {  // start of closure
    $(document).ready(function () {
        // reset inputs before displaying modals
        $('#m_editGroup').on('show.bs.modal', function (event) {
            $('#m_editGroup_pk').val($(event.relatedTarget).data('pk'));
            $('#m_editGroup_obs').val($(event.relatedTarget).data('obs'));
        });
        $('#m_removeGroup').on('show.bs.modal', function (event) {
            $('#m_removeGroup_pk').val($(event.relatedTarget).data('pk'));
        });

        // set focus when displaying modals
        $('#m_addGroup').on('shown.bs.modal', function () {
            $('#m_addGroup_group').focus();
        });
        $('#m_editGroup').on('shown.bs.modal', function () {
            $('#m_editGroup_obs').focus();
        });
        $('#m_removeGroup').on('shown.bs.modal', function () {
            $('#m_removeGroup_confirmButton').focus();
        });
    });
})();   // end of closure
