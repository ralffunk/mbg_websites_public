# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0002_persondetail_row_user_insert'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='persondetail',
            name='row_user',
        ),
    ]
