# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('people', '0007_auto_20160907_0646'),
    ]

    operations = [
        migrations.CreateModel(
            name='GroupMember',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('obs', models.CharField(default='', max_length=1000, blank=True)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('group', models.ForeignKey(to='people.Group', related_name='members')),
                ('person', models.ForeignKey(to='people.Person', on_delete=django.db.models.deletion.PROTECT)),
                ('row_user_end', models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='groupmember_end_set')),
                ('row_user_start', models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='groupmember_start_set')),
            ],
            options={
                'default_permissions': (),
            },
        ),
    ]
