# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0008_groupmember'),
    ]

    operations = [
        migrations.AddField(
            model_name='persondetail',
            name='mbg_subscription_email',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='mbg_subscription_print',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='groupmember',
            name='group',
            field=models.ForeignKey(to='people.Group'),
        ),
    ]
