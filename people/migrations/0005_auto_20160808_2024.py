# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('people', '0004_auto_20160713_1544'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('row_deleted', models.BooleanField(default=False)),
            ],
            options={
                'permissions': (('list_groups', 'ver lista de grupos'), ('create_group', 'crear grupo'), ('update_group', 'modificar grupo'), ('delete_group', 'eliminar grupo')),
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='GroupDetail',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=50, default='', error_messages={'blank': 'El nombre del grupo no puede quedar vacío.'})),
                ('obs', models.CharField(max_length=1000, blank=True, default='')),
                ('row_action', models.CharField(max_length=6, default='CREATE')),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(null=True, blank=True)),
                ('group', models.ForeignKey(to='people.Group', related_name='details')),
                ('leaders', models.ForeignKey(to='people.Marriage')),
                ('row_user_insert', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.PROTECT, blank=True, null=True)),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.AddField(
            model_name='persondetail',
            name='mbg_group',
            field=models.ForeignKey(to='people.Group', on_delete=django.db.models.deletion.SET_NULL, blank=True, null=True),
        ),
    ]
