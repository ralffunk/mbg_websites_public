# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0005_auto_20160808_2024'),
    ]

    operations = [
        migrations.AddField(
            model_name='persondetail',
            name='father',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='people.Person', null=True, related_name='father_set', blank=True),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='mother',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='people.Person', null=True, related_name='mother_set', blank=True),
        ),
    ]
