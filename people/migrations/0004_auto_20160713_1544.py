# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('people', '0003_remove_persondetail_row_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Marriage',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('row_deleted', models.BooleanField(default=False)),
            ],
            options={
                'permissions': (('list_marriages', 'ver lista de matrimonios'), ('create_marriage', 'crear matrimonio'), ('update_marriage', 'modificar matrimonio'), ('delete_marriage', 'eliminar matrimonio')),
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='MarriageDetail',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('location', models.CharField(max_length=200, blank=True, default='')),
                ('start_date', models.DateField(blank=True, null=True)),
                ('end_date', models.DateField(blank=True, null=True)),
                ('obs', models.CharField(max_length=1000, blank=True, default='')),
                ('row_action', models.CharField(max_length=6, default='CREATE')),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('marriage', models.ForeignKey(related_name='details', to='people.Marriage')),
                ('person_1', models.ForeignKey(related_name='marriagedetail_p1_set', to='people.Person')),
                ('person_2', models.ForeignKey(related_name='marriagedetail_p2_set', to='people.Person')),
                ('row_user_insert', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.PROTECT, null=True)),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.AddField(
            model_name='persondetail',
            name='address',
            field=models.CharField(max_length=200, blank=True, default=''),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='baptism_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='baptism_location',
            field=models.CharField(max_length=200, blank=True, default=''),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='birth_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='birth_location',
            field=models.CharField(max_length=200, blank=True, default=''),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='death_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='death_location',
            field=models.CharField(max_length=200, blank=True, default=''),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='doc_id',
            field=models.CharField(max_length=50, blank=True, default=''),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='email',
            field=models.CharField(max_length=200, blank=True, default=''),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='gender',
            field=models.CharField(max_length=1, choices=[('M', 'Hombre'), ('F', 'Mujer')], default='M'),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='mbg_entry_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='mbg_exit_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='persondetail',
            name='obs',
            field=models.CharField(max_length=1000, blank=True, default=''),
        ),
    ]
