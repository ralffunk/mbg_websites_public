# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0009_auto_20160907_1930'),
    ]

    operations = [
        migrations.AddField(
            model_name='persondetail',
            name='married_last_name',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
    ]
