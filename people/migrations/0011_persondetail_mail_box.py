# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0010_persondetail_married_last_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='persondetail',
            name='mail_box',
            field=models.CharField(default='', blank=True, max_length=200),
        ),
    ]
