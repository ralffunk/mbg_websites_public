# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0006_auto_20160827_1038'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='groupdetail',
            name='leaders',
        ),
        migrations.RemoveField(
            model_name='persondetail',
            name='mbg_group',
        ),
        migrations.AddField(
            model_name='groupdetail',
            name='type',
            field=models.CharField(max_length=2, default='99', choices=[('01', 'Gemeindegruppe'), ('02', 'Hauskreis'), ('99', 'Otro')]),
        ),
    ]
