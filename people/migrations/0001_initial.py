# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('row_deleted', models.BooleanField(default=False)),
            ],
            options={
                'default_permissions': (),
                'permissions': (('list_people', 'ver lista de personas'), ('create_person', 'crear persona'), ('update_person', 'modificar persona'), ('delete_person', 'eliminar persona')),
            },
        ),
        migrations.CreateModel(
            name='PersonDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('first_name', models.CharField(error_messages={'blank': 'El nombre de la persona no puede quedar vacío.'}, default='', max_length=50)),
                ('last_name', models.CharField(error_messages={'blank': 'El apellido de la persona no puede quedar vacío.'}, default='', max_length=50)),
                ('phone', models.CharField(blank=True, default='', max_length=50)),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('person', models.ForeignKey(to='people.Person', related_name='details')),
            ],
            options={
                'default_permissions': (),
            },
        ),
    ]
