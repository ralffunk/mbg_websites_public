# -*- coding: utf-8 -*-
"""
Models.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import datetime
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.utils import timezone as tz


class Person(models.Model):
    """
    A person with version control.
    """
    row_deleted = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()
        permissions = (
            ('list_people', 'ver lista de personas'),
            ('create_person', 'crear persona'),
            ('update_person', 'modificar persona'),
            ('delete_person', 'eliminar persona'),
        )

    def __str__(self):
        det = self.current_details
        if det:
            return str(det)
        else:
            return 'Person(pk: {})'.format(self.pk)

    @property
    def current_details(self):
        return self.details.filter(row_time_end__isnull=True).order_by('-row_time_start').first()

    @property
    def current_groups(self):
        return self.groupmember_set.filter(row_time_end__isnull=True)

    @property
    def sorted_current_groups(self):
        return sorted(
            self.current_groups,
            key=lambda x: (x.group.current_details.type, x.group.current_details.name))

    def do_create(self, first_name, last_name, married_last_name, gender, father, mother,
                  birth_date, birth_location, death_date, death_location, baptism_date,
                  baptism_location, mbg_entry_date, mbg_exit_date, mbg_subscription_email,
                  mbg_subscription_print, doc_id, email, phone, address, mail_box, obs, user):
        new_det = PersonDetail()
        new_det.first_name = first_name
        new_det.last_name = last_name
        new_det.married_last_name = married_last_name
        new_det.gender = gender
        new_det.father = father
        new_det.mother = mother
        new_det.birth_date = birth_date
        new_det.birth_location = birth_location
        new_det.death_date = death_date
        new_det.death_location = death_location
        new_det.baptism_date = baptism_date
        new_det.baptism_location = baptism_location
        new_det.mbg_entry_date = mbg_entry_date
        new_det.mbg_exit_date = mbg_exit_date
        new_det.mbg_subscription_email = mbg_subscription_email
        new_det.mbg_subscription_print = mbg_subscription_print
        new_det.doc_id = doc_id
        new_det.email = email
        new_det.phone = phone
        new_det.address = address
        new_det.mail_box = mail_box
        new_det.obs = obs
        new_det.row_action = 'CREATE'
        new_det.row_user_insert = user
        new_det.full_clean(exclude=['person'])

        self.save()
        new_det.person = self
        new_det.save()

    def do_update(self, first_name, last_name, married_last_name, gender, father, mother,
                  birth_date, birth_location, death_date, death_location, baptism_date,
                  baptism_location, mbg_entry_date, mbg_exit_date, mbg_subscription_email,
                  mbg_subscription_print, doc_id, email, phone, address, mail_box, obs, user):
        new_det = PersonDetail()
        new_det.person = self
        new_det.first_name = first_name
        new_det.last_name = last_name
        new_det.married_last_name = married_last_name
        new_det.gender = gender
        new_det.father = father
        new_det.mother = mother
        new_det.birth_date = birth_date
        new_det.birth_location = birth_location
        new_det.death_date = death_date
        new_det.death_location = death_location
        new_det.baptism_date = baptism_date
        new_det.baptism_location = baptism_location
        new_det.mbg_entry_date = mbg_entry_date
        new_det.mbg_exit_date = mbg_exit_date
        new_det.mbg_subscription_email = mbg_subscription_email
        new_det.mbg_subscription_print = mbg_subscription_print
        new_det.doc_id = doc_id
        new_det.email = email
        new_det.phone = phone
        new_det.address = address
        new_det.mail_box = mail_box
        new_det.obs = obs
        new_det.row_action = 'UPDATE'
        new_det.row_user_insert = user
        new_det.full_clean()

        old_det = self.current_details
        if old_det:
            if not new_det.is_equal(old_det):
                old_det.row_time_end = tz.now()
                old_det.save()
                new_det.save()
        else:
            new_det.save()

    def do_delete(self, user):
        new_det = PersonDetail()
        new_det.person = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.first_name = old_det.first_name
            new_det.last_name = old_det.last_name
            new_det.married_last_name = old_det.married_last_name
            new_det.gender = old_det.gender
            new_det.father = old_det.father
            new_det.mother = old_det.mother
            new_det.birth_date = old_det.birth_date
            new_det.birth_location = old_det.birth_location
            new_det.death_date = old_det.death_date
            new_det.death_location = old_det.death_location
            new_det.baptism_date = old_det.baptism_date
            new_det.baptism_location = old_det.baptism_location
            new_det.mbg_entry_date = old_det.mbg_entry_date
            new_det.mbg_exit_date = old_det.mbg_exit_date
            new_det.mbg_subscription_email = old_det.mbg_subscription_email
            new_det.mbg_subscription_print = old_det.mbg_subscription_print
            new_det.doc_id = old_det.doc_id
            new_det.email = old_det.email
            new_det.phone = old_det.phone
            new_det.address = old_det.address
            new_det.mail_box = old_det.mail_box
            new_det.obs = old_det.obs

        new_det.row_action = 'DELETE'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = True
        self.save()

    def undo_delete(self, user):
        new_det = PersonDetail()
        new_det.person = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.first_name = old_det.first_name
            new_det.last_name = old_det.last_name
            new_det.married_last_name = old_det.married_last_name
            new_det.gender = old_det.gender
            new_det.father = old_det.father
            new_det.mother = old_det.mother
            new_det.birth_date = old_det.birth_date
            new_det.birth_location = old_det.birth_location
            new_det.death_date = old_det.death_date
            new_det.death_location = old_det.death_location
            new_det.baptism_date = old_det.baptism_date
            new_det.baptism_location = old_det.baptism_location
            new_det.mbg_entry_date = old_det.mbg_entry_date
            new_det.mbg_exit_date = old_det.mbg_exit_date
            new_det.mbg_subscription_email = old_det.mbg_subscription_email
            new_det.mbg_subscription_print = old_det.mbg_subscription_print
            new_det.doc_id = old_det.doc_id
            new_det.email = old_det.email
            new_det.phone = old_det.phone
            new_det.address = old_det.address
            new_det.mail_box = old_det.mail_box
            new_det.obs = old_det.obs

        new_det.row_action = 'UNDO_D'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = False
        self.save()


class PersonDetail(models.Model):
    """
    A person detail with version control.
    """
    GENDERS = (
        ('M', 'Hombre'),
        ('F', 'Mujer'),
    )
    person = models.ForeignKey(Person, related_name='details')
    first_name = models.CharField(max_length=50, default='', error_messages={
        'blank': 'El nombre de la persona no puede quedar vacío.'})
    last_name = models.CharField(max_length=50, default='', error_messages={
        'blank': 'El apellido de la persona no puede quedar vacío.'})
    married_last_name = models.CharField(max_length=50, blank=True, default='')
    gender = models.CharField(max_length=1, default='M', choices=GENDERS)
    father = models.ForeignKey(
        Person, related_name='father_set', null=True, blank=True, on_delete=models.SET_NULL)
    mother = models.ForeignKey(
        Person, related_name='mother_set', null=True, blank=True, on_delete=models.SET_NULL)
    birth_date = models.DateField(null=True, blank=True)
    birth_location = models.CharField(max_length=200, blank=True, default='')
    death_date = models.DateField(null=True, blank=True)
    death_location = models.CharField(max_length=200, blank=True, default='')
    baptism_date = models.DateField(null=True, blank=True)
    baptism_location = models.CharField(max_length=200, blank=True, default='')
    mbg_entry_date = models.DateField(null=True, blank=True)
    mbg_exit_date = models.DateField(null=True, blank=True)
    mbg_subscription_email = models.BooleanField(default=False)
    mbg_subscription_print = models.BooleanField(default=False)
    doc_id = models.CharField(max_length=50, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    phone = models.CharField(max_length=50, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    mail_box = models.CharField(max_length=200, blank=True, default='')
    obs = models.CharField(max_length=1000, blank=True, default='')
    row_action = models.CharField(max_length=6, default='CREATE')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return '{} {}'.format(self.first_name, self.complete_last_name)

    def is_equal(self, other):
        if (self.first_name == other.first_name
                and self.last_name == other.last_name
                and self.married_last_name == other.married_last_name
                and self.gender == other.gender
                and self.father == other.father
                and self.mother == other.mother
                and self.birth_date == other.birth_date
                and self.birth_location == other.birth_location
                and self.death_date == other.death_date
                and self.death_location == other.death_location
                and self.baptism_date == other.baptism_date
                and self.baptism_location == other.baptism_location
                and self.mbg_entry_date == other.mbg_entry_date
                and self.mbg_exit_date == other.mbg_exit_date
                and self.mbg_subscription_email == other.mbg_subscription_email
                and self.mbg_subscription_print == other.mbg_subscription_print
                and self.doc_id == other.doc_id
                and self.email == other.email
                and self.phone == other.phone
                and self.address == other.address
                and self.mail_box == other.mail_box
                and self.obs == other.obs):
            return True

        return False

    @property
    def complete_last_name(self):
        if self.married_last_name:
            return '{} ({})'.format(self.married_last_name, self.last_name)

        return self.last_name

    @property
    def age(self):
        if not self.birth_date:
            return 0

        if self.death_date:
            d = self.death_date
        else:
            d = datetime.date.today()

        age = d.year - self.birth_date.year
        if (d.month, d.day) < (self.birth_date.month, self.birth_date.day):
            age -= 1

        return age

    @property
    def death_delta(self):
        if not self.death_date:
            return 0

        d = datetime.date.today()
        age = d.year - self.death_date.year
        if (d.month, d.day) < (self.death_date.month, self.death_date.day):
            age -= 1

        return age

    @property
    def baptism_age(self):
        if not self.birth_date or not self.baptism_date:
            return 0

        age = self.baptism_date.year - self.birth_date.year
        if ((self.baptism_date.month, self.baptism_date.day)
                < (self.birth_date.month, self.birth_date.day)):
            age -= 1

        return age

    @property
    def mbg_entry_delta(self):
        if not self.mbg_entry_date:
            return 0

        d = datetime.date.today()
        age = d.year - self.mbg_entry_date.year
        if (d.month, d.day) < (self.mbg_entry_date.month, self.mbg_entry_date.day):
            age -= 1

        return age

    @property
    def mbg_exit_delta(self):
        if not self.mbg_exit_date:
            return 0

        d = datetime.date.today()
        age = d.year - self.mbg_exit_date.year
        if (d.month, d.day) < (self.mbg_exit_date.month, self.mbg_exit_date.day):
            age -= 1

        return age

    @property
    def is_member(self):
        return self.mbg_entry_date and not self.mbg_exit_date

    @property
    def children(self):
        qs = PersonDetail.objects.filter(person__row_deleted=False, row_time_end__isnull=True)
        qs = qs.exclude(person=self.person)
        qs = qs.filter(Q(Q(father=self.person) | Q(mother=self.person)))

        return [p for p in qs.order_by('birth_date')]


class Marriage(models.Model):
    """
    A marriage with version control.
    """
    row_deleted = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()
        permissions = (
            ('list_marriages', 'ver lista de matrimonios'),
            ('create_marriage', 'crear matrimonio'),
            ('update_marriage', 'modificar matrimonio'),
            ('delete_marriage', 'eliminar matrimonio'),
        )

    def __str__(self):
        det = self.current_details
        if det:
            return str(det)
        else:
            return 'Marriage(pk: {})'.format(self.pk)

    @property
    def current_details(self):
        return self.details.filter(row_time_end__isnull=True).order_by('-row_time_start').first()

    def do_create(self, person_1, person_2, location, start_date, end_date, obs, user):
        new_det = MarriageDetail()

        # try putting the MALE in person_1, but it will still work in exceptional cases
        if person_1.current_details.gender == 'M':
            new_det.person_1 = person_1
            new_det.person_2 = person_2
        else:
            new_det.person_1 = person_2
            new_det.person_2 = person_1

        new_det.location = location
        new_det.start_date = start_date
        new_det.end_date = end_date
        new_det.obs = obs
        new_det.row_action = 'CREATE'
        new_det.row_user_insert = user
        new_det.full_clean(exclude=['marriage'])

        self.save()
        new_det.marriage = self
        new_det.save()

    def do_update(self, person_1, person_2, location, start_date, end_date, obs, user):
        new_det = MarriageDetail()
        new_det.marriage = self

        # try putting the MALE in person_1, but it will still work in exceptional cases
        if person_1.current_details.gender == 'M':
            new_det.person_1 = person_1
            new_det.person_2 = person_2
        else:
            new_det.person_1 = person_2
            new_det.person_2 = person_1

        new_det.location = location
        new_det.start_date = start_date
        new_det.end_date = end_date
        new_det.obs = obs
        new_det.row_action = 'UPDATE'
        new_det.row_user_insert = user
        new_det.full_clean()

        old_det = self.current_details
        if old_det:
            if not new_det.is_equal(old_det):
                old_det.row_time_end = tz.now()
                old_det.save()
                new_det.save()
        else:
            new_det.save()

    def do_delete(self, user):
        new_det = MarriageDetail()
        new_det.marriage = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.person_1 = old_det.person_1
            new_det.person_2 = old_det.person_2
            new_det.location = old_det.location
            new_det.start_date = old_det.start_date
            new_det.end_date = old_det.end_date
            new_det.obs = old_det.obs

        new_det.row_action = 'DELETE'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = True
        self.save()

    def undo_delete(self, user):
        new_det = MarriageDetail()
        new_det.marriage = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.person_1 = old_det.person_1
            new_det.person_2 = old_det.person_2
            new_det.location = old_det.location
            new_det.start_date = old_det.start_date
            new_det.end_date = old_det.end_date
            new_det.obs = old_det.obs

        new_det.row_action = 'UNDO_D'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = False
        self.save()


class MarriageDetail(models.Model):
    """
    A marriage detail with version control.
    """
    marriage = models.ForeignKey(Marriage, related_name='details')
    person_1 = models.ForeignKey(Person, related_name='marriagedetail_p1_set')
    person_2 = models.ForeignKey(Person, related_name='marriagedetail_p2_set')
    location = models.CharField(max_length=200, blank=True, default='')
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    obs = models.CharField(max_length=1000, blank=True, default='')
    row_action = models.CharField(max_length=6, default='CREATE')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        det_1 = self.person_1.current_details
        det_2 = self.person_2.current_details
        return '{} & {} {}'.format(det_1.first_name, det_2.first_name, det_1.last_name.split()[0])

    def is_equal(self, other):
        if (self.person_1 == other.person_1
                and self.person_2 == other.person_2
                and self.location == other.location
                and self.start_date == other.start_date
                and self.end_date == other.end_date
                and self.obs == other.obs):
            return True

        return False

    @property
    def age(self):
        if not self.start_date:
            return 0

        if self.end_date:
            d = self.end_date
        else:
            d = datetime.date.today()

        age = d.year - self.start_date.year
        if (d.month, d.day) < (self.start_date.month, self.start_date.day):
            age -= 1

        return age

    @property
    def end_delta(self):
        if not self.end_date:
            return 0

        d = datetime.date.today()
        age = d.year - self.end_date.year
        if (d.month, d.day) < (self.end_date.month, self.end_date.day):
            age -= 1

        return age

    @property
    def children(self):
        qs = PersonDetail.objects.filter(person__row_deleted=False, row_time_end__isnull=True)
        qs = qs.exclude(Q(Q(person=self.person_1) | Q(person=self.person_2)))
        qs = qs.filter(Q(Q(father=self.person_1) | Q(mother=self.person_2)))

        children_list = []
        for p in qs.order_by('birth_date'):
            p.has_father = False
            if p.father == self.person_1:
                p.has_father = True

            p.has_mother = False
            if p.mother == self.person_2:
                p.has_mother = True

            children_list.append(p)

        return children_list


class Group(models.Model):
    """
    A group with version control.
    """
    row_deleted = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()
        permissions = (
            ('list_groups', 'ver lista de grupos'),
            ('create_group', 'crear grupo'),
            ('update_group', 'modificar grupo'),
            ('delete_group', 'eliminar grupo'),
        )

    def __str__(self):
        det = self.current_details
        if det:
            return str(det)
        else:
            return 'Group(pk: {})'.format(self.pk)

    @property
    def current_details(self):
        return self.details.filter(row_time_end__isnull=True).order_by('-row_time_start').first()

    @property
    def current_members(self):
        return self.groupmember_set.filter(row_time_end__isnull=True)

    @property
    def sorted_current_members(self):
        return sorted(
            self.current_members,
            key=lambda x: (x.person.current_details.last_name, x.person.current_details.first_name))

    @property
    def current_member_count(self):
        return self.current_members.count()

    def do_create(self, type_, name, obs, user):
        new_det = GroupDetail()
        new_det.type = type_
        new_det.name = name
        new_det.obs = obs
        new_det.row_action = 'CREATE'
        new_det.row_user_insert = user
        new_det.full_clean(exclude=['group'])

        self.save()
        new_det.group = self
        new_det.save()

    def do_update(self, type_, name, obs, user):
        new_det = GroupDetail()
        new_det.group = self
        new_det.type = type_
        new_det.name = name
        new_det.obs = obs
        new_det.row_action = 'UPDATE'
        new_det.row_user_insert = user
        new_det.full_clean()

        old_det = self.current_details
        if old_det:
            if not new_det.is_equal(old_det):
                old_det.row_time_end = tz.now()
                old_det.save()
                new_det.save()
        else:
            new_det.save()

    def do_delete(self, user):
        new_det = GroupDetail()
        new_det.group = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.type = old_det.type
            new_det.name = old_det.name
            new_det.obs = old_det.obs

        new_det.row_action = 'DELETE'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = True
        self.save()

    def undo_delete(self, user):
        new_det = GroupDetail()
        new_det.group = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.type = old_det.type
            new_det.name = old_det.name
            new_det.obs = old_det.obs

        new_det.row_action = 'UNDO_D'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = False
        self.save()


class GroupDetail(models.Model):
    """
    A group detail with version control.
    """
    GROUP_TYPES = (
        ('01', 'Gemeindegruppe'),
        ('02', 'Hauskreis'),
        ('99', 'Otro'),
    )
    group = models.ForeignKey(Group, related_name='details')
    type = models.CharField(max_length=2, default='99', choices=GROUP_TYPES)
    name = models.CharField(max_length=50, default='', error_messages={
        'blank': 'El nombre del grupo no puede quedar vacío.'})
    obs = models.CharField(max_length=1000, blank=True, default='')
    row_action = models.CharField(max_length=6, default='CREATE')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return '{}: {}'.format(self.get_type_display(), self.name)

    def is_equal(self, other):
        if (self.type == other.type
                and self.name == other.name
                and self.obs == other.obs):
            return True

        return False


class GroupMember(models.Model):
    """
    A group member.
    """
    group = models.ForeignKey(Group)
    person = models.ForeignKey(Person, on_delete=models.PROTECT)
    obs = models.CharField(max_length=1000, blank=True, default='')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_start = models.ForeignKey(
        User, null=True, blank=True, related_name='groupmember_start_set', on_delete=models.PROTECT)
    row_user_end = models.ForeignKey(
        User, null=True, blank=True, related_name='groupmember_end_set', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return '"{}" en "{}"'.format(self.person, self.group)
