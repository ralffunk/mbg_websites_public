# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import xlsxwriter
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone as tz
from io import BytesIO
from mbg_websites import utils
from people.models import Person, PersonDetail, Marriage, MarriageDetail


@login_required()
@permission_required('people.list_marriages', raise_exception=True)
def marriages_list(request):
    """
    List marriages.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de matrimonios', ),
    ]

    qs = MarriageDetail.objects.filter(marriage__row_deleted=False, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
    elif request.GET.get('sf_order', '') == '3':
        ctx['sf_order'] = '3'
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('start_date')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    marriage_list = []
    for e in qs:
        e.url = reverse('people:marriages-read', args=[e.marriage.pk])
        det_1 = e.person_1.current_details
        det_2 = e.person_2.current_details
        e.person_1_str = det_1.first_name
        e.person_2_str = det_2.first_name
        e.last_name = det_1.last_name.split()[0]

        if (not ctx['sf_text']
                or ctx['sf_text'].upper() in e.person_1_str.upper()
                or ctx['sf_text'].upper() in e.person_2_str.upper()
                or ctx['sf_text'].upper() in e.last_name.upper()):
            marriage_list.append(e)

    if ctx['sf_order'] == '1':
        marriage_list = sorted(
            marriage_list,
            key=lambda x: x.person_1_str)
    elif ctx['sf_order'] == '2':
        marriage_list = sorted(
            marriage_list,
            key=lambda x: x.person_2_str)
    elif ctx['sf_order'] == '3':
        marriage_list = sorted(
            marriage_list,
            key=lambda x: x.last_name)
    else:
        pass

    paginator = Paginator(marriage_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'people/marriage_list.html', ctx)


@login_required()
@permission_required('people.list_marriages', raise_exception=True)
def marriages_list_excel(request):
    """
    Download marriages list as EXCEL.
    :param request: HttpRequest
    :return: HttpResponse
    """
    filename = 'lista-de-matrimonios'
    column_title_list = (
        'ID interno',
        'Hombre',
        'Mujer',
        'Lugar de casamiento',
        'Fecha de casamiento',
        'Fecha de fín',
        'Observación',
    )

    temp_buffer = BytesIO()
    wb = xlsxwriter.Workbook(temp_buffer, options={'in_memory': True})
    ws = wb.add_worksheet('list')
    date_format = wb.add_format({'num_format': 'dd-mm-yyyy'})

    row = 0
    ws.write(row, 0, 'Lista de matrimonios')
    row += 1
    ws.write(row, 0, 'Archivo descargado por "{}" el {} a las {}'.format(
        request.user.profile, *tz.localtime(tz.now()).strftime('%Y-%m-%d %H:%M').split()))
    row += 1
    row += 1

    for j, col_title in enumerate(column_title_list):
        ws.write(row, j, col_title)
    row += 1

    for p in MarriageDetail.objects.filter(
            marriage__row_deleted=False, row_time_end__isnull=True).order_by('start_date'):
        ws.write(row, 0, p.marriage.pk)
        ws.write(row, 1, str(p.person_1))
        ws.write(row, 2, str(p.person_2))
        ws.write(row, 3, p.location)
        ws.write(row, 4, p.start_date, date_format)
        ws.write(row, 5, p.end_date, date_format)
        ws.write(row, 6, p.obs)
        row += 1

    wb.close()
    return utils.make_excel(temp_buffer, filename)


@login_required()
@permission_required('people.delete_marriage', raise_exception=True)
def marriages_list_deleted(request):
    """
    List deleted marriages.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Matrimonios', reverse('people:marriages-list')),
        ('Matrimonios eliminados', ),
    ]

    qs = MarriageDetail.objects.filter(marriage__row_deleted=True, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('start_date')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    marriage_list = []
    for e in qs:
        e.url = reverse('people:marriages-read', args=[e.marriage.pk])
        det_1 = e.person_1.current_details
        det_2 = e.person_2.current_details
        e.person_1_str = det_1.first_name
        e.person_2_str = det_2.first_name
        e.last_name = det_1.last_name

        if (not ctx['sf_text']
                or ctx['sf_text'].upper() in e.person_1_str.upper()
                or ctx['sf_text'].upper() in e.person_2_str.upper()
                or ctx['sf_text'].upper() in e.last_name.upper()):
            marriage_list.append(e)

    paginator = Paginator(marriage_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'people/marriage_deleted_list.html', ctx)


@login_required()
@permission_required('people.create_marriage', raise_exception=True)
def marriages_create(request):
    """
    Create a marriage.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear nuevo matrimonio')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Matrimonios', reverse('people:marriages-list')),
        ('Crear nuevo matrimonio', ),
    ]
    ctx['CRUD'] = 'C'
    ctx['marriage'] = Marriage()
    return _marriages_create_or_update(request, ctx)


@login_required()
@permission_required('people.list_marriages', raise_exception=True)
def marriages_read(request, marriage_id):
    """
    Read a marriage.
    :param request: HttpRequest
    :param marriage_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles del matrimonio')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Matrimonios', reverse('people:marriages-list')),
        ('Matrimonio {}'.format(marriage_id), ),
    ]
    ctx['CRUD'] = 'R'
    ctx['marriage'] = get_object_or_404(Marriage, pk=marriage_id)
    ctx['current_details'] = ctx['marriage'].current_details
    ctx['history_list'] = ctx['marriage'].details.order_by('-row_time_start')[:5]

    if ctx['marriage'].row_deleted:
        messages.error(request, 'Este matrimonio se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Matrimonios eliminados', reverse('people:marriages-list-deleted')))

    if ctx['current_details'].start_date and not ctx['current_details'].end_date:
        if ctx['current_details'].person_1.current_details.death_date:
            messages.error(request, 'El hombre falleció pero el matrimonio aún está activo.')
        if ctx['current_details'].person_2.current_details.death_date:
            messages.error(request, 'La mujer falleció pero el matrimonio aún está activo.')

    return render(request, 'people/marriage_form.html', ctx)


@login_required()
@permission_required('people.update_marriage', raise_exception=True)
def marriages_update(request, marriage_id):
    """
    Update a marriage.
    :param request: HttpRequest
    :param marriage_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar matrimonio')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Matrimonios', reverse('people:marriages-list')),
        ('Matrimonio {}'.format(marriage_id), reverse('people:marriages-read', args=[marriage_id])),
        ('Modificar matrimonio', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['marriage'] = get_object_or_404(Marriage, pk=marriage_id, row_deleted=False)
    ctx['current_details'] = ctx['marriage'].current_details
    return _marriages_create_or_update(request, ctx)


@login_required()
@permission_required('people.delete_marriage', raise_exception=True)
def marriages_delete(request, marriage_id):
    """
    Delete a marriage.
    :param request: HttpRequest
    :param marriage_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar matrimonio')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Matrimonios', reverse('people:marriages-list')),
        ('Matrimonio {}'.format(marriage_id), reverse('people:marriages-read', args=[marriage_id])),
        ('Eliminar matrimonio', ),
    ]
    ctx['CRUD'] = 'D'
    ctx['marriage'] = get_object_or_404(Marriage, pk=marriage_id, row_deleted=False)
    ctx['current_details'] = ctx['marriage'].current_details

    if request.method == 'POST':
        ctx['marriage'].do_delete(request.user)
        return redirect('people:marriages-list')
    else:
        return render(request, 'people/marriage_form.html', ctx)


@login_required()
@permission_required('people.delete_marriage', raise_exception=True)
def marriages_delete_undo(request, marriage_id):
    """
    Undo the delete of a marriage.
    :param request: HttpRequest
    :param marriage_id:
    :return: HttpResponse
    """
    marriage = get_object_or_404(Marriage, pk=marriage_id)

    if request.method == 'POST':
        if marriage.row_deleted:
            marriage.undo_delete(request.user)

    return redirect('people:marriages-read', marriage.pk)


@login_required()
@permission_required('people.list_marriages', raise_exception=True)
def marriages_history(request, marriage_id):
    """
    View changes history of a marriage.
    :param request: HttpRequest
    :param marriage_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Matrimonios', reverse('people:marriages-list')),
        ('Matrimonio {}'.format(marriage_id), reverse('people:marriages-read', args=[marriage_id])),
        ('Historial del matrimonio', ),
    ]
    ctx['marriage'] = get_object_or_404(Marriage, pk=marriage_id)
    qs = ctx['marriage'].details

    ctx['sf_text'] = request.GET.get('sf_text', '')

    ctx.update(utils.get_sf_dates(request.GET))
    qs = qs.filter(row_time_start__range=(ctx['sf_date_start'], ctx['sf_date_end']))

    ctx['sf_order'] = '0'
    qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    marriage_list = []
    for e in qs:
        e.person_1_str = str(e.person_1)
        e.person_2_str = str(e.person_2)

        if (not ctx['sf_text']
                or ctx['sf_text'].upper() in e.row_action.upper()
                or ctx['sf_text'].upper() in e.person_1_str.upper()
                or ctx['sf_text'].upper() in e.person_2_str.upper()
                or ctx['sf_text'].upper() in e.location.upper()
                or ctx['sf_text'].upper() in e.obs.upper()):
            marriage_list.append(e)

    paginator = Paginator(marriage_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['marriage'].row_deleted:
        messages.error(request, 'Este matrimonio se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Matrimonios eliminados', reverse('people:marriages-list-deleted')))

    return render(request, 'people/marriage_history_list.html', ctx)


def _marriages_create_or_update(request, ctx):
    """
    Create or update a marriage.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    ctx['male_person_list'] = sorted(
        [(e.person.pk, str(e)) for e in PersonDetail.objects.filter(
            person__row_deleted=False, row_time_end__isnull=True, gender='M')],
        key=lambda x: x[1])
    ctx['female_person_list'] = sorted(
        [(e.person.pk, str(e)) for e in PersonDetail.objects.filter(
            person__row_deleted=False, row_time_end__isnull=True, gender='F')],
        key=lambda x: x[1])

    if request.method == 'POST':
        new_data = dict()

        new_data['person_1'] = None
        if request.POST.get('person_1', ''):
            try:
                new_data['person_1'] = Person.objects.get(
                    pk=int(request.POST.get('person_1')), row_deleted=False)
            except (ValueError, Person.DoesNotExist):
                messages.error(request, 'El hombre seleccionado no es una persona válida.')
                return render(request, 'people/marriage_form.html', ctx)

        new_data['person_2'] = None
        if request.POST.get('person_2', ''):
            try:
                new_data['person_2'] = Person.objects.get(
                    pk=int(request.POST.get('person_2')), row_deleted=False)
            except (ValueError, Person.DoesNotExist):
                messages.error(request, 'La mujer seleccionada no es una persona válida.')
                return render(request, 'people/marriage_form.html', ctx)

        if new_data['person_1'] == new_data['person_2']:
            messages.error(request, 'Tienen que ser dos personas diferentes.')
            return render(request, 'people/marriage_form.html', ctx)

        new_data['location'] = request.POST.get('location', '')

        new_data['start_date'] = None
        if request.POST.get('start_date', ''):
            try:
                new_data['start_date'] = utils.parse_date(
                    request.POST.get('start_date'), raise_error=True).date()
            except ValueError:
                messages.error(request, 'El formato de la fecha de casamiento no es válido.')
                return render(request, 'people/marriage_form.html', ctx)

        new_data['end_date'] = None
        if request.POST.get('end_date', ''):
            try:
                new_data['end_date'] = utils.parse_date(
                    request.POST.get('end_date'), raise_error=True).date()
            except ValueError:
                messages.error(request, 'El formato de la fecha de fin no es válido.')
                return render(request, 'people/marriage_form.html', ctx)

        if (new_data['start_date'] and new_data['end_date']
                and new_data['start_date'] > new_data['end_date']):
            messages.error(request, 'La fecha de casamiento debe ser anterior al fin.')
            return render(request, 'people/marriage_form.html', ctx)

        new_data['obs'] = request.POST.get('obs', '')

        try:
            if ctx['CRUD'] == 'C':
                ctx['marriage'].do_create(user=request.user, **new_data)
            else:
                ctx['marriage'].do_update(user=request.user, **new_data)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'people/marriage_form.html', ctx)

        return redirect('people:marriages-read', ctx['marriage'].pk)
    else:
        return render(request, 'people/marriage_form.html', ctx)
