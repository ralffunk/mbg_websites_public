# -*- coding: utf-8 -*-
"""
Run worker that processes queue.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
import sys
from rq import Worker, Queue, Connection


if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mbg_websites.settings')

    import django
    django.setup()

    burst_mode = True
    if len(sys.argv) > 1:
        if sys.argv[1] == '--no-burst-mode':
            burst_mode = False

    from django.conf import settings
    if settings.REDIS_CONNECTION:
        with Connection(settings.REDIS_CONNECTION):
            worker = Worker(map(Queue, ['high', 'default', 'low']))
            worker.work(burst=burst_mode)
