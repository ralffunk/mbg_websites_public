# -*- coding: utf-8 -*-
"""
Django settings for mbg_websites project.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/
For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
For production settings, see
https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(             # base/
    os.path.dirname(                    # base/mbg_websites/
        os.path.abspath(__file__)))     # base/mbg_websites/settings.py


WEBSITE_INFO = {
    'title': 'MBG Websites',
    'version': 'v17.12',
    'author': 'Ralf Funk',
    'login_text': [
        'Para acceder a ciertas partes de este sistema debes iniciar sesión usando una de estas '
        'redes sociales.',
        'Estos botones te llevarán a la página de la red social seleccionada, donde deberás '
        'iniciar sesión. Así ese sitio nos ayudará a verificar tu identidad y te podremos dar '
        'acceso a nuestro sistema.',
        'En ningún momento la red social nos permite obtener tu contraseña, y tampoco publicamos o '
        'usamos tu información para otros fines que verificar tu identidad.',
    ],
}
ROOT_URLCONF = 'mbg_websites.urls'
WSGI_APPLICATION = 'mbg_websites.wsgi.application'
ADMIN_USERNAME = 'theadmin'


INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'social.apps.django_app.default',
    'mailer',
    'guardian',
    'django_redis',
    'accounts',
    'people',
    'planners',
    'songs',
]
MIDDLEWARE_CLASSES = [
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'social.apps.django_app.middleware.SocialAuthExceptionMiddleware',
    'accounts.middleware.PersistentAnonymousSessionsMiddleware',
]
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'mbg_websites.context_processors.website_info',
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
            ],
        },
    },
]


# Sessions
# https://docs.djangoproject.com/en/1.8/topics/http/sessions/
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
SESSION_COOKIE_AGE = 24*60*60
SESSION_SAVE_EVERY_REQUEST = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'mediafiles')


# messages
from django.contrib.messages import constants as messages
MESSAGE_TAGS = {
    # map django messages with ERROR level to bootstrap's alert-danger
    messages.ERROR: 'danger',
    # the other levels match perfectly
    # INFO -> alert-info
    # SUCCESS -> alert-success
    # WARNING -> alert-warning
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/
LANGUAGE_CODE = 'es-PY'
TIME_ZONE = 'America/Asuncion'
USE_L10N = True
USE_TZ = True


# Security settings suggested when running 'manage.py check --deploy'
# https://docs.djangoproject.com/en/1.8/ref/middleware/
SECURE_HSTS_SECONDS = 3600      # set for more time ???
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_SSL_REDIRECT = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True
X_FRAME_OPTIONS = 'DENY'
ALLOWED_HOSTS = ['*']


# Configurations for 'python-social-auth'
# http://psa.matiasaguirre.net/docs/configuration/settings.html
# http://psa.matiasaguirre.net/docs/configuration/django.html
# http://psa.matiasaguirre.net/docs/backends/facebook.html
# http://psa.matiasaguirre.net/docs/backends/google.html
AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
)
SOCIAL_AUTH_URL_NAMESPACE = 'social'
SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'social.pipeline.user.create_user',
    'accounts.pipelines.create_profile',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
    'accounts.pipelines.log_login',
)
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/accounts/profile/new/'
SOCIAL_AUTH_LOGIN_ERROR_URL = '/accounts/login/error/'
SOCIAL_AUTH_INACTIVE_USER_URL = '/accounts/login/error/inactive/'


# Django-Guardian for object permission management
# https://docs.djangoproject.com/en/1.8/ref/settings/#std:setting-AUTHENTICATION_BACKENDS
# http://django-guardian.readthedocs.org/en/stable/configuration.html
ANONYMOUS_USER_NAME = None
GUARDIAN_RAISE_403 = True


# ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# The following settings are different between development and production
# environments or contain sensitive data to be kept outside of the VCS
# ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####


SECRET_KEY = os.environ.get('SECRET_KEY', 'd@vm=9+gjz73rrb*!l14an1*8s*5m!p6rk)#by=&u^jvbkda7l')
SOCIAL_AUTH_FACEBOOK_KEY = os.environ.get('SOCIAL_AUTH_FACEBOOK_KEY')
SOCIAL_AUTH_FACEBOOK_SECRET = os.environ.get('SOCIAL_AUTH_FACEBOOK_SECRET')
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.environ.get('SOCIAL_AUTH_GOOGLE_OAUTH2_KEY')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.environ.get('SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET')
GOOGLE_SERVICE_ACCOUNT_KEY = os.environ.get('GOOGLE_SERVICE_ACCOUNT_KEY')


if 'PROD_ENV' not in os.environ:
    DEBUG = True
    # to use without HTTPS
    SECURE_HSTS_SECONDS = 0
    SECURE_SSL_REDIRECT = False
    SECURE_PROXY_SSL_HEADER = None
    SESSION_COOKIE_SECURE = False
    CSRF_COOKIE_SECURE = False


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'private_data', 'db.sqlite3'),
    }
}
if 'PROD_ENV' in os.environ:
    import dj_database_url
    DATABASES['default'] = dj_database_url.config()     # get data from os.environ
    DATABASES['default']['CONN_MAX_AGE'] = 500


# Email
# https://docs.djangoproject.com/en/1.8/topics/email/
# https://docs.djangoproject.com/en/1.8/ref/settings/#email
ADMINS = (
    ('Ralf Funk', 'ralffunk0@gmail.com'),
)
SERVER_EMAIL = 'mbg.websites@gmail.com'
DEFAULT_FROM_EMAIL = SERVER_EMAIL
EMAIL_SUBJECT_PREFIX = '[MBG-WEBSITES] '
EMAIL_BACKEND = 'mailer.backend.DbBackend'
if 'PROD_ENV' in os.environ:
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_USE_TLS = True
    EMAIL_PORT = 587
    EMAIL_HOST_USER = SERVER_EMAIL
    EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', '')
else:
    MAILER_EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# Redis
# https://devcenter.heroku.com/articles/python-rq
REDIS_CONNECTION = None
if 'PROD_ENV' in os.environ:
    import redis
    REDIS_CONNECTION = redis.from_url(os.environ.get('REDISTOGO_URL', ''))


# Cache
# https://docs.djangoproject.com/en/1.8/topics/cache/
# http://niwinz.github.io/django-redis/latest/
CACHE_MIDDLEWARE_SECONDS = 1    # error: the pages don't refresh after an update of a model
if 'PROD_ENV' in os.environ:
    CACHES = {
        'default': {
            'BACKEND': 'django_redis.cache.RedisCache',
            'LOCATION': os.environ.get('REDISTOGO_URL', ''),
            'OPTIONS': {
                'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            }
        }
    }
