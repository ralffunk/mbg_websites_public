# -*- coding: utf-8 -*-
"""
Custom context processors for templates.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.conf import settings


def website_info(request):
    """
    Returns context variables with info about the website.
    """
    return {
        'website_info': settings.WEBSITE_INFO,
        'admin_username': settings.ADMIN_USERNAME,
    }
