# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render


def start_page(request):
    """
    Start page.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    return render(request, 'index.html', ctx)


def about_page(request):
    """
    About page.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Acerca de', ),
    ]
    return render(request, 'about.html', ctx)


def help_page(request):
    """
    Help page.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Ayuda', ),
    ]
    return render(request, 'help.html', ctx)


def certificate_challenge(request):
    """
    Certificate challenge.
    :param request: HttpRequest
    :return: HttpResponse
    """
    return HttpResponse('.')
