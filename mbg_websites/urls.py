# -*- coding: utf-8 -*-
"""
mbg_websites URL Configuration.

The 'urlpatterns' list routes URLs to views. For more information please see:
https://docs.djangoproject.com/en/1.8/topics/http/urls/
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.conf.urls import url, include
from mbg_websites import views


urlpatterns = [
    url(r'^$',
        views.start_page,
        name='start-page'),
    url(r'^about/$',
        views.about_page,
        name='about-page'),
    url(r'^help/$',
        views.help_page,
        name='help-page'),
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^people/', include('people.urls', namespace='people')),
    url(r'^planners/', include('planners.urls', namespace='planners')),
    url(r'^songs/', include('songs.urls', namespace='songs')),
    url('', include('social.apps.django_app.urls', namespace='social')),
    # url(r'^.well-known/acme-challenge/.../$',
    #     views.certificate_challenge),
]
