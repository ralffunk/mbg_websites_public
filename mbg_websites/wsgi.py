# -*- coding: utf-8 -*-
"""
WSGI config for mbg_websites project.

It exposes the WSGI callable as a module-level variable named 'application'.
See also: https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
See also: https://pypi.python.org/pypi/whitenoise
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mbg_websites.settings')

from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise
application = DjangoWhiteNoise(get_wsgi_application())
