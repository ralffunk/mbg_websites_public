# -*- coding: utf-8 -*-
"""
Helper classes and definitions.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import collections
import datetime
import json
from io import BytesIO
from django.conf import settings
from django.http import HttpResponse
from django.utils import timezone as tz
from oauth2client.service_account import ServiceAccountCredentials
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER, TA_RIGHT
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import mm
from reportlab.platypus import Paragraph, SimpleDocTemplate


month_dict = collections.OrderedDict(
    (
        (1, 'enero'),
        (2, 'febrero'),
        (3, 'marzo'),
        (4, 'abril'),
        (5, 'mayo'),
        (6, 'junio'),
        (7, 'julio'),
        (8, 'agosto'),
        (9, 'setiembre'),
        (10, 'octubre'),
        (11, 'noviembre'),
        (12, 'diciembre'),
    )
)
pdf_module_variables = {
    'pdf_title': '(not defined)',
}


def parse_date(date_str, format_str='%d-%m-%Y', default_date=None, raise_error=False):
    """
    Parse date from string and return a datetime object with timezone.
    :param date_str:
    :param format_str:
    :param default_date:
    :param raise_error:
    :return:
    """
    if isinstance(date_str, str):
        try:
            dt = datetime.datetime.strptime(date_str, format_str)

            if dt:
                t = tz.localtime(tz.now())
                dt = dt.replace(tzinfo=t.tzinfo)

                if dt:
                    return dt
        except ValueError as err:
            if raise_error:
                raise err

    return default_date


def get_sf_dates(get_dict):
    """
    Extract date parameters.
    :param get_dict:
    :return:
    """
    t = tz.localtime(tz.now())
    t = t.replace(hour=0, minute=0, second=0, microsecond=0)

    d = dict()
    d['sf_date_start'] = parse_date(
        get_dict.get('sf_date_start', ''),
        default_date=t.replace(month=1, day=1))
    d['sf_date_end'] = parse_date(
        get_dict.get('sf_date_end', ''),
        default_date=t.replace(month=12, day=31))

    # to include all times of the end day
    d['sf_date_end'] += datetime.timedelta(days=1) - datetime.timedelta(seconds=1)

    return d


def get_google_credentials():
    """
    Get Google service account credentials.
    :return:
    """
    if not settings.GOOGLE_SERVICE_ACCOUNT_KEY:
        return None

    credentials = ServiceAccountCredentials.from_json_keyfile_dict(
        json.loads(settings.GOOGLE_SERVICE_ACCOUNT_KEY),
        scopes=['https://www.googleapis.com/auth/calendar'])

    return credentials


def make_pdf(user, pdf_title, story, as_portrait=True):
    """
    Renders a PDF.
    :param user:
    :param pdf_title:
    :param story:
    :param as_portrait: (optional)
    :return:
    """
    # we use this dict so that data is accessible in header and footer functions
    pdf_module_variables['username'] = str(user.profile)
    pdf_module_variables['pdf_title'] = pdf_title

    # create PDF from list of flowables, using a temp buffer to assemble PDF
    margins = {
        'leftMargin': 15*mm,
        'rightMargin': 15*mm,
        'topMargin': 15*mm,
        'bottomMargin': 15*mm,
    }

    # page orientation
    page_size = A4
    if not as_portrait:
        page_size = landscape(A4)

    temp_buffer = BytesIO()
    doc = SimpleDocTemplate(temp_buffer, pagesize=page_size, **margins)
    doc.build(story, onFirstPage=_pdf_first_page, onLaterPages=_pdf_later_pages)

    # when download name is not ascii, an error happens with Gunicorn server,
    # but not with the Django development server. So we take non-ascii characters out
    name = ''.join(e for e in pdf_title.replace(' ', '_') if ord(e) < 128)

    # write temp buffer to response
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename="{}.pdf"'.format(name)
    response.write(temp_buffer.getvalue())
    temp_buffer.close()
    return response


def _pdf_first_page(my_c, doc):
    """
    Adding title, header and footer to first page
    :param my_c: Canvas
    :param doc: SimpleDocTemplate
    :return: None
    """
    if pdf_module_variables['pdf_title']:
        my_c.saveState()
        title = Paragraph(
            pdf_module_variables['pdf_title'],
            ParagraphStyle('Normal', fontSize=16, alignment=TA_CENTER))
        title.wrap(doc.width, 10*mm)
        title.drawOn(my_c, doc.leftMargin, doc.height + doc.bottomMargin - 5*mm)
        my_c.restoreState()

    _pdf_header_and_footer(my_c, doc)


def _pdf_later_pages(my_c, doc):
    """
    Adding header and footer to every page other than the first page
    :param my_c: Canvas
    :param doc: SimpleDocTemplate
    :return: None
    """
    _pdf_header_and_footer(my_c, doc)


def _pdf_header_and_footer(my_c, doc):
    """
    Auxiliary function that adds header and footer to pages
    :param my_c: Canvas
    :param doc: SimpleDocTemplate
    :return: None
    """
    my_c.saveState()

    # Header
    header1 = Paragraph(
        'Usuario: {}'.format(pdf_module_variables['username']),
        ParagraphStyle('Normal', textColor=colors.gray))
    header1.wrap(doc.width, doc.topMargin)
    # header1.drawOn(my_c, doc.leftMargin, doc.height + doc.bottomMargin)

    header2 = Paragraph(
        '{}'.format(tz.localtime(tz.now()).strftime('%d-%m-%Y - %H:%M')),
        ParagraphStyle('Normal', textColor=colors.gray, alignment=TA_RIGHT))
    header2.wrap(doc.width, doc.topMargin)
    header2.drawOn(my_c, doc.leftMargin, doc.height + doc.bottomMargin)

    # Footer
    footer = Paragraph(
        'Página {}'.format(doc.page),
        ParagraphStyle('Normal', textColor=colors.gray, alignment=TA_CENTER))
    footer.wrap(doc.width, doc.bottomMargin)
    footer.drawOn(my_c, doc.leftMargin, doc.bottomMargin - 3*mm)

    my_c.restoreState()


def make_excel(temp_buffer, filename):
    """
    Prepares an EXCEL file for download.
    :param temp_buffer: io.BytesIO with the workbook
    :param filename:
    :return:
    """
    # when download name is not ascii, an error happens with Gunicorn server,
    # but not with the Django development server. So we take non-ascii characters out
    name = ''.join(e for e in filename.replace(' ', '_') if ord(e) < 128)

    # write temp buffer to response
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.'
                                         'spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="{}.xlsx"'.format(name)
    response.write(temp_buffer.getvalue())
    temp_buffer.close()
    return response
