(function () {  // start of closure
    $(document).ready(function () {
        // reset content before displaying modal
        $('#m_loginModal').on('show.bs.modal', function (event) {
            $('#m_collapseMore').collapse('hide');
        });
    });
})();   // end of closure
