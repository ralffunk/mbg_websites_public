(function () {  // start of closure
    $(document).ready(function () {
        // create datepicker
        var opt = {
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            todayBtn: 'linked',
            weekStart: 1,
            language: 'es',
            orientation: 'top left'
        };
        $('#m_sf_date_start').datepicker(opt);
        $('#m_sf_date_end').datepicker(opt);

        // set focus after displaying modal
        $('#m_sf').on('shown.bs.modal', function () {
            $('#m_sf_text').focus();
        });

        // go to url on table row click
        $('.clickable-row').click(function () {
            window.document.location = $(this).data('href');
        });
    });
})();   // end of closure
