# mbg_websites_public
MBG Concordia platform for different web services

---

**Author:** Ralf Funk (ralffunk0@gmail.com)

**Project start date:** december 2015

**Latest version:** see variable WEBSITE_INFO in the [settings file](/mbg_websites/settings.py)
