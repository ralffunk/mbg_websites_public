# -*- coding: utf-8 -*-
"""
Creation of PPTX files.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from pptx import Presentation
from pptx.dml.color import RGBColor
from pptx.enum.text import PP_PARAGRAPH_ALIGNMENT, MSO_AUTO_SIZE, MSO_VERTICAL_ANCHOR
from pptx.util import Pt, Cm


class LyricsPresentation(Presentation):
    """
    A PPTX of song lyrics with dual-language support.
    """

    def __init__(
            self,
            base_file_name=None,
            color_language_1=(255, 255, 255),
            color_language_2=(255, 204, 0),
            color_songbook=(255, 255, 255),
            color_copyright=(255, 255, 255),
            color_slide_name=(255, 255, 255),
            font_name='Calibri',
            font_size_title_1=44,
            font_size_title_2=44,
            font_size_songbook=24,
            font_size_copyright=14,
            font_size_slide_name=14,
            font_size_language_1=24,
            font_size_language_2=24):
        super().__init__(base_file_name)
        self._color_language_1 = RGBColor(*color_language_1)
        self._color_language_2 = RGBColor(*color_language_2)
        self._color_songbook = RGBColor(*color_songbook)
        self._color_copyright = RGBColor(*color_copyright)
        self._color_slide_name = RGBColor(*color_slide_name)
        self._font_name = font_name
        self._font_size_title_1 = Pt(font_size_title_1)
        self._font_size_title_2 = Pt(font_size_title_2)
        self._font_size_songbook = Pt(font_size_songbook)
        self._font_size_copyright = Pt(font_size_copyright)
        self._font_size_slide_name = Pt(font_size_slide_name)
        self._font_size_language_1 = Pt(font_size_language_1)
        self._font_size_language_2 = Pt(font_size_language_2)

        # get index for blank slide layout
        self._blank_layout_index = [len(s.shapes) for s in self.slide_layouts].index(0)

        # calculate position of text boxes
        # position (0,0) is the top left corner of the slide
        x = Cm(1)
        w = self.slide_width - 2 * x
        h = (self.slide_height - 2 * Cm(1) - Cm(1)) / 2
        self._pos = {
            'content_1': {
                'x': x,
                'y': Cm(1),
                'w': w,
                'h': h,
            },
            'content_2': {
                'x': x,
                'y': self.slide_height / 2 + Cm(0.5),
                'w': w,
                'h': h,
            },
            'songbook': {       # bottom left
                'x': Cm(0.5),
                'y': self.slide_height - Cm(1.5),
                'w': w / 2,
                'h': Cm(1),
            },
            'copyright': {      # bottom right
                'x': self.slide_width / 2 + Cm(0.5),
                'y': self.slide_height - Cm(1.5),
                'w': w / 2,
                'h': Cm(1),
            },
            'slide_name': {     # top left
                'x': Cm(0.5),
                'y': Cm(0.5),
                'w': Cm(3),
                'h': Cm(1),
            },
        }

    def add_title_slide(self, title_1=None, title_2=None, songbook_text=None, copyright_text=None):
        # base PPTX should already have an empty slide
        slide = self.slides[0]

        if songbook_text:
            self._add_songbook(slide, songbook_text)

        if copyright_text:
            self._add_copyright(slide, copyright_text)

        # change height of content in slides for ONE LANGUAGE ONLY
        if not title_2:
            self._pos['content_1']['h'] = self.slide_height - 2 * self._pos['content_1']['y']

        if title_1:
            tf = self._add_textbox(slide, **self._pos['content_1'])
            p = tf.paragraphs[0]
            p.alignment = PP_PARAGRAPH_ALIGNMENT.CENTER
            p.font.name = self._font_name
            p.font.size = self._font_size_title_1
            p.font.color.rgb = self._color_language_1
            p.text = title_1

        if title_2:
            tf = self._add_textbox(slide, **self._pos['content_2'])
            tf.vertical_anchor = MSO_VERTICAL_ANCHOR.TOP
            p = tf.paragraphs[0]
            p.alignment = PP_PARAGRAPH_ALIGNMENT.CENTER
            p.font.name = self._font_name
            p.font.size = self._font_size_title_2
            p.font.color.rgb = self._color_language_2
            p.text = title_2

    def add_content_slide(self, slide_name=None, lines_1=None, lines_2=None, songbook_text=None,
                          copyright_text=None):
        slide = self.slides.add_slide(self.slide_layouts[self._blank_layout_index])

        if songbook_text:
            self._add_songbook(slide, songbook_text)

        if copyright_text:
            self._add_copyright(slide, copyright_text)

        if slide_name:
            self._add_slide_name(slide, slide_name)

        if lines_1:
            tf = self._add_textbox(slide, **self._pos['content_1'])
            self._add_lines(tf, lines_1, self._color_language_1, self._font_size_language_1)

        if lines_2:
            tf = self._add_textbox(slide,  **self._pos['content_2'])
            self._add_lines(tf, lines_2, self._color_language_2, self._font_size_language_2)

    def _add_lines(self, text_frame, lines, font_color, font_size):
        for i, line in enumerate(lines):
            if i == 0:
                p = text_frame.paragraphs[0]
            else:
                p = text_frame.add_paragraph()
            p.alignment = PP_PARAGRAPH_ALIGNMENT.CENTER
            p.font.name = self._font_name
            p.font.size = font_size
            p.font.color.rgb = font_color
            p.text = line

    def _add_songbook(self, slide, songbook_text):
        tf = self._add_textbox(slide, **self._pos['songbook'])
        tf.vertical_anchor = MSO_VERTICAL_ANCHOR.BOTTOM
        p = tf.paragraphs[0]
        p.alignment = PP_PARAGRAPH_ALIGNMENT.LEFT
        p.font.name = self._font_name
        p.font.size = self._font_size_songbook
        p.font.color.rgb = self._color_songbook
        p.text = songbook_text

    def _add_copyright(self, slide, copyright_text):
        tf = self._add_textbox(slide, **self._pos['copyright'])
        tf.vertical_anchor = MSO_VERTICAL_ANCHOR.BOTTOM
        p = tf.paragraphs[0]
        p.alignment = PP_PARAGRAPH_ALIGNMENT.RIGHT
        p.font.name = self._font_name
        p.font.size = self._font_size_copyright
        p.font.color.rgb = self._color_copyright
        p.text = copyright_text

    def _add_slide_name(self, slide, slide_name):
        tf = self._add_textbox(slide, **self._pos['slide_name'])
        p = tf.paragraphs[0]
        p.alignment = PP_PARAGRAPH_ALIGNMENT.LEFT
        p.font.name = self._font_name
        p.font.size = self._font_size_slide_name
        p.font.color.rgb = self._color_slide_name
        p.text = slide_name

    @staticmethod
    def _add_textbox(slide, x, y, w, h):
        tb = slide.shapes.add_textbox(x, y, w, h)
        tb.text_frame.vertical_anchor = MSO_VERTICAL_ANCHOR.MIDDLE
        tb.text_frame.auto_size = MSO_AUTO_SIZE.SHAPE_TO_FIT_TEXT
        tb.text_frame.word_wrap = True
        return tb.text_frame


def make_pptx(song_lyrics, song_properties, output_file, language_1, language_2, style_prop_dict):
    """
    Make a PPTX from a song.
    :param song_lyrics:
    :param song_properties:
    :param output_file:
    :param language_1:
    :param language_2:
    :param style_prop_dict:
    :return:
    """
    prs = LyricsPresentation(**style_prop_dict)

    content = dict()

    content['title_1'] = ''
    if language_1 == 'de':
        content['title_1'] = song_lyrics.title_de
    elif language_1 == 'en':
        content['title_1'] = song_lyrics.title_en
    elif language_1 == 'es':
        content['title_1'] = song_lyrics.title_es
    if not content['title_1']:
        raise ValueError('La canción no tiene título en el idioma "{}".'.format(language_1))

    content['title_2'] = ''
    if language_2 == 'de':
        content['title_2'] = song_lyrics.title_de
    elif language_2 == 'en':
        content['title_2'] = song_lyrics.title_en
    elif language_2 == 'es':
        content['title_2'] = song_lyrics.title_es
    if not content['title_2']:
        language_2 = None       # modify input parameter

    songbook_text = '; '.join(str(e) for e in song_properties.songsongbook_set.all())
    copyright_text = song_properties.copyright
    content['songbook_text'] = songbook_text
    content['copyright_text'] = copyright_text
    prs.add_title_slide(**content)

    # verses
    for v_name in song_lyrics.verse_order.split():
        content = dict()
        content['slide_name'] = v_name
        content['songbook_text'] = songbook_text
        content['copyright_text'] = copyright_text
        lg_list_1 = song_lyrics.verses.get(name=v_name, language=language_1).content_lines
        if not language_2:
            for lg1 in lg_list_1:
                content['lines_1'] = lg1
                prs.add_content_slide(**content)
        else:
            lg_list_2 = song_lyrics.verses.get(name=v_name, language=language_2).content_lines
            for lg1, lg2 in zip(lg_list_1, lg_list_2):
                content['lines_1'] = lg1
                content['lines_2'] = lg2
                prs.add_content_slide(**content)

    # final empty slide
    prs.add_content_slide()
    prs.save(output_file)
