# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import io
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.servers.basehttp import FileWrapper
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from pptx import Presentation
from songs.models import Author, AuthorDetail, Songbook, SongbookDetail, SongAuthor, SongSongbook
from songs.models import PresentationStyle, Song, SongLyrics, Verse, File, check_verse_order_2
from songs.openlyrics import parse_xml, validate_xml_tree, xml_to_song


def songs_list(request):
    """
    List songs.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de canciones', ),
    ]
    ctx['song_list'] = _get_current_song_list()

    return render(request, 'songs/song_list.html', ctx)


@login_required()
@permission_required('songs.delete_song', raise_exception=True)
def songs_list_deleted(request):
    """
    List deleted songs.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Canciones eliminadas', ),
    ]

    qs = SongLyrics.objects.filter(song__row_deleted=True, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(title_de__icontains=ctx['sf_text'])
    q_t2 = Q(title_en__icontains=ctx['sf_text'])
    q_t3 = Q(title_es__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2 | q_t3)

    ctx['sf_order'] = '0'
    qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('songs:songs-read', args=[e.song.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'songs/song_deleted_list.html', ctx)


@login_required()
@permission_required('songs.create_song', raise_exception=True)
def songs_create_from_file(request):
    """
    Create a song from an uploaded file.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Importar canción')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Importar canción', ),
    ]

    if request.method == 'POST':
        file = request.FILES.get('song_file')
        if not file:
            messages.error(request, 'No se recibió ningún archivo.')
            return render(request, 'songs/song_upload_form.html', ctx)

        if request.POST.get('file_type') == 'xml':
            # read XML file
            try:
                tree = parse_xml(file)
            except SyntaxError as err:
                messages.error(request, 'El archivo no contiene XML válido. Error: "{}"'
                                        '.'.format(err))
                return render(request, 'songs/song_upload_form.html', ctx)

            try:
                validate_xml_tree(tree)
            except AssertionError as err:
                messages.error(request, 'El archivo no contiene una canción válida en formato '
                                        'OpenLyrics. Error: "{}".'.format(err))
                return render(request, 'songs/song_upload_form.html', ctx)

            try:
                new_lyrics, new_properties = xml_to_song(tree, request.user.username)
            except ValidationError as err:
                for msgs in err.message_dict.values():
                    for msg in msgs:
                        messages.error(request, msg)
                return render(request, 'songs/song_upload_form.html', ctx)
        else:
            # read PPTX file
            default_language = 'de'

            new_lyrics = dict()
            new_lyrics['title_de'] = 'PowerPoint'
            new_lyrics['singable_de'] = True
            new_lyrics['title_en'] = ''
            new_lyrics['singable_en'] = False
            new_lyrics['title_es'] = ''
            new_lyrics['singable_es'] = False
            new_lyrics['verse_order'] = ''
            new_lyrics['verse_list'] = []

            new_properties = dict()
            new_properties['songauthor_list'] = []
            new_properties['songsongbook_list'] = []
            new_properties['ccli_no'] = 0
            new_properties['variant'] = ''
            new_properties['version'] = ''
            new_properties['released'] = ''
            new_properties['publisher'] = ''
            new_properties['copyright'] = ''
            new_properties['keywords'] = ''
            new_properties['themes'] = ''
            new_properties['comments'] = ''
            new_properties['key'] = ''
            new_properties['transposition'] = 0
            new_properties['tempo'] = ''

            prs = Presentation(file)
            for i, slide in enumerate(prs.slides):
                paragraph_list = []
                for shape in slide.shapes:
                    if shape.has_text_frame:
                        for paragraph in shape.text_frame.paragraphs:
                            if paragraph.text.strip():
                                paragraph_list.append(paragraph.text.strip())
                new_content = _join_content(paragraph_list)

                if new_content:
                    if i == 0:      # slide with title
                        new_lyrics['title_de'] = new_content[:100]
                    else:
                        v = Verse()
                        v.language = default_language
                        v.name = 'v{}'.format(i)
                        v.content = new_content
                        new_lyrics['verse_list'].append(v)
            new_lyrics['verse_order'] = ' '.join(v.name for v in new_lyrics['verse_list'])

        song = Song()
        try:
            song.do_create_lyrics(user=request.user, **new_lyrics)
            song.do_create_properties(request.user)
            song.do_update_properties(user=request.user, **new_properties)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'songs/song_upload_form.html', ctx)

        _clear_songs_cache()
        return redirect('songs:songs-read', song.pk)
    else:
        return render(request, 'songs/song_upload_form.html', ctx)


@login_required()
@permission_required('songs.create_song', raise_exception=True)
def songs_create(request):
    """
    Create a song.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear nueva canción')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Crear nueva canción', ),
    ]
    ctx['CRUD'] = 'C'
    ctx['song'] = Song()
    return _songs_create_or_update_lyrics(request, ctx)


def songs_read(request, song_id):
    """
    Read a song.
    :param request: HttpRequest
    :param song_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles de canción')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Canción {}'.format(song_id), ),
    ]
    ctx['CRUD'] = 'R'
    ctx['song'] = get_object_or_404(Song, pk=song_id)

    ctx['song_lyrics'] = ctx['song'].current_lyrics
    ctx['verse_ordering'] = request.GET.get('verse_ordering', 'language')
    ctx['verse_list'] = sorted(
        ctx['song_lyrics'].verses.all(),
        key=lambda x: _get_sort_key(x.name, x.language, ctx['verse_ordering']))

    ctx['song_properties'] = ctx['song'].current_properties
    ctx['author_list'] = sorted(
        [str(e) for e in ctx['song_properties'].songauthor_set.all()])
    ctx['songbook_list'] = sorted(
        [str(e) for e in ctx['song_properties'].songsongbook_set.all()])

    ctx['presentation_styles'] = PresentationStyle.objects.order_by('pk')

    if ctx['song'].row_deleted:
        messages.error(request, 'Esta canción se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Canciones eliminadas', reverse('songs:songs-list-deleted')))
    else:
        try:
            check_verse_order_2(ctx['song_lyrics'].verse_order, ctx['verse_list'])
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.warning(request, msg)

    return render(request, 'songs/song_read_form.html', ctx)


@login_required()
@permission_required('songs.update_song', raise_exception=True)
def songs_update_lyrics(request, song_id):
    """
    Update the lyrics of a song.
    :param request: HttpRequest
    :param song_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar letra de canción')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Canción {}'.format(song_id), reverse('songs:songs-read', args=[song_id])),
        ('Modificar letra', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['song'] = get_object_or_404(Song, pk=song_id, row_deleted=False)
    ctx['song_lyrics'] = ctx['song'].current_lyrics
    ctx['verse_list'] = sorted(
        ctx['song_lyrics'].verses.all(),
        key=lambda x: _get_sort_key(x.name, x.language, 'language'))
    return _songs_create_or_update_lyrics(request, ctx)


@login_required()
@permission_required('songs.update_song', raise_exception=True)
def songs_update_properties(request, song_id):
    """
    Update the properties of a song.
    :param request: HttpRequest
    :param song_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar propiedades de canción')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Canción {}'.format(song_id), reverse('songs:songs-read', args=[song_id])),
        ('Modificar propiedades', ),
    ]
    ctx['song'] = get_object_or_404(Song, pk=song_id, row_deleted=False)
    ctx['song_lyrics'] = ctx['song'].current_lyrics
    ctx['song_properties'] = ctx['song'].current_properties
    ctx['author_list'] = sorted(
        [{'pk': e.pk, 'name': e.name} for e in Author.objects.filter(row_deleted=False)],
        key=lambda x: x['name'])
    ctx['songbook_list'] = sorted(
        [{'pk': e.pk, 'name': e.name} for e in Songbook.objects.filter(row_deleted=False)],
        key=lambda x: x['name'])

    if request.method == 'POST':
        new_data = dict()

        list1 = []
        for e in request.POST.getlist('authors'):
            try:
                e_pk, e_dsc = e.split('$')
                pk_, optional_ = e_pk.split('-')
                n = SongAuthor()
                n.type = optional_

                if pk_ == 'new':
                    new_name = e_dsc.split('(')[0].strip()
                    if new_name:
                        det = AuthorDetail.objects.filter(
                            name=new_name, row_time_end__isnull=True,
                            author__row_deleted=False).first()
                        if det:
                            # author with 'new_name' already exists
                            n.author = det.author
                            list1.append(n)
                        else:
                            # create new author
                            author = Author()
                            author.do_create(new_name, request.user)
                            n.author = author
                            list1.append(n)
                else:
                    author = Author.objects.filter(pk=pk_, row_deleted=False).first()
                    if author:
                        # author exists and is not deleted
                        n.author = author
                        list1.append(n)
            except ValueError:
                pass
        new_data['songauthor_list'] = list1

        list1 = []
        for e in request.POST.getlist('songbooks'):
            try:
                e_pk, e_dsc = e.split('$')
                pk_, optional_ = e_pk.split('-')
                n = SongSongbook()
                n.entry = optional_

                if pk_ == 'new':
                    new_name = e_dsc.split('#')[0].strip()
                    if new_name:
                        det = SongbookDetail.objects.filter(
                            name=new_name, row_time_end__isnull=True,
                            songbook__row_deleted=False).first()
                        if det:
                            # songbook with 'new_name' already exists
                            n.songbook = det.songbook
                            list1.append(n)
                        else:
                            # create new songbook
                            songbook = Songbook()
                            songbook.do_create(new_name, request.user)
                            n.songbook = songbook
                            list1.append(n)
                else:
                    songbook = Songbook.objects.filter(pk=pk_, row_deleted=False).first()
                    if songbook:
                        # songbook exists and is not deleted
                        n.songbook = songbook
                        list1.append(n)
            except ValueError:
                pass
        new_data['songsongbook_list'] = list1

        new_data['ccli_no'] = 0
        try:
            new_data['ccli_no'] = int(request.POST.get('ccli_no', 0))
        except ValueError:
            pass

        new_data['variant'] = request.POST.get('variant', '')
        new_data['version'] = request.POST.get('version', '')
        new_data['released'] = request.POST.get('released', '')
        new_data['publisher'] = request.POST.get('publisher', '')
        new_data['copyright'] = request.POST.get('copyright', '')
        new_data['keywords'] = request.POST.get('keywords', '')
        new_data['themes'] = request.POST.get('themes', '')
        new_data['comments'] = request.POST.get('comments', '')
        new_data['key'] = request.POST.get('key', '')

        new_data['transposition'] = 0
        try:
            new_data['transposition'] = int(request.POST.get('transposition', 0))
        except ValueError:
            pass

        new_data['tempo'] = request.POST.get('tempo', '')

        try:
            ctx['song'].do_update_properties(user=request.user, **new_data)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'songs/song_update_properties_form.html', ctx)

        _clear_songs_cache()
        return redirect('songs:songs-read', ctx['song'].pk)
    else:
        return render(request, 'songs/song_update_properties_form.html', ctx)


@login_required()
@permission_required('songs.delete_song', raise_exception=True)
def songs_delete(request, song_id):
    """
    Delete a song.
    :param request: HttpRequest
    :param song_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar canción')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Canción {}'.format(song_id), reverse('songs:songs-read', args=[song_id])),
        ('Eliminar canción', ),
    ]
    ctx['CRUD'] = 'D'
    ctx['song'] = get_object_or_404(Song, pk=song_id, row_deleted=False)
    ctx['song_lyrics'] = ctx['song'].current_lyrics

    if request.method == 'POST':
        ctx['song'].do_delete_lyrics(request.user)
        ctx['song'].do_delete_properties(request.user)
        _clear_songs_cache()
        return redirect('songs:songs-list')
    else:
        return render(request, 'songs/song_read_form.html', ctx)


@login_required()
@permission_required('songs.delete_song', raise_exception=True)
def songs_delete_undo(request, song_id):
    """
    Undo the delete of a song.
    :param request: HttpRequest
    :param song_id:
    :return: HttpResponse
    """
    song = get_object_or_404(Song, pk=song_id)

    if request.method == 'POST':
        if song.row_deleted:
            song.undo_delete_lyrics(request.user)
            song.undo_delete_properties(request.user)
            _clear_songs_cache()

    return redirect('songs:songs-read', song.pk)


@login_required()
@permission_required('songs.update_song', raise_exception=True)
def songs_history_lyrics(request, song_id):
    """
    View changes history of song lyrics.
    :param request: HttpRequest
    :param song_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Canción {}'.format(song_id), reverse('songs:songs-read', args=[song_id])),
        ('Historial de cambios', ),
    ]
    ctx['song'] = get_object_or_404(Song, pk=song_id)

    paginator = Paginator(ctx['song'].lyrics.order_by('-row_time_start'), 5)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'songs/song_lyrics_history_list.html', ctx)


@login_required()
@permission_required('songs.update_song', raise_exception=True)
def songs_history_properties(request, song_id):
    """
    View changes history of song properties.
    :param request: HttpRequest
    :param song_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Canción {}'.format(song_id), reverse('songs:songs-read', args=[song_id])),
        ('Historial de cambios', ),
    ]
    ctx['song'] = get_object_or_404(Song, pk=song_id)

    paginator = Paginator(ctx['song'].properties.order_by('-row_time_start'), 10)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'songs/song_properties_history_list.html', ctx)


def songs_make_pptx(request, song_id):
    """
    Get existing or make new PPTX for a song.
    :param request: HttpRequest
    :param song_id:
    :return: HttpResponse
    """
    song = get_object_or_404(Song, pk=song_id, row_deleted=False)
    language_1 = request.GET.get('language1', '--')
    language_2 = request.GET.get('language2', '--')
    style_id = request.GET.get('style', 1)

    try:
        style = PresentationStyle.objects.get(pk=style_id)
        style_update_time = style.row_time_update
        style_prop_dict = style.prop_dict
    except PresentationStyle.DoesNotExist:
        style_id = 0
        style_update_time = None
        style_prop_dict = dict()
    codename_ = '{}|{}|{}'.format(language_1[:2], language_2[:2], style_id)

    try:
        f = song.files.get(content_type='pptx', codename=codename_)
        if f.row_time < max(song.current_lyrics.row_time_start,
                            song.current_properties.row_time_start):
            f = None
        elif style_update_time and f.row_time < style_update_time:
            f = None
    except File.DoesNotExist:
        f = None

    if not f:
        try:
            f = song.make_pptx(language_1, language_2, style_prop_dict, codename_, request.user)
        except ValueError as err:
            messages.error(request, 'Ocurrió un error al descargar el PPTX: {}'.format(err))
            return redirect('songs:songs-read', song.pk)

    response = HttpResponse(
        FileWrapper(io.BytesIO(f.bytes)),
        content_type=File.CONTENT_TYPES[f.content_type])
    response['Content-Disposition'] = 'attachment; filename={}.{}'.format(
        f.download_name, f.content_type)
    response['Content-Length'] = f.size
    return response


def songs_make_xml(request, song_id):
    """
    Get existing or make new XML for a song.
    :param request: HttpRequest
    :param song_id:
    :return: HttpResponse
    """
    song = get_object_or_404(Song, pk=song_id, row_deleted=False)

    try:
        f = song.files.get(content_type='xml', codename='')
        if f.row_time < max(song.current_lyrics.row_time_start,
                            song.current_properties.row_time_start):
            f = None
    except File.DoesNotExist:
        f = None

    if not f:
        f = song.make_xml(request.user)

    response = HttpResponse(
        FileWrapper(io.BytesIO(f.bytes)),
        content_type=File.CONTENT_TYPES[f.content_type])
    # 'attachment' option is not included in the 'Content-Disposition' header so that
    # the file can be viewed in the browser and does not trigger a download right away
    response['Content-Disposition'] = 'filename={}.{}'.format(f.download_name, f.content_type)
    response['Content-Length'] = f.size
    return response


def _songs_create_or_update_lyrics(request, ctx):
    """
    Create or update a song.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    if request.method == 'POST':
        new_data = dict()

        new_data['title_de'] = request.POST.get('title_de', '')
        if new_data['title_de'] and request.POST.get('singable_de') == 'True':
            new_data['singable_de'] = True
        else:
            new_data['singable_de'] = False

        new_data['title_en'] = request.POST.get('title_en', '')
        if new_data['title_en'] and request.POST.get('singable_en') == 'True':
            new_data['singable_en'] = True
        else:
            new_data['singable_en'] = False

        new_data['title_es'] = request.POST.get('title_es', '')
        if new_data['title_es'] and request.POST.get('singable_es') == 'True':
            new_data['singable_es'] = True
        else:
            new_data['singable_es'] = False

        # remove excessive whitespaces from verse_order
        verse_order_list = []
        for e in request.POST.get('verse_order', '').split():
            if e.strip():
                verse_order_list.append(e.strip())
        new_data['verse_order'] = ' '.join(verse_order_list)

        verse_dict = {}
        for e in request.POST.getlist('verses'):
            v = Verse()
            v.language, v.name = e.split(':')
            v.content = _join_content(
                [s.strip() for s in request.POST.get(e, '').split('\n')])
            verse_dict['{}:{}'.format(v.language, v.name)] = v          # prevents duplicate verses
        new_data['verse_list'] = verse_dict.values()

        # set default verse_order if its empty
        if not new_data['verse_order']:
            new_data['verse_order'] = ' '.join(sorted(list(set(
                v.name for v in new_data['verse_list']))))

        try:
            if ctx['CRUD'] == 'C':
                ctx['song'].do_create_lyrics(user=request.user, **new_data)
                ctx['song'].do_create_properties(request.user)
            else:
                ctx['song'].do_update_lyrics(user=request.user, **new_data)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            # send new data to template, so that user can correct his errors
            ctx['song_lyrics'] = {
                'title_de': new_data['title_de'],
                'title_en': new_data['title_en'],
                'title_es': new_data['title_es'],
                'verse_order': new_data['verse_order'],
            }
            ctx['verse_list'] = new_data['verse_list']
            return render(request, 'songs/song_update_lyrics_form.html', ctx)

        _clear_songs_cache()
        return redirect('songs:songs-read', ctx['song'].pk)
    else:
        return render(request, 'songs/song_update_lyrics_form.html', ctx)


def _get_sort_key(v_name, v_language, verse_ordering):
    """
    Get key to sort the verse list.
    :param v_name:
    :param v_language:
    :param verse_ordering:
    :return: (type, number, language) or (language, type, number)
    """
    try:
        num = int(v_name[1:])
    except ValueError:
        num = 0

    if verse_ordering == 'language':
        return v_language, Verse.VERSE_TYPES.get(v_name[:1], 0), num
    else:
        return Verse.VERSE_TYPES.get(v_name[:1], 0), num, v_language


def _join_content(content_list):
    """
    Join verse content.
    :param content_list:
    :return:
    """
    content = '\n'.join(content_list)
    content = content.strip()           # remove newline after last line
    if content:
        while '\n\n' in content:
            content = content.replace(
                '\n\n',
                '\n{}\n'.format(Verse.slide_break()))
        double_break = '{0}\n{0}'.format(Verse.slide_break())
        while double_break in content:
            content = content.replace(
                double_break,
                Verse.slide_break())
    return content


def _clear_songs_cache():
    try:
        # this function is only present with django-redis
        n = cache.delete_pattern('SONG_LIST_*')
        print('OK cache.delete_pattern', n)
    except AttributeError:
        print('songs.views._clear_songs_cache AttributeError cache.delete_pattern')


def _get_current_song_list():
    cache_key = 'SONG_LIST_FULL'
    song_list = cache.get(cache_key)

    if song_list is None:
        song_list = []
        for lyrics in SongLyrics.objects.filter(song__row_deleted=False, row_time_end__isnull=True):
            # possibly multiple instances for one lyrics element, because of multiple languages
            aux_list = []
            if len(lyrics.title_de) > 0:
                aux_list.append({
                    'language': 'de',
                    'title': lyrics.title_de,
                    'singable': lyrics.singable_de,
                })
            if len(lyrics.title_en) > 0:
                aux_list.append({
                    'language': 'en',
                    'title': lyrics.title_en,
                    'singable': lyrics.singable_en,
                })
            if len(lyrics.title_es) > 0:
                aux_list.append({
                    'language': 'es',
                    'title': lyrics.title_es,
                    'singable': lyrics.singable_es,
                })

            # add common info
            properties = lyrics.song.current_properties
            authors = ', '.join(str(e) for e in properties.songauthor_set.all())
            songbooks = ', '.join(str(e) for e in properties.songsongbook_set.all())
            for elem in aux_list:
                elem['authors'] = authors
                elem['songbooks'] = songbooks
                elem['variant'] = properties.variant
                elem['url'] = reverse('songs:songs-read', args=[lyrics.song.pk])
                song_list.append(elem)

        song_list = sorted(
            song_list,
            key=lambda x: (x['language'], x['title']))

        cache.set(cache_key, song_list, timeout=None)

    return song_list
