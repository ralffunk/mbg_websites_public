# -*- coding: utf-8 -*-
"""
Models.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
from tempfile import SpooledTemporaryFile
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone as tz


class Author(models.Model):
    """
    An author with version control.
    """
    TYPES = {
        'm': 'música',
        'w': 'letra',
        't': 'traducción',
    }
    row_deleted = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()
        permissions = (
            ('create_author', 'crear autor'),
            ('update_author', 'modificar autor'),
            ('delete_author', 'eliminar autor'),
        )

    def __str__(self):
        return self.name

    @property
    def current_details(self):
        return self.details.filter(row_time_end__isnull=True).order_by('-row_time_start').first()

    def do_create(self, new_name, user):
        new_det = AuthorDetail()
        new_det.name = new_name
        new_det.row_action = 'CREATE'
        new_det.row_user_insert = user
        new_det.full_clean(exclude=['author'])

        if AuthorDetail.objects.filter(name=new_name, row_time_end__isnull=True,
                                       author__row_deleted=False).exists():
            raise ValidationError(
                {'name': 'El nombre del autor no puede ser duplicado.'})

        self.save()
        new_det.author = self
        new_det.save()

    def do_update(self, new_name, user):
        new_det = AuthorDetail()
        new_det.author = self
        new_det.name = new_name
        new_det.row_action = 'UPDATE'
        new_det.row_user_insert = user
        new_det.full_clean()

        old_det = self.current_details
        if old_det:
            if not new_det.is_equal(old_det):
                if AuthorDetail.objects.filter(name=new_name, row_time_end__isnull=True,
                                               author__row_deleted=False).exists():
                    raise ValidationError(
                        {'name': 'El nombre del autor no puede ser duplicado.'})

                old_det.row_time_end = tz.now()
                old_det.save()
                new_det.save()
        else:
            new_det.save()

    def do_delete(self, user):
        new_det = AuthorDetail()
        new_det.author = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.name = old_det.name

        new_det.row_action = 'DELETE'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = True
        self.save()

    def undo_delete(self, user):
        new_det = AuthorDetail()
        new_det.author = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.name = old_det.name

        new_det.row_action = 'UNDO_D'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = False
        self.save()

    @property
    def name(self):
        det = self.current_details
        if det:
            return det.name
        elif self.pk:
            return 'Author(pk: {})'.format(self.pk)
        return ''


class AuthorDetail(models.Model):
    """
    An author detail with version control.
    """
    author = models.ForeignKey(Author, related_name='details')
    name = models.CharField(max_length=100, default='', error_messages={
        'blank': 'El nombre del autor no puede quedar vacío.'})
    row_action = models.CharField(max_length=6, default='CREATE')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.name

    def is_equal(self, other):
        if self.name == other.name:
            return True
        return False


class Songbook(models.Model):
    """
    A songbook with version control.
    """
    row_deleted = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()
        permissions = (
            ('create_songbook', 'crear himnario'),
            ('update_songbook', 'modificar himnario'),
            ('delete_songbook', 'eliminar himnario'),
        )

    def __str__(self):
        return self.name

    @property
    def current_details(self):
        return self.details.filter(row_time_end__isnull=True).order_by('-row_time_start').first()

    def do_create(self, new_name, user):
        new_det = SongbookDetail()
        new_det.name = new_name
        new_det.row_action = 'CREATE'
        new_det.row_user_insert = user
        new_det.full_clean(exclude=['songbook'])

        if SongbookDetail.objects.filter(name=new_name, row_time_end__isnull=True,
                                         songbook__row_deleted=False).exists():
            raise ValidationError(
                {'name': 'El nombre del himnario no puede ser duplicado.'})

        self.save()
        new_det.songbook = self
        new_det.save()

    def do_update(self, new_name, user):
        new_det = SongbookDetail()
        new_det.songbook = self
        new_det.name = new_name
        new_det.row_action = 'UPDATE'
        new_det.row_user_insert = user
        new_det.full_clean()

        old_det = self.current_details
        if old_det:
            if not new_det.is_equal(old_det):
                if SongbookDetail.objects.filter(name=new_name, row_time_end__isnull=True,
                                                 songbook__row_deleted=False).exists():
                    raise ValidationError(
                        {'name': 'El nombre del himnario no puede ser duplicado.'})

                old_det.row_time_end = tz.now()
                old_det.save()
                new_det.save()
        else:
            new_det.save()

    def do_delete(self, user):
        new_det = SongbookDetail()
        new_det.songbook = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.name = old_det.name

        new_det.row_action = 'DELETE'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = True
        self.save()

    def undo_delete(self, user):
        new_det = SongbookDetail()
        new_det.songbook = self

        old_det = self.current_details
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.name = old_det.name

        new_det.row_action = 'UNDO_D'
        new_det.row_user_insert = user
        new_det.save()
        self.row_deleted = False
        self.save()

    @property
    def name(self):
        det = self.current_details
        if det:
            return det.name
        elif self.pk:
            return 'Songbook(pk: {})'.format(self.pk)
        return ''


class SongbookDetail(models.Model):
    """
    A songbook detail with version control.
    """
    songbook = models.ForeignKey(Songbook, related_name='details')
    name = models.CharField(max_length=100, default='', error_messages={
        'blank': 'El nombre del himnario no puede quedar vacío.'})
    row_action = models.CharField(max_length=6, default='CREATE')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.name

    def is_equal(self, other):
        if self.name == other.name:
            return True
        return False


class Song(models.Model):
    """
    A song with version control.
    """
    row_deleted = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()
        permissions = (
            ('create_song', 'crear canción'),
            ('update_song', 'modificar canción'),
            ('delete_song', 'eliminar canción'),
        )

    def __str__(self):
        return 'Song(pk: {})'.format(self.pk)

    @property
    def current_lyrics(self):
        return self.lyrics.filter(row_time_end__isnull=True).order_by('-row_time_start').first()

    @property
    def first_title(self):
        if self.current_lyrics.title_de:
            title = self.current_lyrics.title_de
        elif self.current_lyrics.title_en:
            title = self.current_lyrics.title_en
        else:
            title = self.current_lyrics.title_es

        if self.row_deleted:
            title += ' [ELIMINADO]'
        return title

    @property
    def verse_names(self):
        return sorted(set(v.name for v in self.current_lyrics.verses.all()))

    def do_create_lyrics(self, title_de, singable_de, title_en, singable_en, title_es, singable_es,
                         verse_order, verse_list, user):
        new_det = SongLyrics()
        new_det.title_de = title_de
        new_det.singable_de = singable_de
        new_det.title_en = title_en
        new_det.singable_en = singable_en
        new_det.title_es = title_es
        new_det.singable_es = singable_es
        new_det.verse_order = verse_order
        new_det.row_action = 'CREATE'
        new_det.row_user_insert = user

        new_det.full_clean(exclude=['song'])
        if verse_list:
            for v in verse_list:
                v.full_clean(exclude=['song_lyrics'])
        else:
            raise ValidationError(
                {'song': 'La canción debe tener por lo menos una estrofa.'})

        check_unique_verses(verse_list)
        check_verse_order_1(new_det.verse_order, verse_list)
        # check_verse_order_2(new_det.verse_order, verse_list)      # will only be a warning
        check_languages_1(new_det.title_de, new_det.title_en, new_det.title_es, verse_list)
        check_languages_2(new_det.title_de, new_det.title_en, new_det.title_es, verse_list)
        check_verse_content(verse_list)

        self.save()
        new_det.song = self
        new_det.save()
        for v in verse_list:
            v.song_lyrics = new_det
            v.save()

    def do_update_lyrics(self, title_de, singable_de, title_en, singable_en, title_es, singable_es,
                         verse_order, verse_list, user):
        new_det = SongLyrics()
        new_det.song = self
        new_det.title_de = title_de
        new_det.singable_de = singable_de
        new_det.title_en = title_en
        new_det.singable_en = singable_en
        new_det.title_es = title_es
        new_det.singable_es = singable_es
        new_det.verse_order = verse_order
        new_det.row_action = 'UPDATE'
        new_det.row_user_insert = user
        new_det.full_clean()
        if verse_list:
            for v in verse_list:
                v.full_clean(exclude=['song_lyrics'])
        else:
            raise ValidationError(
                {'song': 'La canción debe tener por lo menos una estrofa.'})

        check_unique_verses(verse_list)
        check_verse_order_1(new_det.verse_order, verse_list)
        # check_verse_order_2(new_det.verse_order, verse_list)      # will only be a warning
        check_languages_1(new_det.title_de, new_det.title_en, new_det.title_es, verse_list)
        check_languages_2(new_det.title_de, new_det.title_en, new_det.title_es, verse_list)
        check_verse_content(verse_list)

        old_det = self.current_lyrics
        if old_det:
            if not new_det.is_equal(old_det, verse_list):
                old_det.row_time_end = tz.now()
                old_det.save()
                new_det.save()
                for v in verse_list:
                    v.song_lyrics = new_det
                    v.save()
        else:
            new_det.save()
            for v in verse_list:
                v.song_lyrics = new_det
                v.save()

    def do_delete_lyrics(self, user):
        new_det = SongLyrics()
        new_det.song = self

        old_det = self.current_lyrics
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.title_de = old_det.title_de
            new_det.singable_de = old_det.singable_de
            new_det.title_en = old_det.title_en
            new_det.singable_en = old_det.singable_en
            new_det.title_es = old_det.title_es
            new_det.singable_es = old_det.singable_es
            new_det.verse_order = old_det.verse_order
            new_det.row_action = 'DELETE'
            new_det.row_user_insert = user
            new_det.save()
            for ov in old_det.verses.all():
                nv = Verse()
                nv.song_lyrics = new_det
                nv.language = ov.language
                nv.name = ov.name
                nv.content = ov.content
                nv.save()
        else:
            new_det.row_action = 'DELETE'
            new_det.row_user_insert = user
            new_det.save()

        self.row_deleted = True
        self.save()

    def undo_delete_lyrics(self, user):
        new_det = SongLyrics()
        new_det.song = self

        old_det = self.current_lyrics
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.title_de = old_det.title_de
            new_det.singable_de = old_det.singable_de
            new_det.title_en = old_det.title_en
            new_det.singable_en = old_det.singable_en
            new_det.title_es = old_det.title_es
            new_det.singable_es = old_det.singable_es
            new_det.verse_order = old_det.verse_order
            new_det.row_action = 'UNDO_D'
            new_det.row_user_insert = user
            new_det.save()
            for ov in old_det.verses.all():
                nv = Verse()
                nv.song_lyrics = new_det
                nv.language = ov.language
                nv.name = ov.name
                nv.content = ov.content
                nv.save()
        else:
            new_det.row_action = 'UNDO_D'
            new_det.row_user_insert = user
            new_det.save()

        self.row_deleted = False
        self.save()

    @property
    def current_properties(self):
        return self.properties.filter(row_time_end__isnull=True).order_by('-row_time_start').first()

    def do_create_properties(self, user):
        # properties are only created with default values and AFTER the song has been saved
        new_det = SongProperties()
        new_det.song = self
        new_det.row_action = 'CREATE'
        new_det.row_user_insert = user
        new_det.save()

    def do_update_properties(self, songauthor_list, songsongbook_list, ccli_no, variant, version,
                             released, publisher, copyright, keywords, themes, comments, key,
                             transposition, tempo, user):
        new_det = SongProperties()
        new_det.song = self
        new_det.ccli_no = ccli_no
        new_det.variant = variant
        new_det.version = version
        new_det.released = released
        new_det.publisher = publisher
        new_det.copyright = copyright
        new_det.keywords = keywords
        new_det.themes = themes
        new_det.comments = comments
        new_det.key = key
        new_det.transposition = transposition
        new_det.tempo = tempo
        new_det.row_action = 'UPDATE'
        new_det.row_user_insert = user
        new_det.full_clean()
        # no validation for authors and songbooks is needed

        old_det = self.current_properties
        if old_det:
            if not new_det.is_equal(old_det, songauthor_list, songsongbook_list):
                old_det.row_time_end = tz.now()
                old_det.save()
                new_det.save()
                for e in songauthor_list:
                    e.song_properties = new_det
                    e.save()
                for e in songsongbook_list:
                    e.song_properties = new_det
                    e.save()
        else:
            new_det.save()
            for e in songauthor_list:
                e.song_properties = new_det
                e.save()
            for e in songsongbook_list:
                e.song_properties = new_det
                e.save()

    def do_delete_properties(self, user):
        new_det = SongProperties()
        new_det.song = self

        old_det = self.current_properties
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.ccli_no = old_det.ccli_no
            new_det.variant = old_det.variant
            new_det.version = old_det.version
            new_det.released = old_det.released
            new_det.publisher = old_det.publisher
            new_det.copyright = old_det.copyright
            new_det.keywords = old_det.keywords
            new_det.themes = old_det.themes
            new_det.comments = old_det.comments
            new_det.key = old_det.key
            new_det.transposition = old_det.transposition
            new_det.tempo = old_det.tempo
            new_det.row_action = 'DELETE'
            new_det.row_user_insert = user
            new_det.save()
            for o in old_det.songauthor_set.all():
                n = SongAuthor()
                n.song_properties = new_det
                n.author = o.author
                n.type = o.type
                n.save()
            for o in old_det.songsongbook_set.all():
                n = SongSongbook()
                n.song_properties = new_det
                n.songbook = o.songbook
                n.entry = o.entry
                n.save()
        else:
            new_det.row_action = 'DELETE'
            new_det.row_user_insert = user
            new_det.save()

        self.row_deleted = True
        self.save()

    def undo_delete_properties(self, user):
        new_det = SongProperties()
        new_det.song = self

        old_det = self.current_properties
        if old_det:
            old_det.row_time_end = tz.now()
            old_det.save()
            new_det.ccli_no = old_det.ccli_no
            new_det.variant = old_det.variant
            new_det.version = old_det.version
            new_det.released = old_det.released
            new_det.publisher = old_det.publisher
            new_det.copyright = old_det.copyright
            new_det.keywords = old_det.keywords
            new_det.themes = old_det.themes
            new_det.comments = old_det.comments
            new_det.key = old_det.key
            new_det.transposition = old_det.transposition
            new_det.tempo = old_det.tempo
            new_det.row_action = 'UNDO_D'
            new_det.row_user_insert = user
            new_det.save()
            for o in old_det.songauthor_set.all():
                n = SongAuthor()
                n.song_properties = new_det
                n.author = o.author
                n.type = o.type
                n.save()
            for o in old_det.songsongbook_set.all():
                n = SongSongbook()
                n.song_properties = new_det
                n.songbook = o.songbook
                n.entry = o.entry
                n.save()
        else:
            new_det.row_action = 'UNDO_D'
            new_det.row_user_insert = user
            new_det.save()

        self.row_deleted = False
        self.save()

    def make_pptx(self, language_1, language_2, style_prop_dict, codename, user):
        song_lyrics = self.current_lyrics
        song_properties = self.current_properties

        with SpooledTemporaryFile() as output_file:
            # this may raise ValueError
            from songs import pptx_01
            pptx_01.make_pptx(song_lyrics, song_properties, output_file, language_1, language_2,
                              style_prop_dict)
            output_file.seek(0)
            file_bytes = output_file.read()

            download_name = ''
            if language_1 == 'de':
                download_name = '{}'.format(song_lyrics.title_de)
            if language_1 == 'en':
                download_name = '{}'.format(song_lyrics.title_en)
            if language_1 == 'es':
                download_name = '{}'.format(song_lyrics.title_es)

            if song_properties.variant:
                download_name += ' ({})'.format(song_properties.variant)
            if language_2 in ('de', 'en', 'es'):
                download_name += ' [{}-{}]'.format(language_1, language_2)
            download_name = download_name.lower()

            # when download_name is not ascii, an error happens with Gunicorn server,
            # but not with the Django development server. So we take non-ascii characters out
            download_name = download_name.replace('ä', 'ae').replace('ö', 'oe').replace('ü', 'ue')
            download_name = ''.join(
                e if ord(e) < 128 else '-'
                for e in download_name)

            # create or update file
            try:
                file = self.files.get(content_type='pptx', codename=codename)
            except File.DoesNotExist:
                file = File()
                file.song = self
                file.content_type = 'pptx'
                file.codename = codename
            file.download_name = download_name
            file.size = len(file_bytes)
            file.bytes = file_bytes
            file.row_time = tz.now()
            if user.is_authenticated():
                file.row_user_insert = user
            file.save()

            return file

    def make_xml(self, user):
        with SpooledTemporaryFile() as output_file:
            from songs.openlyrics import song_to_xml_file
            song_to_xml_file(self, output_file)
            output_file.seek(0)
            file_bytes = output_file.read()

            # create or update file
            try:
                file = self.files.get(content_type='xml', codename='')
            except File.DoesNotExist:
                file = File()
                file.song = self
                file.content_type = 'xml'
                file.codename = ''
            file.download_name = 'song_{}'.format(self.pk)
            file.size = len(file_bytes)
            file.bytes = file_bytes
            file.row_time = tz.now()
            if user.is_authenticated():
                file.row_user_insert = user
            file.save()

            return file


class SongLyrics(models.Model):
    """
    Song lyrics with version control.
    """
    song = models.ForeignKey(Song, related_name='lyrics')
    title_de = models.CharField(max_length=100, blank=True, default='')
    singable_de = models.BooleanField(default=True)
    title_en = models.CharField(max_length=100, blank=True, default='')
    singable_en = models.BooleanField(default=True)
    title_es = models.CharField(max_length=100, blank=True, default='')
    singable_es = models.BooleanField(default=True)
    verse_order = models.CharField(max_length=100, blank=True, default='')
    row_action = models.CharField(max_length=6, default='CREATE')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        if self.title_de:
            return self.title_de
        if self.title_en:
            return self.title_en
        if self.title_es:
            return self.title_es
        return 'SongLyrics(pk: {})'.format(self.pk)

    def is_equal(self, other, self_new_verse_list):
        if self.title_de == other.title_de:
            if self.singable_de == other.singable_de:
                if self.title_en == other.title_en:
                    if self.singable_en == other.singable_en:
                        if self.title_es == other.title_es:
                            if self.singable_es == other.singable_es:
                                if self.verse_order == other.verse_order:
                                    # check if the qty of verses is the same
                                    if len(self_new_verse_list) == other.verses.count():
                                        # check if the names and content of verses are the same
                                        self_new_verse_list = sorted(
                                            [(e.language, e.name, e.content)
                                             for e in self_new_verse_list])
                                        other_verse_list = sorted(
                                            [(e.language, e.name, e.content)
                                             for e in other.verses.all()])
                                        if self_new_verse_list == other_verse_list:
                                            return True
        return False


class Verse(models.Model):
    """
    A verse of the specified song with version control.

    Different slides for the same verse are separated by an empty line.
    """
    VERSE_TYPES = {
        'v': 1,
        'p': 2,
        'c': 3,
        'b': 4,
        'e': 5,
    }
    song_lyrics = models.ForeignKey(SongLyrics, related_name='verses')
    name = models.CharField(max_length=5, default='', error_messages={
        'blank': 'El nombre de la estrofa no puede quedar vacío.'})
    language = models.CharField(max_length=5, default='', error_messages={
        'blank': 'El idioma de la estrofa no puede quedar vacío.'})
    content = models.CharField(max_length=1000, default='', error_messages={
        'blank': 'La estrofa no puede quedar vacía.'})

    class Meta:
        default_permissions = ()
        ordering = ['song_lyrics', 'language', 'name']

    def __str__(self):
        return 'Verse(pk: {}, name: {}, language: {})'.format(self.pk, self.name, self.language)

    @property
    def content_lines(self):
        lg_list = []
        for lg in [s.strip() for s in self.content.split(Verse.slide_break())]:
            lg_list.append(lg.split('\n'))
        return lg_list

    @property
    def content_for_modification(self):
        return self.content.replace(Verse.slide_break(), '')

    @classmethod
    def slide_break(cls):
        return '#_SLIDE_BREAK_#'


class SongProperties(models.Model):
    """
    Song properties with version control.
    """
    song = models.ForeignKey(Song, related_name='properties')
    # identification
    # OBS: titles are part of SongLyrics
    ccli_no = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El número CCLI debe ser mayor o igual a 0.')])
    variant = models.CharField(max_length=25, blank=True, default='')
    version = models.CharField(max_length=25, blank=True, default='')
    released = models.CharField(max_length=25, blank=True, default='')
    publisher = models.CharField(max_length=100, blank=True, default='')
    copyright = models.CharField(max_length=100, blank=True, default='')
    # additional info
    # OBS: verse order is part of SongLyrics
    keywords = models.CharField(max_length=100, blank=True, default='')
    themes = models.CharField(max_length=100, blank=True, default='')
    comments = models.CharField(max_length=100, blank=True, default='')
    # musical properties
    key = models.CharField(max_length=25, blank=True, default='')
    transposition = models.IntegerField(default=0, validators=[
        MinValueValidator(-99, message='La transposición debe estar entre -99 y 99.'),
        MaxValueValidator(99, message='La transposición debe estar entre -99 y 99.')])
    tempo = models.CharField(max_length=25, blank=True, default='')
    row_action = models.CharField(max_length=6, default='CREATE')
    row_time_start = models.DateTimeField(default=tz.now)
    row_time_end = models.DateTimeField(null=True, blank=True)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return 'SongProperties(pk: {})'.format(self.pk)

    def is_equal(self, other, self_new_songauthor_list, self_new_songsongbook_list):
        # check if the authors are the same
        self_new_songauthor_list = sorted(
            [(e.author.pk, e.type) for e in self_new_songauthor_list])
        other_songauthor_list = sorted(
            [(e.author.pk, e.type) for e in other.songauthor_set.all()])
        if self_new_songauthor_list != other_songauthor_list:
            return False

        # check if the songbooks are the same
        self_new_songsongbook_list = sorted(
            [(e.songbook.pk, e.entry) for e in self_new_songsongbook_list])
        other_songsongbook_list = sorted(
            [(e.songbook.pk, e.entry) for e in other.songsongbook_set.all()])
        if self_new_songsongbook_list != other_songsongbook_list:
            return False

        if self.ccli_no == other.ccli_no:
            if self.variant == other.variant:
                if self.version == other.version:
                    if self.released == other.released:
                        if self.publisher == other.publisher:
                            if self.copyright == other.copyright:
                                if self.keywords == other.keywords:
                                    if self.themes == other.themes:
                                        if self.comments == other.comments:
                                            if self.key == other.key:
                                                if self.transposition == other.transposition:
                                                    if self.tempo == other.tempo:
                                                        return True
        return False


class SongAuthor(models.Model):
    """
    The many-to-many relationship between SongProperties and Author with version control.
    """
    song_properties = models.ForeignKey(SongProperties)
    author = models.ForeignKey(Author)
    type = models.CharField(max_length=3, blank=True, default='')      # mwt

    class Meta:
        default_permissions = ()

    def __str__(self):
        if self.type:
            return '{} ({})'.format(
                self.author.name,
                ' y '.join([Author.TYPES[e] for e in self.type]))
        return self.author.name

    def get_type_display(self):
        if self.type:
            return ' y '.join([Author.TYPES[e] for e in self.type])
        return ''


class SongSongbook(models.Model):
    """
    The many-to-many relationship between SongProperties and Songbook with version control.
    """
    song_properties = models.ForeignKey(SongProperties)
    songbook = models.ForeignKey(Songbook)
    entry = models.CharField(max_length=5, blank=True, default='')

    class Meta:
        default_permissions = ()

    def __str__(self):
        if self.entry:
            return '{} #{}'.format(self.songbook.name, self.entry)
        return self.songbook.name


class File(models.Model):
    """
    A file saved as binary data in the database.
    """
    CONTENT_TYPES = {
        'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'xml': 'application/xml',
    }
    song = models.ForeignKey(Song, related_name='files')
    content_type = models.CharField(max_length=5, blank=True, default='')
    codename = models.CharField(max_length=10, blank=True, default='')
    download_name = models.CharField(max_length=200, blank=True, default='')
    size = models.IntegerField(default=0)
    bytes = models.BinaryField()
    row_time = models.DateTimeField(default=tz.now)
    row_user_insert = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        unique_together = ('song', 'content_type', 'codename')

    def __str__(self):
        return 'File(pk: {})'.format(self.pk)


def check_unique_verses(verse_list):
    """
    Check for duplicate verse names and languages.
    :param verse_list:
    :raises ValidationError
    :return:
    """
    verse_set = set()
    for v in verse_list:
        if (v.name, v.language) not in verse_set:
            verse_set.add((v.name, v.language))
        else:
            raise ValidationError(
                {'song': 'La estrofa "{}" aparece más de una vez con el idioma "{}"'
                         '.'.format(v.name, v.language)})


def check_verse_order_1(verse_order, verse_list):
    """
    Check that all names in verse_order are existing verses.
    :param verse_order:
    :param verse_list:
    :raises ValidationError
    :return:
    """
    verse_name_set = set(v.name for v in verse_list)
    for v_name in verse_order.split():
        if v_name not in verse_name_set:
            raise ValidationError(
                {'song': 'El nombre de estrofa "{}" aparece en el orden de presentación pero no '
                         'corresponde a una estrofa existente.'.format(v_name)})


def check_verse_order_2(verse_order, verse_list):
    """
    Check that all verse names are present in verse_order.
    :param verse_order:
    :param verse_list:
    :raises ValidationError
    :return:
    """
    verse_name_set = set(v.name for v in verse_list)
    verse_order_set = set(s for s in verse_order.split())
    for v_name in verse_name_set:
        if v_name not in verse_order_set:
            raise ValidationError(
                {'song': 'La estrofa "{}" no está en el orden de presentación.'.format(v_name)})


def check_languages_1(title_de, title_en, title_es, verse_list):
    """
    Check that each verse has a translation for every title language.
    :param title_de:
    :param title_en:
    :param title_es:
    :param verse_list:
    :raises ValidationError
    :return:
    """
    title_language_list = []
    if title_de:
        title_language_list.append('de')
    if title_en:
        title_language_list.append('en')
    if title_es:
        title_language_list.append('es')

    verse_dict = {}
    for v in verse_list:
        if v.name in verse_dict:
            verse_dict[v.name].add(v.language)
        else:
            verse_dict[v.name] = set()
            verse_dict[v.name].add(v.language)
    for v_name, v_language_set in verse_dict.items():
        for t_language in title_language_list:
            if t_language not in v_language_set:
                raise ValidationError(
                    {'song': 'Hay un título con idioma "{}" pero la estrofa "{}" no tiene una '
                             'traducción para el mismo.'.format(t_language, v_name)})


def check_languages_2(title_de, title_en, title_es, verse_list):
    """
    Check that each verse translation language has a corresponding title in that language.
    :param title_de:
    :param title_en:
    :param title_es:
    :param verse_list:
    :raises ValidationError
    :return:
    """
    title_language_list = []
    if title_de:
        title_language_list.append('de')
    if title_en:
        title_language_list.append('en')
    if title_es:
        title_language_list.append('es')

    for v in verse_list:
        if v.language not in title_language_list:
            raise ValidationError(
                {'song': 'Hay una estrofa con el idioma "{}" pero no hay ningún título para el '
                         'mismo.'.format(v.language)})


def check_verse_content(verse_list):
    """
    Check that all verses with the same name have equal quantity of slides.
    :param verse_list:
    :raises ValidationError
    :return:
    """
    verse_dict = {}
    for v in verse_list:
        if v.name in verse_dict:
            verse_dict[v.name].append(len(v.content_lines))
        else:
            verse_dict[v.name] = [len(v.content_lines)]
    for v_name, v_lengths in verse_dict.items():
        if v_lengths.count(v_lengths[0]) != len(v_lengths):
            raise ValidationError(
                {'song': 'Las estrofas con nombre "{}" tienen distintas cantidades de diapositivas '
                         'para los diferentes idiomas.'.format(v_name)})


class PresentationStyle(models.Model):
    """
    Some style options for a PPTX.
    """
    BASE_FILE_NAMES = (
        ('4-3-black.pptx', '4:3 - fondo negro'),
        ('4-3-white.pptx', '4:3 - fondo blanco'),
        ('16-10-black.pptx', '16:10 - fondo negro'),
        ('16-10-white.pptx', '16:10 - fondo blanco'),
    )
    name = models.CharField(max_length=20, unique=True, default='', error_messages={
        'blank': 'El nombre del estilo no puede quedar vacío.',
        'unique': 'El nombre del estilo no puede ser duplicado.'})
    base_file_name = models.CharField(
        max_length=50, default=BASE_FILE_NAMES[0][0], choices=BASE_FILE_NAMES,
        error_messages={'blank': 'El estilo base no puede quedar vacío.'})
    color_language_1_r = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_language_1_g = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_language_1_b = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_language_2_r = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_language_2_g = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_language_2_b = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_songbook_r = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_songbook_g = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_songbook_b = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_copyright_r = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_copyright_g = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_copyright_b = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_slide_name_r = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_slide_name_g = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    color_slide_name_b = models.IntegerField(default=0, validators=[
        MinValueValidator(0, message='El color debe ser mayor o igual a 0.'),
        MaxValueValidator(255, message='El color debe ser menor o igual a 255.')])
    font_name = models.CharField(max_length=100, default='Calibri', error_messages={
        'blank': 'El tipo de letra no puede quedar vacío.'})
    font_size_title_1 = models.IntegerField(default=10, validators=[
        MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'),
        MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')])
    font_size_title_2 = models.IntegerField(default=10, validators=[
        MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'),
        MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')])
    font_size_songbook = models.IntegerField(default=10, validators=[
        MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'),
        MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')])
    font_size_copyright = models.IntegerField(default=10, validators=[
        MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'),
        MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')])
    font_size_slide_name = models.IntegerField(default=10, validators=[
        MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'),
        MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')])
    font_size_language_1 = models.IntegerField(default=10, validators=[
        MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'),
        MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')])
    font_size_language_2 = models.IntegerField(default=10, validators=[
        MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'),
        MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')])
    row_time_insert = models.DateTimeField(auto_now_add=True)
    row_time_update = models.DateTimeField(auto_now=True)
    row_user_insert = models.ForeignKey(User, related_name='created_styles',
                                        on_delete=models.PROTECT)
    row_user_update = models.ForeignKey(User, related_name='modified_styles',
                                        on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (
            ('create_style', 'crear estilo de presentación'),
            ('update_style', 'modificar estilo de presentación'),
            ('delete_style', 'eliminar estilo de presentación'),
        )

    def __str__(self):
        return self.name

    @property
    def color_language_1(self):
        return self.color_language_1_r, self.color_language_1_g, self.color_language_1_b

    @color_language_1.setter
    def color_language_1(self, e):
        self.color_language_1_r = e[0]
        self.color_language_1_g = e[1]
        self.color_language_1_b = e[2]

    @property
    def color_language_2(self):
        return self.color_language_2_r, self.color_language_2_g, self.color_language_2_b

    @color_language_2.setter
    def color_language_2(self, e):
        self.color_language_2_r = e[0]
        self.color_language_2_g = e[1]
        self.color_language_2_b = e[2]

    @property
    def color_songbook(self):
        return self.color_songbook_r, self.color_songbook_g, self.color_songbook_b

    @color_songbook.setter
    def color_songbook(self, e):
        self.color_songbook_r = e[0]
        self.color_songbook_g = e[1]
        self.color_songbook_b = e[2]

    @property
    def color_copyright(self):
        return self.color_copyright_r, self.color_copyright_g, self.color_copyright_b

    @color_copyright.setter
    def color_copyright(self, e):
        self.color_copyright_r = e[0]
        self.color_copyright_g = e[1]
        self.color_copyright_b = e[2]

    @property
    def color_slide_name(self):
        return self.color_slide_name_r, self.color_slide_name_g, self.color_slide_name_b

    @color_slide_name.setter
    def color_slide_name(self, e):
        self.color_slide_name_r = e[0]
        self.color_slide_name_g = e[1]
        self.color_slide_name_b = e[2]

    @property
    def prop_dict(self):
        d = dict()
        d['base_file_name'] = os.path.join(
            settings.BASE_DIR, 'songs', 'pptx_templates', self.base_file_name)
        d['color_language_1'] = self.color_language_1
        d['color_language_2'] = self.color_language_2
        d['color_songbook'] = self.color_songbook
        d['color_copyright'] = self.color_copyright
        d['color_slide_name'] = self.color_slide_name
        d['font_name'] = self.font_name
        d['font_size_title_1'] = self.font_size_title_1
        d['font_size_title_2'] = self.font_size_title_2
        d['font_size_songbook'] = self.font_size_songbook
        d['font_size_copyright'] = self.font_size_copyright
        d['font_size_slide_name'] = self.font_size_slide_name
        d['font_size_language_1'] = self.font_size_language_1
        d['font_size_language_2'] = self.font_size_language_2
        return d
