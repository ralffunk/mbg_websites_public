# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from mbg_websites import utils
from songs.models import Songbook, SongbookDetail


def songbooks_list(request):
    """
    List songbooks.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de himnarios', ),
    ]

    qs = SongbookDetail.objects.filter(songbook__row_deleted=False, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    qs = qs.filter(name__icontains=ctx['sf_text'])

    ctx['sf_order'] = '0'
    qs = qs.order_by('name')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    songbook_list = []
    for i, e in enumerate(qs):
        songbook_list.append({
            'url': reverse('songs:songbooks-read', args=[e.songbook.pk]),
            'is_header': True,
            'counter': i+1,
            'name': e.name,
        })

        for j, s in enumerate(e.songbook.songsongbook_set.filter(
                song_properties__row_time_end__isnull=True)):
            songbook_list.append({
                'url': reverse('songs:songs-read', args=[s.song_properties.song.pk]),
                'deleted': s.song_properties.song.row_deleted,
                'counter': j+1,
                'song_title': s.song_properties.song.first_title,
                'entry': s.entry,
            })

    paginator = Paginator(songbook_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'songs/songbook_list.html', ctx)


@login_required()
@permission_required('songs.delete_songbook', raise_exception=True)
def songbooks_list_deleted(request):
    """
    List deleted songbooks.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Himnarios', reverse('songs:songbooks-list')),
        ('Himnarios eliminados', ),
    ]

    qs = SongbookDetail.objects.filter(songbook__row_deleted=True, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    qs = qs.filter(name__icontains=ctx['sf_text'])

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('name')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('songs:songbooks-read', args=[e.songbook.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'songs/songbook_deleted_list.html', ctx)


@login_required()
@permission_required('songs.create_songbook', raise_exception=True)
def songbooks_create(request):
    """
    Create a songbook.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear nuevo himnario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Himnarios', reverse('songs:songbooks-list')),
        ('Crear nuevo himnario', ),
    ]
    ctx['CRUD'] = 'C'
    ctx['songbook'] = Songbook()
    return _songbooks_create_or_update(request, ctx)


@login_required()
@permission_required('songs.update_songbook', raise_exception=True)
def songbooks_read(request, songbook_id):
    """
    Read a songbook.
    :param request: HttpRequest
    :param songbook_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles del himnario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Himnarios', reverse('songs:songbooks-list')),
        ('Himnario {}'.format(songbook_id), ),
    ]
    ctx['CRUD'] = 'R'
    ctx['songbook'] = get_object_or_404(Songbook, pk=songbook_id)
    ctx['history_list'] = ctx['songbook'].details.order_by('-row_time_start')[:5]

    if ctx['songbook'].row_deleted:
        messages.error(request, 'Este himnario se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Himnarios eliminados', reverse('songs:songbooks-list-deleted')))

    return render(request, 'songs/songbook_form.html', ctx)


@login_required()
@permission_required('songs.update_songbook', raise_exception=True)
def songbooks_update(request, songbook_id):
    """
    Update a songbook.
    :param request: HttpRequest
    :param songbook_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar himnario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Himnarios', reverse('songs:songbooks-list')),
        ('Himnario {}'.format(songbook_id), reverse('songs:songbooks-read', args=[songbook_id])),
        ('Modificar himnario', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['songbook'] = get_object_or_404(Songbook, pk=songbook_id, row_deleted=False)
    return _songbooks_create_or_update(request, ctx)


@login_required()
@permission_required('songs.delete_songbook', raise_exception=True)
def songbooks_delete(request, songbook_id):
    """
    Delete a songbook.
    :param request: HttpRequest
    :param songbook_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar himnario')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Himnarios', reverse('songs:songbooks-list')),
        ('Himnario {}'.format(songbook_id), reverse('songs:songbooks-read', args=[songbook_id])),
        ('Eliminar himnario', ),
    ]
    ctx['CRUD'] = 'D'
    ctx['songbook'] = get_object_or_404(Songbook, pk=songbook_id, row_deleted=False)

    ctx['songbook'].can_be_deleted = True
    if ctx['songbook'].songsongbook_set.filter(song_properties__row_time_end__isnull=True).exists():
        ctx['songbook'].can_be_deleted = False
        messages.error(request, 'Este himnario no puede ser eliminado porque tiene canciones '
                                'asociadas.')

    if request.method == 'POST' and ctx['songbook'].can_be_deleted:
        ctx['songbook'].do_delete(request.user)
        return redirect('songs:songbooks-list')
    else:
        return render(request, 'songs/songbook_form.html', ctx)


@login_required()
@permission_required('songs.delete_songbook', raise_exception=True)
def songbooks_delete_undo(request, songbook_id):
    """
    Undo the delete of a songbook.
    :param request: HttpRequest
    :param songbook_id:
    :return: HttpResponse
    """
    songbook = get_object_or_404(Songbook, pk=songbook_id)

    if request.method == 'POST':
        if songbook.row_deleted:
            songbook.undo_delete(request.user)

    return redirect('songs:songbooks-read', songbook.pk)


@login_required()
@permission_required('songs.update_songbook', raise_exception=True)
def songbooks_history(request, songbook_id):
    """
    View changes history of a songbook.
    :param request: HttpRequest
    :param songbook_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Himnarios', reverse('songs:songbooks-list')),
        ('Himnario {}'.format(songbook_id), reverse('songs:songbooks-read', args=[songbook_id])),
        ('Historial del himnario', ),
    ]
    ctx['songbook'] = get_object_or_404(Songbook, pk=songbook_id)
    qs = ctx['songbook'].details

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(row_action__icontains=ctx['sf_text'])
    q_t2 = Q(name__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2)

    ctx.update(utils.get_sf_dates(request.GET))
    qs = qs.filter(row_time_start__range=(ctx['sf_date_start'], ctx['sf_date_end']))

    ctx['sf_order'] = '0'
    qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['songbook'].row_deleted:
        messages.error(request, 'Este himnario se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Himnarios eliminados', reverse('songs:songbooks-list-deleted')))

    return render(request, 'songs/songbook_history_list.html', ctx)


def _songbooks_create_or_update(request, ctx):
    """
    Create or update a songbook.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    if request.method == 'POST':
        try:
            if ctx['CRUD'] == 'C':
                ctx['songbook'].do_create(request.POST.get('name', ''), request.user)
            else:
                ctx['songbook'].do_update(request.POST.get('name', ''), request.user)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'songs/songbook_form.html', ctx)

        return redirect('songs:songbooks-read', ctx['songbook'].pk)
    else:
        return render(request, 'songs/songbook_form.html', ctx)
