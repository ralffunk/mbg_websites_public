# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import zipfile
from django.http import HttpResponse
from tempfile import SpooledTemporaryFile
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.servers.basehttp import FileWrapper
from django.core.urlresolvers import reverse
from django.db.models import Sum, Q
from django.shortcuts import render, redirect
from mbg_websites import utils
from songs.models import Song, File, PresentationStyle


@login_required()
def files_list(request):
    """
    List files.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Canciones', reverse('songs:songs-list')),
        ('Lista de archivos', ),
    ]

    qs = File.objects.all()

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(codename__icontains=ctx['sf_text'])
    q_t2 = Q(download_name__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2)

    ctx.update(utils.get_sf_dates(request.GET))
    qs = qs.filter(row_time__range=(ctx['sf_date_start'], ctx['sf_date_end']))

    if request.GET.get('sf_type', '') == 'pptx':
        ctx['sf_type'] = 'pptx'
        qs = qs.filter(content_type='pptx')
    elif request.GET.get('sf_type', '') == 'xml':
        ctx['sf_type'] = 'xml'
        qs = qs.filter(content_type='xml')
    else:
        ctx['sf_type'] = 'A'

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('-row_time')
    elif request.GET.get('sf_order', '') == '2':
        ctx['sf_order'] = '2'
        qs = qs.order_by('-size')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('song', 'content_type', 'codename')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_type={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_type'],
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    res = File.objects.aggregate(Sum('size'))
    if 'size__sum' in res:
        if res['size__sum'] is not None:
            ctx['sum_size'] = res['size__sum']

    return render(request, 'songs/file_list.html', ctx)


@login_required()
def files_download_zip(request):
    """
    Download zip with multiple files.
    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == 'POST':
        with SpooledTemporaryFile() as output_file:
            zf = zipfile.ZipFile(output_file, 'w', compression=zipfile.ZIP_DEFLATED)

            for pk in request.POST.getlist('files'):
                try:
                    f = File.objects.get(pk=pk)
                    zf.writestr('{}.{}'.format(f.download_name, f.content_type), f.bytes)
                except File.DoesNotExist:
                    pass

            zf.close()
            output_file.seek(0)
            response = HttpResponse(
                FileWrapper(output_file),
                content_type='application/x-zip-compressed')
            response['Content-Disposition'] = 'attachment; filename={}.{}'.format(
                'archivos_de_canciones', 'zip')
            return response
    else:
        return redirect('songs:files-list')


@login_required()
def files_download_zip_pptx(request):
    """
    Download zip with multiple files.
    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == 'POST' or True:
        language_1 = request.GET.get('language1', '--')
        language_2 = request.GET.get('language2', '--')
        style_id = request.GET.get('style', 1)

        try:
            style = PresentationStyle.objects.get(pk=style_id)
            style_update_time = style.row_time_update
            style_prop_dict = style.prop_dict
        except PresentationStyle.DoesNotExist:
            style_id = 0
            style_update_time = None
            style_prop_dict = dict()
        codename_ = '{}|{}|{}'.format(language_1[:2], language_2[:2], style_id)

        with SpooledTemporaryFile() as output_file:
            zf = zipfile.ZipFile(output_file, 'w', compression=zipfile.ZIP_DEFLATED)

            for song in Song.objects.filter(row_deleted=False):
                try:
                    f = song.files.get(content_type='pptx', codename=codename_)
                    if f.row_time < max(song.current_lyrics.row_time_start,
                                        song.current_properties.row_time_start):
                        f = None
                    elif style_update_time and f.row_time < style_update_time:
                        f = None
                except File.DoesNotExist:
                    f = None

                if not f:
                    try:
                        f = song.make_pptx(language_1, language_2, style_prop_dict, codename_,
                                           request.user)
                        zf.writestr('{}.{}'.format(f.download_name, f.content_type), f.bytes)
                    except ValueError as err:
                        zf.writestr('ERROR_song_{}.txt'.format(song.pk),
                                    'Ocurrió un error al generar el PPTX: {}\n'.format(err))

            zf.close()
            output_file.seek(0)
            response = HttpResponse(
                FileWrapper(output_file),
                content_type='application/x-zip-compressed')
            response['Content-Disposition'] = 'attachment; filename={}.{}'.format(
                'archivos_de_canciones', 'zip')
            return response
    else:
        return redirect('songs:files-list')


@login_required()
def files_download_zip_xml(request):
    """
    Download zip with XML files from all songs.
    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == 'POST' or True:
        with SpooledTemporaryFile() as output_file:
            zf = zipfile.ZipFile(output_file, 'w', compression=zipfile.ZIP_DEFLATED)

            for song in Song.objects.filter(row_deleted=False):
                try:
                    f = song.files.get(content_type='xml', codename='')
                    if f.row_time < max(song.current_lyrics.row_time_start,
                                        song.current_properties.row_time_start):
                        f = None
                except File.DoesNotExist:
                    f = None

                if not f:
                    f = song.make_xml(request.user)

                zf.writestr('{}.{}'.format(f.download_name, f.content_type), f.bytes)

            zf.close()
            output_file.seek(0)
            response = HttpResponse(
                FileWrapper(output_file),
                content_type='application/x-zip-compressed')
            response['Content-Disposition'] = 'attachment; filename={}.{}'.format(
                'archivos_de_canciones', 'zip')
            return response
    else:
        return redirect('songs:files-list')
