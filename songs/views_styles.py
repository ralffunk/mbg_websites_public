# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from songs.models import PresentationStyle


@login_required()
def styles_list(request):
    """
    List styles.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Estilos de presentación', ),
    ]

    qs = PresentationStyle.objects.all()

    ctx['sf_text'] = request.GET.get('sf_text', '')
    qs = qs.filter(name__icontains=ctx['sf_text'])

    ctx['sf_order'] = '0'
    qs = qs.order_by('name')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('songs:styles-read', args=[e.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'songs/style_list.html', ctx)


@login_required()
@permission_required('songs.create_style', raise_exception=True)
def styles_create(request):
    """
    Create a style.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear nuevo estilo')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Estilos', reverse('songs:styles-list')),
        ('Crear nuevo estilo', ),
    ]
    ctx['CRUD'] = 'C'
    ctx['style'] = PresentationStyle()
    ctx['style'].row_user_insert = request.user
    return _styles_create_or_update(request, ctx)


@login_required()
def styles_read(request, style_id):
    """
    Read a style.
    :param request: HttpRequest
    :param style_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles de estilo')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Estilos', reverse('songs:styles-list')),
        ('Estilo {}'.format(style_id), ),
    ]
    ctx['CRUD'] = 'R'
    ctx['style'] = get_object_or_404(PresentationStyle, pk=style_id)
    return render(request, 'songs/style_read_form.html', ctx)


@login_required()
@permission_required('songs.update_style', raise_exception=True)
def styles_update(request, style_id):
    """
    Update a style.
    :param request: HttpRequest
    :param style_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar estilo')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Estilos', reverse('songs:styles-list')),
        ('Estilo {}'.format(style_id), reverse('songs:styles-read', args=[style_id])),
        ('Modificar estilo', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['style'] = get_object_or_404(PresentationStyle, pk=style_id)
    return _styles_create_or_update(request, ctx)


@login_required()
@permission_required('songs.delete_style', raise_exception=True)
def styles_delete(request, style_id):
    """
    Delete a style.
    :param request: HttpRequest
    :param style_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar estilo')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Estilos', reverse('songs:styles-list')),
        ('Estilo {}'.format(style_id), reverse('songs:styles-read', args=[style_id])),
        ('Eliminar estilo', ),
    ]
    ctx['CRUD'] = 'D'
    ctx['style'] = get_object_or_404(PresentationStyle, pk=style_id)

    if request.method == 'POST':
        ctx['style'].delete()
        return redirect('songs:styles-list')
    else:
        return render(request, 'songs/style_read_form.html', ctx)


def _styles_create_or_update(request, ctx):
    """
    Create or update a style.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    ctx['base_file_name_list'] = PresentationStyle._meta.get_field('base_file_name').choices

    if request.method == 'POST':
        ctx['style'].name = request.POST.get('name', '')

        try:
            index = int(request.POST.get('base_file_name', 0))
            ctx['style'].base_file_name = ctx['base_file_name_list'][index][0]
        except ValueError:
            pass    # will keep old value

        try:
            s = request.POST.get('color_language_1', '')
            e1, e2, e3 = s.split('(')[1].split(')')[0].split(',')
            ctx['style'].color_language_1 = int(e1), int(e2), int(e3)
        except ValueError:
            pass    # will keep old value
        except IndexError:
            pass    # will keep old value

        try:
            s = request.POST.get('color_language_2', '')
            e1, e2, e3 = s.split('(')[1].split(')')[0].split(',')
            ctx['style'].color_language_2 = int(e1), int(e2), int(e3)
        except ValueError:
            pass    # will keep old value
        except IndexError:
            pass    # will keep old value

        try:
            s = request.POST.get('color_songbook', '')
            e1, e2, e3 = s.split('(')[1].split(')')[0].split(',')
            ctx['style'].color_songbook = int(e1), int(e2), int(e3)
        except ValueError:
            pass    # will keep old value
        except IndexError:
            pass    # will keep old value

        try:
            s = request.POST.get('color_copyright', '')
            e1, e2, e3 = s.split('(')[1].split(')')[0].split(',')
            ctx['style'].color_copyright = int(e1), int(e2), int(e3)
        except ValueError:
            pass    # will keep old value
        except IndexError:
            pass    # will keep old value

        try:
            s = request.POST.get('color_slide_name', '')
            e1, e2, e3 = s.split('(')[1].split(')')[0].split(',')
            ctx['style'].color_slide_name = int(e1), int(e2), int(e3)
        except ValueError:
            pass    # will keep old value
        except IndexError:
            pass    # will keep old value

        # validate 'font_name' ???
        # ctx['style'].font_name = request.POST.get('font_name', '')

        try:
            ctx['style'].font_size_title_1 = int(request.POST.get('font_size_title_1', 10))
        except ValueError:
            pass    # will keep old value

        try:
            ctx['style'].font_size_title_2 = int(request.POST.get('font_size_title_2', 10))
        except ValueError:
            pass    # will keep old value

        try:
            ctx['style'].font_size_songbook = int(request.POST.get('font_size_songbook', 10))
        except ValueError:
            pass    # will keep old value

        try:
            ctx['style'].font_size_copyright = int(request.POST.get('font_size_copyright', 10))
        except ValueError:
            pass    # will keep old value

        try:
            ctx['style'].font_size_slide_name = int(request.POST.get('font_size_slide_name', 10))
        except ValueError:
            pass    # will keep old value

        try:
            ctx['style'].font_size_language_1 = int(request.POST.get('font_size_language_1', 10))
        except ValueError:
            pass    # will keep old value

        try:
            ctx['style'].font_size_language_2 = int(request.POST.get('font_size_language_2', 10))
        except ValueError:
            pass    # will keep old value

        ctx['style'].row_user_update = request.user

        try:
            ctx['style'].full_clean()
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'songs/style_update_form.html', ctx)

        ctx['style'].save()

        return redirect('songs:styles-read', ctx['style'].pk)
    else:
        return render(request, 'songs/style_update_form.html', ctx)
