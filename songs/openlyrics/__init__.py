# -*- coding: utf-8 -*-
"""
OpenLyrics parsing module.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import re
import os
from django.utils import timezone as tz
from lxml import etree
from songs.models import Author, Songbook, AuthorDetail, SongbookDetail, SongAuthor, SongSongbook
from songs.models import Verse


OLYR_NS = 'http://openlyrics.info/namespace/2009/song'
OLYR_VERSION = '0.8'
OLYR_CREATED_IN = OLYR_MODIFIED_IN = 'OpenLyrics Python Library 0.3'


def song_to_xml_tree(song):
    """
    Convert a song to a XML tree.
    :param song:
    :return:
    """
    song_lyrics = song.current_lyrics
    song_properties = song.current_properties

    root = etree.Element('song')
    root.set('xmlns', OLYR_NS)
    root.set('version', OLYR_VERSION)
    root.set('createdIn', OLYR_CREATED_IN)
    root.set('modifiedIn', OLYR_MODIFIED_IN)
    root.set('modifiedDate', tz.localtime(tz.now()).strftime('%Y-%m-%dT%H:%M:%S%z'))

    # ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # #####  TAG: properties  ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

    props = etree.Element('properties')

    e_parent = etree.Element('titles')
    if song_lyrics.title_de:
        e_child = etree.Element('title')
        e_child.text = song_lyrics.title_de
        e_child.set('lang', 'de')
        e_parent.append(e_child)
    if song_lyrics.title_en:
        e_child = etree.Element('title')
        e_child.text = song_lyrics.title_en
        e_child.set('lang', 'en')
        e_parent.append(e_child)
    if song_lyrics.title_es:
        e_child = etree.Element('title')
        e_child.text = song_lyrics.title_es
        e_child.set('lang', 'es')
        e_parent.append(e_child)
    props.append(e_parent)

    if song_lyrics.verse_order:
        e = etree.Element('verseOrder')
        e.text = song_lyrics.verse_order
        props.append(e)

    if song_properties.songauthor_set.exists():
        e_parent = etree.Element('authors')
        for e in song_properties.songauthor_set.order_by('author'):
            e_child = etree.Element('author')
            e_child.text = e.author.name
            if e.type == 'm':
                e_child.set('type', 'music')
            elif e.type == 'w':
                e_child.set('type', 'words')
            elif e.type == 't':
                e_child.set('type', 'translation')
            e_parent.append(e_child)
        props.append(e_parent)

    if song_properties.songsongbook_set.exists():
        e_parent = etree.Element('songbooks')
        for e in song_properties.songsongbook_set.order_by('songbook'):
            e_child = etree.Element('songbook')
            e_child.set('name', e.songbook.name)
            if e.entry:
                e_child.set('entry', e.entry)
            e_parent.append(e_child)
        props.append(e_parent)

    if song_properties.ccli_no > 0:
        e = etree.Element('ccliNo')
        e.text = str(song_properties.ccli_no)
        props.append(e)

    if song_properties.variant:
        e = etree.Element('variant')
        e.text = song_properties.variant
        props.append(e)

    if song_properties.version:
        e = etree.Element('version')
        e.text = song_properties.version
        props.append(e)

    if song_properties.released:
        e = etree.Element('released')
        e.text = song_properties.released
        props.append(e)

    if song_properties.publisher:
        e = etree.Element('publisher')
        e.text = song_properties.publisher
        props.append(e)

    if song_properties.copyright:
        e = etree.Element('copyright')
        e.text = song_properties.copyright
        props.append(e)

    if song_properties.keywords:
        e = etree.Element('keywords')
        e.text = song_properties.keywords
        props.append(e)

    if song_properties.themes:
        e_parent = etree.Element('themes')
        _has_content = False
        for part in song_properties.themes.split(';'):
            if part.strip():
                _has_content = True
                e_child = etree.Element('theme')
                e_child.text = part.strip()
                e_parent.append(e_child)
        if _has_content:
            props.append(e_parent)

    if song_properties.comments:
        e_parent = etree.Element('comments')
        _has_content = False
        for part in song_properties.comments.split(';'):
            if part.strip():
                _has_content = True
                e_child = etree.Element('comment')
                e_child.text = part.strip()
                e_parent.append(e_child)
        if _has_content:
            props.append(e_parent)

    if song_properties.key:
        e = etree.Element('key')
        e.text = song_properties.key
        props.append(e)

    if song_properties.transposition != 0:
        e = etree.Element('transposition')
        e.text = str(song_properties.transposition)
        props.append(e)

    if song_properties.tempo:
        e = etree.Element('tempo')
        e.text = song_properties.tempo
        e.set('type', 'text')
        props.append(e)

    root.append(props)

    # ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # #####  TAG: lyrics      ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

    lyrics = etree.Element('lyrics')

    for v in song_lyrics.verses.order_by('language', 'name'):
        e_v = etree.Element('verse')
        e_v.set('name', v.name)
        e_v.set('lang', v.language)
        for lg in v.content_lines:
            e_lg = etree.Element('lines')
            for i, line in enumerate(lg):
                if i == 0:
                    e_lg.text = line    # first line
                else:
                    br = etree.Element('br')
                    br.tail = line
                    e_lg.append(br)
            e_v.append(e_lg)
        lyrics.append(e_v)

    root.append(lyrics)

    tree = etree.ElementTree(root)
    _indent_xml(tree)
    return tree


def song_to_xml_file(song, output):
    """
    Convert a song to a XML file.
    :param song:
    :param output:
    :return:
    """
    tree = song_to_xml_tree(song)
    tree.write(output, encoding='UTF-8', xml_declaration=True, pretty_print=True)


def parse_xml(source):
    """
    Parse a XML file.
    :param source:
    :return:
    """
    parser = etree.XMLParser(remove_blank_text=True)
    try:
        return etree.parse(source, parser)
    except etree.XMLSyntaxError as err:
        raise SyntaxError(err)


def validate_xml_tree(tree):
    """
    Check that XML tree is a valid OpenLyrics song.
    :param tree:
    :return:
    """
    # make validator from RelaxNG config file
    relaxng_file_name = 'openlyrics-0.8.rng'
    dir_path = os.path.dirname(os.path.abspath(__file__))
    relaxng_tree = etree.parse(os.path.join(dir_path, relaxng_file_name))
    relaxng_validator = etree.RelaxNG(relaxng_tree)

    # validate OpenLyrics XML song tree
    relaxng_validator.assert_(tree)


def xml_to_song(tree, username):
    """
    Convert a XML tree to a song.
    :param tree:
    :param username:
    :return: new_lyrics, new_properties
    """
    new_lyrics = dict()
    new_properties = dict()
    namespace = ''
    if '}' in tree.getroot().tag:
        namespace = tree.getroot().tag.split('}')[0].lstrip('{')

    # ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # #####  TAG: properties  ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

    new_lyrics['title_de'] = ''
    new_lyrics['singable_de'] = False
    new_lyrics['title_en'] = ''
    new_lyrics['singable_en'] = False
    new_lyrics['title_es'] = ''
    new_lyrics['singable_es'] = False
    for e in tree.findall(_path('properties/titles/title', namespace)):
        text_ = _strip(e.text)
        if text_:
            if e.get('lang', None) == 'de':
                new_lyrics['title_de'] = text_
            elif e.get('lang', None) == 'en':
                new_lyrics['title_en'] = text_
            else:
                new_lyrics['title_es'] = text_      # titles with no language will be ES
    if new_lyrics['title_de']:
        new_lyrics['singable_de'] = True
    if new_lyrics['title_en']:
        new_lyrics['singable_en'] = True
    if new_lyrics['title_es']:
        new_lyrics['singable_es'] = True

    new_lyrics['verse_order'] = ''
    elem = tree.find(_path('properties/verseOrder', namespace))
    if elem is not None:
        new_lyrics['verse_order'] = ' '.join(
            e.strip() for e in _strip(elem.text).split() if e.strip())

    # authors
    author_dict = dict()
    for e in tree.findall(_path('properties/authors/author', namespace)):
        text_ = _strip(e.text)
        type_ = e.get('type', '')
        if text_:
            if text_ not in author_dict:
                author_dict[text_] = set()
            if type_ in ('music', 'words', 'translation'):
                author_dict[text_].add(type_[0])
    list1 = []
    for name_, type_set in author_dict.items():
        n = SongAuthor()
        n.type = ''
        if 'm' in type_set:
            n.type += 'm'
        if 'w' in type_set:
            n.type += 'w'
        if 't' in type_set:
            n.type += 't'

        det = AuthorDetail.objects.filter(
            name=name_, row_time_end__isnull=True, author__row_deleted=False).first()
        if det:
            # author already exists
            n.author = det.author
            list1.append(n)
        else:
            # create new author
            author = Author()
            author.do_create(name_, username)
            n.author = author
            list1.append(n)
    new_properties['songauthor_list'] = list1

    # songbooks
    songbook_dict = dict()
    for e in tree.findall(_path('properties/songbooks/songbook', namespace)):
        text_ = e.get('name', '')
        entry_ = e.get('entry', '')
        if text_:
            songbook_dict[text_] = entry_
    list1 = []
    for name_, entry_ in songbook_dict.items():
        n = SongSongbook()
        n.entry = entry_

        det = SongbookDetail.objects.filter(
            name=name_, row_time_end__isnull=True, songbook__row_deleted=False).first()
        if det:
            # songbook already exists
            n.songbook = det.songbook
            list1.append(n)
        else:
            # create new songbook
            songbook = Songbook()
            songbook.do_create(name_, username)
            n.songbook = songbook
            list1.append(n)
    new_properties['songsongbook_list'] = list1

    new_properties['ccli_no'] = 0
    elem = tree.find(_path('properties/ccliNo', namespace))
    if elem is not None:
        try:
            new_properties['ccli_no'] = int(_strip(elem.text))
        except ValueError:
            pass

    new_properties['variant'] = ''
    elem = tree.find(_path('properties/variant', namespace))
    if elem is not None:
        new_properties['variant'] = _strip(elem.text)

    new_properties['version'] = ''
    elem = tree.find(_path('properties/version', namespace))
    if elem is not None:
        new_properties['version'] = _strip(elem.text)

    new_properties['released'] = ''
    elem = tree.find(_path('properties/released', namespace))
    if elem is not None:
        new_properties['released'] = _strip(elem.text)

    new_properties['publisher'] = ''
    elem = tree.find(_path('properties/publisher', namespace))
    if elem is not None:
        new_properties['publisher'] = _strip(elem.text)

    new_properties['copyright'] = ''
    elem = tree.find(_path('properties/copyright', namespace))
    if elem is not None:
        new_properties['copyright'] = _strip(elem.text)

    new_properties['keywords'] = ''
    elem = tree.find(_path('properties/keywords', namespace))
    if elem is not None:
        new_properties['keywords'] = _strip(elem.text)

    new_properties['themes'] = '; '.join(
        _strip(e.text) for e in tree.findall(_path('properties/themes/theme', namespace)))

    new_properties['comments'] = '; '.join(
        _strip(e.text) for e in tree.findall(_path('properties/comments/comment', namespace)))

    new_properties['key'] = ''
    elem = tree.find(_path('properties/key', namespace))
    if elem is not None:
        new_properties['key'] = _strip(elem.text)

    new_properties['transposition'] = 0
    elem = tree.find(_path('properties/transposition', namespace))
    if elem is not None:
        try:
            new_properties['transposition'] = int(_strip(elem.text))
        except ValueError:
            pass

    new_properties['tempo'] = ''
    elem = tree.find(_path('properties/tempo', namespace))
    if elem is not None:
        new_properties['tempo'] = _strip(elem.text)

    # ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # #####  TAG: lyrics      ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

    verse_dict = {}
    for e in tree.findall(_path('lyrics/verse', namespace)):
        v = Verse()
        v.name = e.get('name', '')
        v.language = e.get('lang', '')

        content_list = []
        for i, lg in enumerate(e.findall(_path('lines', namespace))):
            if i > 0:
                content_list.append(Verse.slide_break() + '\n')     # slide separator
            current_line = _strip(lg.text)
            for child in lg:
                if child.tag == '{{{}}}br'.format(namespace):   # Line break
                    if current_line:
                        content_list.append(current_line)
                    current_line = _strip(child.tail)
                elif child.tail:
                    # This implementation skips other tags inside the 'lines' element, so that
                    # 'tag', 'comment' and 'chord' are ignored and just their text is used.
                    # OBS: if this changes, the function '_indent_xml' has to be modified
                    current_line += ' ' + _strip(child.tail)
            # add last line
            if current_line:
                content_list.append(current_line)

        v.content = '\n'.join(s.strip() for s in content_list)
        v.content = v.content.strip()       # remove newline after last line
        verse_dict['{}:{}'.format(v.language, v.name)] = v          # prevents duplicate verses
    new_lyrics['verse_list'] = verse_dict.values()

    # set default verse_order if its empty
    if not new_lyrics['verse_order']:
        new_lyrics['verse_order'] = ' '.join(sorted(list(set(
            v.name for v in new_lyrics['verse_list']))))

    return new_lyrics, new_properties


def _path(tag, namespace):
    """
    Add namespace to tag.

    If a namespace is on a document, the XPath requires '{ns}tag_part1/{ns}tag_part2'.
    This assumes that only one namespace for the document exists.
    :param tag:
    :param namespace:
    :return: tag
    """
    return '/'.join('{{{0}}}{1}'.format(namespace, t) for t in tag.split('/'))


def _strip(text):
    """
    Strip whitespace and return the text.
    :param text:
    :return: text
    """
    if not text:
        return ''
    return re.sub('\s+', ' ', text.strip())


def _indent_xml(tree, tab_size=4):
    """
    Add custom indentation to XML tree.

    Based on: http://effbot.org/zone/element-lib.htm#prettyprint
    See also: http://stackoverflow.com/questions/749796/pretty-printing-xml-in-python
    :param tree:
    :param tab_size: (optional)
    :return:
    """
    tab_unit = ' ' * tab_size

    def _recursive_indent(elem, level=0):
        i = '\n' + level * tab_unit

        if elem.tag == 'lines':
            # add newline after end tag, so that 'end tag of parent'
            # or 'start tag of sibling' is on the next line
            elem.tail = i

            if len(elem):
                # has 'br' elements
                # this implementation only supports 'br' elements as children of 'lines'
                elem.text = i + tab_unit + elem.text    # add newline after start tag, before text

                # iterate over children
                for elem in elem:
                    if elem.tag == 'br':
                        elem.tail = i + tab_unit + elem.tail    # add newline after 'br' tag
                if elem.tag == 'br':
                    # elem now is the last child from the iteration
                    # add newline after line of text of last child to put end tag on the next line
                    elem.tail += i
            elif elem.text and elem.text.strip():
                # has one line of text
                elem.text = i + tab_unit + elem.text    # add newline after start tag, before text
                elem.text += i                          # put end tag on the next line
            else:
                # is completely empty
                # add newline anyways to put end tag on the next line
                elem.text = i
        elif len(elem):
            # all other elements with children

            if not elem.text or not elem.text.strip():
                # put children on the next line
                elem.text = i + tab_unit
            if not elem.tail or not elem.tail.strip():
                # add newline after end tag, so that 'end tag of parent'
                # or 'start tag of sibling' is on the next line
                elem.tail = i

            # iterate over children
            for elem in elem:
                _recursive_indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
                # elem now is the last child from the iteration
                # add newline after last child to put end tag on the next line
                elem.tail = i
        else:
            # all other elements with no children

            if level and (not elem.tail or not elem.tail.strip()):
                # add newline after end tag, so that 'end tag of parent'
                # or 'start tag of sibling' is on the next line
                elem.tail = i

    # start the recursive function
    _recursive_indent(tree.getroot())
