# -*- coding: utf-8 -*-
"""
URL configuration.
"""

# Copyright (C) 2015 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.conf.urls import url, include
from songs import views, views_authors, views_songbooks, views_styles, views_files


urlpatterns_authors = [
    url(r'^$',
        views_authors.authors_list,
        name='authors-list'),
    url(r'^deleted/$',
        views_authors.authors_list_deleted,
        name='authors-list-deleted'),
    url(r'^create/$',
        views_authors.authors_create,
        name='authors-create'),
    url(r'^(?P<author_id>\d+)/$',
        views_authors.authors_read,
        name='authors-read'),
    url(r'^(?P<author_id>\d+)/update/$',
        views_authors.authors_update,
        name='authors-update'),
    url(r'^(?P<author_id>\d+)/delete/$',
        views_authors.authors_delete,
        name='authors-delete'),
    url(r'^(?P<author_id>\d+)/delete/undo/$',
        views_authors.authors_delete_undo,
        name='authors-delete-undo'),
    url(r'^(?P<author_id>\d+)/history/$',
        views_authors.authors_history,
        name='authors-history'),
]


urlpatterns_songbooks = [
    url(r'^$',
        views_songbooks.songbooks_list,
        name='songbooks-list'),
    url(r'^deleted/$',
        views_songbooks.songbooks_list_deleted,
        name='songbooks-list-deleted'),
    url(r'^create/$',
        views_songbooks.songbooks_create,
        name='songbooks-create'),
    url(r'^(?P<songbook_id>\d+)/$',
        views_songbooks.songbooks_read,
        name='songbooks-read'),
    url(r'^(?P<songbook_id>\d+)/update/$',
        views_songbooks.songbooks_update,
        name='songbooks-update'),
    url(r'^(?P<songbook_id>\d+)/delete/$',
        views_songbooks.songbooks_delete,
        name='songbooks-delete'),
    url(r'^(?P<songbook_id>\d+)/delete/undo/$',
        views_songbooks.songbooks_delete_undo,
        name='songbooks-delete-undo'),
    url(r'^(?P<songbook_id>\d+)/history/$',
        views_songbooks.songbooks_history,
        name='songbooks-history'),
]


urlpatterns_styles = [
    url(r'^$',
        views_styles.styles_list,
        name='styles-list'),
    url(r'^create/$',
        views_styles.styles_create,
        name='styles-create'),
    url(r'^(?P<style_id>\d+)/$',
        views_styles.styles_read,
        name='styles-read'),
    url(r'^(?P<style_id>\d+)/update/$',
        views_styles.styles_update,
        name='styles-update'),
    url(r'^(?P<style_id>\d+)/delete/$',
        views_styles.styles_delete,
        name='styles-delete'),
]


urlpatterns_files = [
    url(r'^$',
        views_files.files_list,
        name='files-list'),
    url(r'^zip/$',
        views_files.files_download_zip,
        name='files-download-zip'),
    url(r'^zip/all-pptx/$',
        views_files.files_download_zip_pptx,
        name='files-download-zip-pptx'),
    url(r'^zip/all-xml/$',
        views_files.files_download_zip_xml,
        name='files-download-zip-xml'),
]


urlpatterns = [
    url(r'^$',
        views.songs_list,
        name='songs-list'),
    url(r'^deleted/$',
        views.songs_list_deleted,
        name='songs-list-deleted'),
    url(r'^authors/',
        include(urlpatterns_authors)),
    url(r'^songbooks/',
        include(urlpatterns_songbooks)),
    url(r'^styles/',
        include(urlpatterns_styles)),
    url(r'^files/',
        include(urlpatterns_files)),
    url(r'^create/$',
        views.songs_create,
        name='songs-create'),
    url(r'^create/import/$',
        views.songs_create_from_file,
        name='songs-create-from-file'),
    url(r'^(?P<song_id>\d+)/$',
        views.songs_read,
        name='songs-read'),
    url(r'^(?P<song_id>\d+)/update-lyrics/$',
        views.songs_update_lyrics,
        name='songs-update-lyrics'),
    url(r'^(?P<song_id>\d+)/update-properties/$',
        views.songs_update_properties,
        name='songs-update-properties'),
    url(r'^(?P<song_id>\d+)/delete/$',
        views.songs_delete,
        name='songs-delete'),
    url(r'^(?P<song_id>\d+)/delete/undo/$',
        views.songs_delete_undo,
        name='songs-delete-undo'),
    url(r'^(?P<song_id>\d+)/history-lyrics/$',
        views.songs_history_lyrics,
        name='songs-history-lyrics'),
    url(r'^(?P<song_id>\d+)/history-properties/$',
        views.songs_history_properties,
        name='songs-history-properties'),
    url(r'^(?P<song_id>\d+)/pptx/$',
        views.songs_make_pptx,
        name='songs-pptx'),
    url(r'^(?P<song_id>\d+)/xml/$',
        views.songs_make_xml,
        name='songs-xml'),
]
