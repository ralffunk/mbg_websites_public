(function () {  // start of closure
    $(document).ready(function () {
        // go to url on table row click
        $('.clickable-row').click(function () {
            window.document.location = $(this).data('href');
        });

        $('.my-footable').footable({
            'empty': 'No hay canciones para mostrar',
            'paging': {
			    'enabled': true,
			    'size': 25,
			    'countFormat': 'Página {CP} de {TP}'
		    },
            'filtering': {
                'enabled': true,
                'placeholder': 'Buscar ...'
            },
            'state': {
                'enabled': true,
                'paging': true,
                'filtering': true,
                'sorting': false
            }
        });

        // remove style element that hides the rows
        $('#customSongListCss').remove();
    });
})();   // end of closure
