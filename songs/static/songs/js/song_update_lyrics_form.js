(function () {  // start of closure
    $(document).ready(function () {
        // functions that add en element to the hidden select
        function addVerseToHiddenSelect(language_and_name) {
            $('#verses_select').append($('<option>', {
                value: language_and_name,
                selected: 'selected'
            }));
        }

        // functions that remove an element from the hidden select
        function removeVerseFromHiddenSelect(language_and_name) {
            // carefully only delete the first occurrence
            var filter = 'value="' + language_and_name + '"';
            $('#verses_select option[' + filter + ']:first').remove();
        }

        // function that adds an element to the visible form
        function addVerseToForm(language_and_name, content) {
            var v = language_and_name.split(':');
            var language = v[0];
            var name = v[1];
            var base_row = $('#verse_row_base');
            var new_row = base_row.clone(true);     //clone with event handlers
            new_row.removeAttr('id');
            if(language == 'de') {
                new_row.find('.verse-row-language').addClass('text-success');
            } else if(language == 'en') {
                new_row.find('.verse-row-language').addClass('text-danger');
            } else if(language == 'es') {
                new_row.find('.verse-row-language').addClass('text-primary');
            } else {
                new_row.find('.verse-row-language').addClass('text-warning');
            }
            new_row.find('.verse-row-language').html(language + ':');
            new_row.find('.verse-row-name').html(name);
            new_row.find('textarea').attr('name', language_and_name);
            new_row.find('textarea').val(content);
            new_row.find('.verse-row-rename').data('my-content', language_and_name);
            $('#verse_div').append(new_row);
        }

        // event: click delete-icon of a row
        $('.verse-row-remove').click(function () {
            // select whole row and get its data
            var row = $(this).parents('.verse-row');
            var language = row.find('.verse-row-language').html().trim();
            var name = row.find('.verse-row-name').html().trim();

            row.remove();   // remove row from visible form
            removeVerseFromHiddenSelect(language + name);   //'language' already contains ':'
        });

        // event: click button in modal
        $('#m_confirmAddVerse').click(function () {
            var language = $('#m_languageSelect option:selected').val().trim();
            var name = $('#m_nameSelect option:selected').val().trim();
            var name_number = $('#m_nameInput').val().trim();
            var language_and_name = language + ':' + name;
            if(name_number > 0) {
                language_and_name += name_number;
            }

            addVerseToHiddenSelect(language_and_name);
            addVerseToForm(language_and_name, '');
        });
        $('#m2_confirmRenameVerse').click(function () {
            // remove old
            var old_language_and_name = $('#m2_verseRenamingModal').data('original-name');

            removeVerseFromHiddenSelect(old_language_and_name);
            var filter = 'name="' + old_language_and_name + '"';
            var textarea1 = $('.verse-row textarea[' + filter + ']:first');
            var verse_text = textarea1.val().trim();
            textarea1.parents('.verse-row').remove();       // remove row from visible form

            // add new
            var language = $('#m2_languageSelect option:selected').val().trim();
            var name = $('#m2_nameSelect option:selected').val().trim();
            var name_number = $('#m2_nameInput').val().trim();
            var new_language_and_name = language + ':' + name;
            if(name_number > 0) {
                new_language_and_name += name_number;
            }

            addVerseToHiddenSelect(new_language_and_name);
            addVerseToForm(new_language_and_name, verse_text);
        });

        // reset inputs before displaying modal
        $('#m_verseAdditionModal').on('show.bs.modal', function (event) {
            $('#m_nameInput').val(0);
        });
        $('#m2_verseRenamingModal').on('show.bs.modal', function (event) {
            var language_and_name = $(event.relatedTarget).data('my-content');
            $(this).data('original-name', language_and_name);
            var v = language_and_name.split(':');

            // set language
            var language = v[0];
            $('#m2_languageSelect option').each(function () {
                if($(this).val().trim() == language) {
                    $(this).prop('selected', 'true');
                } else {
                    $(this).prop('selected', null);
                }
            });

            // set name
            var name = v[1].substring(0, 1);
            $('#m2_nameSelect option').each(function () {
                if($(this).val().trim() == name) {
                    $(this).prop('selected', 'true');
                } else {
                    $(this).prop('selected', null);
                }
            });

            // set number
            $('#m2_nameInput').val(0);
            if(v[1].length > 1) {
                var name_number = v[1].substring(1);
                $('#m2_nameInput').val(name_number);
            }
        });

        // set focus on select after displaying modal
        $('#m_verseAdditionModal').on('shown.bs.modal', function () {
            $('#m_languageSelect').focus();
        });
        $('#m2_verseRenamingModal').on('shown.bs.modal', function () {
            $('#m2_languageSelect').focus();
        });

        // set initial state of elements after page load
        // for each option in hidden select, add a row to the visible form
        $('#verses_select option').each(function () {
            addVerseToForm($(this).val().trim(), $(this).data('my-content'));
        });
    });
})();   // end of closure
