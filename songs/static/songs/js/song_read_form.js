(function () {  // start of closure
    $(document).ready(function () {
        // event: click button in modal
        $('#m_confirmDownload').click(function () {
            var my_url = $('#m_downloadUrl').val().trim();
            var language_1 = $('#m_songLanguage1 option:selected').val().trim();
            var language_2 = $('#m_songLanguage2 option:selected').val().trim();
            var style = $('#m_songStyle option:selected').val().trim();
            var params = 'language1=' + language_1 + '&language2=' + language_2 + '&style=' + style;
            window.location.href = my_url + '?' + params;
        });

        // set focus on select after displaying modal
        $('#m_pptxCreationModal').on('shown.bs.modal', function () {
            $('#m_songLanguage1').focus();
        });
    });
})();   // end of closure
