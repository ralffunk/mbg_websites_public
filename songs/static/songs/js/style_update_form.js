(function () {  // start of closure
    $(document).ready(function () {
        // create colorpicker for each element with this class
        $('.my-colorpicker').each(function () {
            $(this).colorpicker({
                'format': 'rgb',
                'align': 'left',
            });
        });
    });
})();   // end of closure
