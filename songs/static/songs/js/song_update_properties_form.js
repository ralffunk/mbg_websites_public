(function () {  // start of closure
    $(document).ready(function () {
        // function that adds an element to the hidden select
        function addToHiddenSelect(entity, pk_dsc) {
            $('#' + entity + 's_select').append($('<option>', {
                value: pk_dsc,
                selected: 'selected'
            }));
        }

        // function that removes an element from the hidden select
        function removeFromHiddenSelect(entity, pk_dsc) {
            var filter = 'value="' + pk_dsc + '"';
            // carefully only delete the first occurrence
            $('#' + entity + 's_select option[' + filter + ']:first').remove();
        }

        // function that adds an element to the form
        function addToForm(entity, pk_dsc) {
            var base_row = $('#' + entity + '_row_base');
            var new_row = base_row.clone(true);     //clone with event handlers
            new_row.removeAttr('id');
            new_row.find('.' + entity + '-pk').html(pk_dsc);
            new_row.find('.' + entity + '-name').html(pk_dsc.split('$')[1]);
            $('#' + entity + '_div').append(new_row);
        }

        // function that updates 'readonly' properties of the name inputs in the modals
        function update_modal_inputs(entity) {
            var name_select = $('#m_' + entity + 'sSelect');
            var new_name_input = $('#m_' + entity + 'NewNameInput');
            if($('#m_' + entity + 'NewCheckbox').prop('checked')) {
                name_select.prop('disabled', 'true');
                new_name_input.prop('readonly', null);
            } else {
                name_select.prop('disabled', null);
                new_name_input.prop('readonly', 'true');
            }
        }

        // function that return the data from the modal selects and inputs
        function get_modal_data(entity) {
            var return_obj = {
                'pk': '-',
                'name': '',
            };

            if($('#m_' + entity + 'NewCheckbox').prop('checked')) {
                return_obj['pk'] = 'new';
                return_obj['name'] = $('#m_' + entity + 'NewNameInput').val().trim();
            } else {
                return_obj['pk'] = $('#m_' + entity + 'sSelect option:selected').val().trim();
                return_obj['name'] = $('#m_' + entity + 'sSelect option:selected').html().trim();
            }
            return_obj['pk'] += '-';

            return return_obj;
        }

        // event: click checkbox in modal
        $('#m_authorNewCheckbox').click(function () {
            update_modal_inputs('author');
        });
        $('#m_songbookNewCheckbox').click(function () {
            update_modal_inputs('songbook');
        });

        // event: click delete-icon of a row
        $('.author-row-remove').click(function () {
            // select whole row and get its data
            var row = $(this).parents('.author-row');
            var pk_dsc = row.find('.author-pk').html().trim();

            row.remove();   // remove row from visible form
            removeFromHiddenSelect('author', pk_dsc);
        });
        $('.songbook-row-remove').click(function () {
            // select whole row and get its data
            var row = $(this).parents('.songbook-row');
            var pk_dsc = row.find('.songbook-pk').html().trim();

            row.remove();   // remove row from visible form
            removeFromHiddenSelect('songbook', pk_dsc);
        });

        // event: click button in modal
        $('#m_authorAddButton').click(function () {
            var return_obj = get_modal_data('author');
            var types = [];

            if($('#m_authorOptional_m').prop('checked')) {
                return_obj['pk'] += 'm';
                types.push('música');
            }
            if($('#m_authorOptional_w').prop('checked')) {
                return_obj['pk'] += 'w';
                types.push('letra');
            }
            if($('#m_authorOptional_t').prop('checked')) {
                return_obj['pk'] += 't';
                types.push('traducción');
            }
            if(types.length > 0) {
                return_obj['name'] += ' (' + types.join(' y ') + ')';
            }
            var pk_dsc = return_obj['pk'] + '$' + return_obj['name'];

            addToHiddenSelect('author', pk_dsc);
            addToForm('author', pk_dsc);
        });
        $('#m_songbookAddButton').click(function () {
            var return_obj = get_modal_data('songbook');
            var optional = $('#m_songbookOptional_entry').val().trim();

            if(optional != '') {
                return_obj['pk'] += optional;
                return_obj['name'] += ' #' + optional;
            }
            var pk_dsc = return_obj['pk'] + '$' + return_obj['name'];

            addToHiddenSelect('songbook', pk_dsc);
            addToForm('songbook', pk_dsc);
        });

        // reset inputs before displaying modal
        $('#m_authorSelectionModal').on('show.bs.modal', function (event) {
            $('#m_authorNewCheckbox').prop('checked', null);
            $('#m_authorNewNameInput').val('');
            update_modal_inputs('author');

            $('#m_authorOptional_m').prop('checked', null);
            $('#m_authorOptional_w').prop('checked', null);
            $('#m_authorOptional_t').prop('checked', null);
        });
        $('#m_songbookSelectionModal').on('show.bs.modal', function (event) {
            $('#m_songbookNewCheckbox').prop('checked', null);
            $('#m_songbookNewNameInput').val('');
            update_modal_inputs('songbook');

            $('#m_songbookOptional_entry').val('');
        });

        // set focus on select after displaying modal
        $('#m_authorSelectionModal').on('shown.bs.modal', function () {
            $('#m_authorsSelect').focus();
        });
        $('#m_songbookSelectionModal').on('shown.bs.modal', function () {
            $('#m_songbooksSelect').focus();
        });

        // display initial elements after page load
        // for each option in hidden select, add a row to the visible form
        $('#authors_select option').each(function () {
            addToForm('author', $(this).val().trim());
        });
        $('#songbooks_select option').each(function () {
            addToForm('songbook', $(this).val().trim());
        });
    });
})();   // end of closure
