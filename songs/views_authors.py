# -*- coding: utf-8 -*-
"""
Views.
"""

# Copyright (C) 2016 Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from mbg_websites import utils
from songs.models import Author, AuthorDetail


def authors_list(request):
    """
    List authors.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Lista de autores', ),
    ]

    qs = AuthorDetail.objects.filter(author__row_deleted=False, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    qs = qs.filter(name__icontains=ctx['sf_text'])

    ctx['sf_order'] = '0'
    qs = qs.order_by('name')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    author_list = []
    for i, e in enumerate(qs):
        author_list.append({
            'url': reverse('songs:authors-read', args=[e.author.pk]),
            'is_header': True,
            'counter': i+1,
            'name': e.name,
        })

        for j, s in enumerate(e.author.songauthor_set.filter(
                song_properties__row_time_end__isnull=True)):
            author_list.append({
                'url': reverse('songs:songs-read', args=[s.song_properties.song.pk]),
                'deleted': s.song_properties.song.row_deleted,
                'counter': j+1,
                'song_title': s.song_properties.song.first_title,
                'type': s.get_type_display(),
            })

    paginator = Paginator(author_list, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'songs/author_list.html', ctx)


@login_required()
@permission_required('songs.delete_author', raise_exception=True)
def authors_list_deleted(request):
    """
    List deleted authors.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Autores', reverse('songs:authors-list')),
        ('Autores eliminados', ),
    ]

    qs = AuthorDetail.objects.filter(author__row_deleted=True, row_time_end__isnull=True)

    ctx['sf_text'] = request.GET.get('sf_text', '')
    qs = qs.filter(name__icontains=ctx['sf_text'])

    if request.GET.get('sf_order', '') == '1':
        ctx['sf_order'] = '1'
        qs = qs.order_by('name')
    else:
        ctx['sf_order'] = '0'
        qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_order'])

    for e in qs:
        e.url = reverse('songs:authors-read', args=[e.author.pk])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    return render(request, 'songs/author_deleted_list.html', ctx)


@login_required()
@permission_required('songs.create_author', raise_exception=True)
def authors_create(request):
    """
    Create an author.
    :param request: HttpRequest
    :return: HttpResponse
    """
    ctx = dict(page_title='Crear nuevo autor')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Autores', reverse('songs:authors-list')),
        ('Crear nuevo autor', ),
    ]
    ctx['CRUD'] = 'C'
    ctx['author'] = Author()
    return _authors_create_or_update(request, ctx)


@login_required()
@permission_required('songs.update_author', raise_exception=True)
def authors_read(request, author_id):
    """
    Read an author.
    :param request: HttpRequest
    :param author_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Detalles del autor')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Autores', reverse('songs:authors-list')),
        ('Autor {}'.format(author_id), ),
    ]
    ctx['CRUD'] = 'R'
    ctx['author'] = get_object_or_404(Author, pk=author_id)
    ctx['history_list'] = ctx['author'].details.order_by('-row_time_start')[:5]

    if ctx['author'].row_deleted:
        messages.error(request, 'Este autor se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Autores eliminados', reverse('songs:authors-list-deleted')))

    return render(request, 'songs/author_form.html', ctx)


@login_required()
@permission_required('songs.update_author', raise_exception=True)
def authors_update(request, author_id):
    """
    Update an author.
    :param request: HttpRequest
    :param author_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Modificar autor')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Autores', reverse('songs:authors-list')),
        ('Autor {}'.format(author_id), reverse('songs:authors-read', args=[author_id])),
        ('Modificar autor', ),
    ]
    ctx['CRUD'] = 'U'
    ctx['author'] = get_object_or_404(Author, pk=author_id, row_deleted=False)
    return _authors_create_or_update(request, ctx)


@login_required()
@permission_required('songs.delete_author', raise_exception=True)
def authors_delete(request, author_id):
    """
    Delete an author.
    :param request: HttpRequest
    :param author_id:
    :return: HttpResponse
    """
    ctx = dict(page_title='Eliminar autor')
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Autores', reverse('songs:authors-list')),
        ('Autor {}'.format(author_id), reverse('songs:authors-read', args=[author_id])),
        ('Eliminar autor', ),
    ]
    ctx['CRUD'] = 'D'
    ctx['author'] = get_object_or_404(Author, pk=author_id, row_deleted=False)

    ctx['author'].can_be_deleted = True
    if ctx['author'].songauthor_set.filter(song_properties__row_time_end__isnull=True).exists():
        ctx['author'].can_be_deleted = False
        messages.error(request, 'Este autor no puede ser eliminado porque tiene canciones '
                                'asociadas.')

    if request.method == 'POST' and ctx['author'].can_be_deleted:
        ctx['author'].do_delete(request.user)
        return redirect('songs:authors-list')
    else:
        return render(request, 'songs/author_form.html', ctx)


@login_required()
@permission_required('songs.delete_author', raise_exception=True)
def authors_delete_undo(request, author_id):
    """
    Undo the delete of an author.
    :param request: HttpRequest
    :param author_id:
    :return: HttpResponse
    """
    author = get_object_or_404(Author, pk=author_id)

    if request.method == 'POST':
        if author.row_deleted:
            author.undo_delete(request.user)

    return redirect('songs:authors-read', author.pk)


@login_required()
@permission_required('songs.update_author', raise_exception=True)
def authors_history(request, author_id):
    """
    View changes history of an author.
    :param request: HttpRequest
    :param author_id:
    :return: HttpResponse
    """
    ctx = dict()
    ctx['breadcrumb'] = [
        ('Inicio', reverse('start-page')),
        ('Autores', reverse('songs:authors-list')),
        ('Autor {}'.format(author_id), reverse('songs:authors-read', args=[author_id])),
        ('Historial de cambios', ),
    ]
    ctx['author'] = get_object_or_404(Author, pk=author_id)
    qs = ctx['author'].details

    ctx['sf_text'] = request.GET.get('sf_text', '')
    q_t1 = Q(row_action__icontains=ctx['sf_text'])
    q_t2 = Q(name__icontains=ctx['sf_text'])
    qs = qs.filter(q_t1 | q_t2)

    ctx.update(utils.get_sf_dates(request.GET))
    qs = qs.filter(row_time_start__range=(ctx['sf_date_start'], ctx['sf_date_end']))

    ctx['sf_order'] = '0'
    qs = qs.order_by('-row_time_start')

    ctx['sf'] = 'sf_text={}&sf_date_start={}&sf_date_end={}&sf_order={}'.format(
        ctx['sf_text'],
        ctx['sf_date_start'].strftime('%d-%m-%Y'),
        ctx['sf_date_end'].strftime('%d-%m-%Y'),
        ctx['sf_order'])

    paginator = Paginator(qs, 50)
    page = request.GET.get('page', 1)
    try:
        ctx['page'] = paginator.page(page)
    except PageNotAnInteger:
        ctx['page'] = paginator.page(1)
    except EmptyPage:
        ctx['page'] = paginator.page(paginator.num_pages)

    if ctx['author'].row_deleted:
        messages.error(request, 'Este autor se encuentra en estado eliminado.')
        ctx['breadcrumb'].insert(
            2, ('Autores eliminados', reverse('songs:authors-list-deleted')))

    return render(request, 'songs/author_history_list.html', ctx)


def _authors_create_or_update(request, ctx):
    """
    Create or update an author.
    :param request: HttpRequest
    :param ctx:
    :return: HttpResponse
    """
    if request.method == 'POST':
        try:
            if ctx['CRUD'] == 'C':
                ctx['author'].do_create(request.POST.get('name', ''), request.user)
            else:
                ctx['author'].do_update(request.POST.get('name', ''), request.user)
        except ValidationError as err:
            for msgs in err.message_dict.values():
                for msg in msgs:
                    messages.error(request, msg)
            return render(request, 'songs/author_form.html', ctx)

        return redirect('songs:authors-read', ctx['author'].pk)
    else:
        return render(request, 'songs/author_form.html', ctx)
