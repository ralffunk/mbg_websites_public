# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def create_first_styles(apps, schema_editor):
    # We can't import the model directly for some reason
    # See also: https://docs.djangoproject.com/en/1.8/topics/migrations/#data-migrations
    PresentationStyle = apps.get_model('songs', 'PresentationStyle')
    User = apps.get_model('auth', 'User')
    usr = User.objects.first()

    PresentationStyle.objects.create(
        name='fondo negro',
        base_file_name='4-3-black.pptx',
        color_language_1_r=255,
        color_language_1_g=255,
        color_language_1_b=255,
        color_language_2_r=255,
        color_language_2_g=204,
        color_language_2_b=0,
        color_songbook_r=255,
        color_songbook_g=255,
        color_songbook_b=255,
        color_copyright_r=255,
        color_copyright_g=255,
        color_copyright_b=255,
        color_slide_name_r=255,
        color_slide_name_g=255,
        color_slide_name_b=255,
        font_name='Calibri',
        font_size_title_1=44,
        font_size_title_2=44,
        font_size_songbook=24,
        font_size_copyright=14,
        font_size_slide_name=14,
        font_size_language_1=36,
        font_size_language_2=32,
        row_user_insert=usr,
        row_user_update=usr)
    PresentationStyle.objects.create(
        name='fondo blanco',
        base_file_name='4-3-white.pptx',
        color_language_1_r=0,
        color_language_1_g=0,
        color_language_1_b=0,
        color_language_2_r=255,
        color_language_2_g=204,
        color_language_2_b=0,
        color_songbook_r=0,
        color_songbook_g=0,
        color_songbook_b=0,
        color_copyright_r=0,
        color_copyright_g=0,
        color_copyright_b=0,
        color_slide_name_r=0,
        color_slide_name_g=0,
        color_slide_name_b=0,
        font_name='Calibri',
        font_size_title_1=44,
        font_size_title_2=44,
        font_size_songbook=24,
        font_size_copyright=14,
        font_size_slide_name=14,
        font_size_language_1=36,
        font_size_language_2=32,
        row_user_insert=usr,
        row_user_update=usr)


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0001_initial'),
        ('accounts', '0002_first_user'),   # dependency for created user
    ]

    operations = [
        migrations.RunPython(create_first_styles),
    ]
