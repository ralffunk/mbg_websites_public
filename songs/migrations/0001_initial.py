# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django.core.validators
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('row_deleted', models.BooleanField(default=False)),
            ],
            options={
                'permissions': (('create_author', 'crear autor'), ('update_author', 'modificar autor'), ('delete_author', 'eliminar autor')),
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='AuthorDetail',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', error_messages={'blank': 'El nombre del autor no puede quedar vacío.'}, max_length=100)),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('author', models.ForeignKey(related_name='details', to='songs.Author')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='PresentationStyle',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', error_messages={'blank': 'El nombre del estilo no puede quedar vacío.', 'unique': 'El nombre del estilo no puede ser duplicado.'}, max_length=20, unique=True)),
                ('base_file_name', models.CharField(choices=[('4-3-black.pptx', '4:3 - fondo negro'), ('4-3-white.pptx', '4:3 - fondo blanco'), ('16-10-black.pptx', '16:10 - fondo negro'), ('16-10-white.pptx', '16:10 - fondo blanco')], default='4-3-black.pptx', error_messages={'blank': 'El estilo base no puede quedar vacío.'}, max_length=50)),
                ('color_language_1_r', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_language_1_g', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_language_1_b', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_language_2_r', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_language_2_g', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_language_2_b', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_songbook_r', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_songbook_g', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_songbook_b', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_copyright_r', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_copyright_g', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_copyright_b', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_slide_name_r', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_slide_name_g', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('color_slide_name_b', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El color debe ser mayor o igual a 0.'), django.core.validators.MaxValueValidator(255, message='El color debe ser menor o igual a 255.')], default=0)),
                ('font_name', models.CharField(default='Calibri', error_messages={'blank': 'El tipo de letra no puede quedar vacío.'}, max_length=100)),
                ('font_size_title_1', models.IntegerField(validators=[django.core.validators.MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'), django.core.validators.MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')], default=10)),
                ('font_size_title_2', models.IntegerField(validators=[django.core.validators.MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'), django.core.validators.MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')], default=10)),
                ('font_size_songbook', models.IntegerField(validators=[django.core.validators.MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'), django.core.validators.MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')], default=10)),
                ('font_size_copyright', models.IntegerField(validators=[django.core.validators.MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'), django.core.validators.MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')], default=10)),
                ('font_size_slide_name', models.IntegerField(validators=[django.core.validators.MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'), django.core.validators.MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')], default=10)),
                ('font_size_language_1', models.IntegerField(validators=[django.core.validators.MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'), django.core.validators.MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')], default=10)),
                ('font_size_language_2', models.IntegerField(validators=[django.core.validators.MinValueValidator(10, message='El tamaño de fuente debe ser mayor o igual a 10.'), django.core.validators.MaxValueValidator(60, message='El tamaño de fuente debe ser menor o igual a 60.')], default=10)),
                ('row_time_insert', models.DateTimeField(auto_now_add=True)),
                ('row_time_update', models.DateTimeField(auto_now=True)),
                ('row_user_insert', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, related_name='created_styles')),
                ('row_user_update', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, related_name='modified_styles')),
            ],
            options={
                'permissions': (('create_style', 'crear estilo de presentación'), ('update_style', 'modificar estilo de presentación'), ('delete_style', 'eliminar estilo de presentación')),
                'ordering': ['name'],
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='Song',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('row_deleted', models.BooleanField(default=False)),
            ],
            options={
                'permissions': (('create_song', 'crear canción'), ('update_song', 'modificar canción'), ('delete_song', 'eliminar canción')),
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='SongAuthor',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('type', models.CharField(blank=True, max_length=3, default='')),
                ('author', models.ForeignKey(to='songs.Author')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='Songbook',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('row_deleted', models.BooleanField(default=False)),
            ],
            options={
                'permissions': (('create_songbook', 'crear himnario'), ('update_songbook', 'modificar himnario'), ('delete_songbook', 'eliminar himnario')),
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='SongbookDetail',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', error_messages={'blank': 'El nombre del himnario no puede quedar vacío.'}, max_length=100)),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('songbook', models.ForeignKey(related_name='details', to='songs.Songbook')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='SongLyrics',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title_de', models.CharField(blank=True, max_length=100, default='')),
                ('title_en', models.CharField(blank=True, max_length=100, default='')),
                ('title_es', models.CharField(blank=True, max_length=100, default='')),
                ('verse_order', models.CharField(blank=True, max_length=100, default='')),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('song', models.ForeignKey(related_name='lyrics', to='songs.Song')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='SongProperties',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('ccli_no', models.IntegerField(validators=[django.core.validators.MinValueValidator(0, message='El número CCLI debe ser mayor o igual a 0.')], default=0)),
                ('variant', models.CharField(blank=True, max_length=25, default='')),
                ('version', models.CharField(blank=True, max_length=25, default='')),
                ('released', models.CharField(blank=True, max_length=25, default='')),
                ('publisher', models.CharField(blank=True, max_length=100, default='')),
                ('copyright', models.CharField(blank=True, max_length=100, default='')),
                ('keywords', models.CharField(blank=True, max_length=100, default='')),
                ('themes', models.CharField(blank=True, max_length=100, default='')),
                ('comments', models.CharField(blank=True, max_length=100, default='')),
                ('key', models.CharField(blank=True, max_length=25, default='')),
                ('transposition', models.IntegerField(validators=[django.core.validators.MinValueValidator(-99, message='La transposición debe estar entre -99 y 99.'), django.core.validators.MaxValueValidator(99, message='La transposición debe estar entre -99 y 99.')], default=0)),
                ('tempo', models.CharField(blank=True, max_length=25, default='')),
                ('row_action', models.CharField(default='CREATE', max_length=6)),
                ('row_time_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_time_end', models.DateTimeField(blank=True, null=True)),
                ('row_user', models.CharField(default='---', max_length=30)),
                ('song', models.ForeignKey(related_name='properties', to='songs.Song')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='SongSongbook',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('entry', models.CharField(blank=True, max_length=5, default='')),
                ('song_properties', models.ForeignKey(to='songs.SongProperties')),
                ('songbook', models.ForeignKey(to='songs.Songbook')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='Verse',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', error_messages={'blank': 'El nombre de la estrofa no puede quedar vacío.'}, max_length=5)),
                ('language', models.CharField(default='', error_messages={'blank': 'El idioma de la estrofa no puede quedar vacío.'}, max_length=5)),
                ('content', models.CharField(default='', error_messages={'blank': 'La estrofa no puede quedar vacía.'}, max_length=1000)),
                ('song_lyrics', models.ForeignKey(related_name='verses', to='songs.SongLyrics')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.AddField(
            model_name='songauthor',
            name='song_properties',
            field=models.ForeignKey(to='songs.SongProperties'),
        ),
    ]
