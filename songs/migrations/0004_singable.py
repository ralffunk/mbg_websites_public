# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0003_auto_20160429_1021'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='presentationstyle',
            options={'permissions': (('create_style', 'crear estilo de presentación'), ('update_style', 'modificar estilo de presentación'), ('delete_style', 'eliminar estilo de presentación')), 'default_permissions': ()},
        ),
        migrations.AlterModelOptions(
            name='verse',
            options={'default_permissions': (), 'ordering': ['song_lyrics', 'language', 'name']},
        ),
        migrations.AddField(
            model_name='songlyrics',
            name='singable_de',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='songlyrics',
            name='singable_en',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='songlyrics',
            name='singable_es',
            field=models.BooleanField(default=True),
        ),
    ]
