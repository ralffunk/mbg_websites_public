# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0005_auto_20160604_1335'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='authordetail',
            name='row_user',
        ),
        migrations.RemoveField(
            model_name='file',
            name='row_user',
        ),
        migrations.RemoveField(
            model_name='songbookdetail',
            name='row_user',
        ),
        migrations.RemoveField(
            model_name='songlyrics',
            name='row_user',
        ),
        migrations.RemoveField(
            model_name='songproperties',
            name='row_user',
        ),
    ]
