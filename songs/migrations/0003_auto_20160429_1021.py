# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('songs', '0002_first_styles'),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('content_type', models.CharField(blank=True, max_length=5, default='')),
                ('codename', models.CharField(blank=True, max_length=10, default='')),
                ('download_name', models.CharField(blank=True, max_length=200, default='')),
                ('size', models.IntegerField(default=0)),
                ('bytes', models.BinaryField()),
                ('row_time', models.DateTimeField(default=django.utils.timezone.now)),
                ('row_user', models.CharField(max_length=30, default='---')),
                ('song', models.ForeignKey(to='songs.Song', related_name='files')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.AlterUniqueTogether(
            name='file',
            unique_together=set([('song', 'content_type', 'codename')]),
        ),
    ]
